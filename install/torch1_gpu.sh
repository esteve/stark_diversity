#!/bin/sh

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

set -eu

mkdir -p whl

#wget -nc -O whl/torch-1.9.1+cu111-cp39-cp39-linux_x86_64.whl https://download.pytorch.org/whl/torch/torch-1.9.1+cu111-cp39-cp39-linux_x86_64.whl
wget -nc -O whl/torch-1.9.1+cu111-cp39-cp39-linux_x86_64.whl https://download.pytorch.org/whl/cu111/torch-1.9.1%2Bcu111-cp39-cp39-linux_x86_64.whl#sha256=c0d2cbb51f59c4a915393cf06c08a43391fcde95f21fe68f1d51727eb3b87353

#python3 -m pip install --upgrade --proxy=${HTTPS_PROXY} --ignore-requires-python "torch==1.9.1" --index-url https://download.pytorch.org/whl/torch
#python3 -m pip install --upgrade --proxy=${HTTPS_PROXY} --ignore-requires-python https://download.pytorch.org/whl/torch/torch-1.9.1+cu111-cp39-cp39-linux_x86_64.whl
##python3 -m pip install --upgrade --proxy=${HTTPS_PROXY} --ignore-requires-python torch-1.9.1+cu111-cp39-cp39-linux_x86_64.whl --index-url https://download.pytorch.org/whl/torch/
python3 -m pip install --upgrade --proxy=${HTTPS_PROXY} --ignore-requires-python whl/torch-1.9.1+cu111-cp39-cp39-linux_x86_64.whl 
