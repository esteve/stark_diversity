#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import os
import glob

def main():
	path_list_lang = ["data", "target", "union", "UniversalDependencies", "lang"]
	path = os.sep.join(path_list_lang)
	cmd_ = f"mkdir -p {path}"
	os.system(cmd_)

	path_list_all = ["data", "target", "union", "UniversalDependencies", "UD_All_Languages-UNION"]
	path = os.sep.join(path_list_all)
	cmd_ = f"mkdir -p {path}"
	os.system(cmd_)

	path_list = ["data", "input", "UniversalDependencies"]
	path = os.sep.join(path_list)
	ld_ud = os.listdir(path)
	for subpath in ld_ud:
		if subpath.strip(".") == "":
			continue
		path_list.append(subpath)
		path = os.sep.join(path_list)
		if not os.path.isdir(path):
			print(f"Not a directory: {repr(path)}")
		else:
			subpath_split = subpath.split("-")
			lang = subpath_split[0][3:]
			treebank = subpath_split[1][:]
			
			path_list_lang.append(f"UD_{lang}-UNION")
			path_lang = os.sep.join(path_list_lang)
			cmd_ = f"mkdir -p {path_lang}"
			os.system(cmd_)

			path_list.append("*.conllu")

			for conllu_path in glob.glob(os.sep.join(path_list)):
				conllu_path_short = conllu_path.split(os.sep)[-1]

				conllu_path = os.path.realpath(conllu_path)

				conllu_path_short_link = f"{subpath}_{conllu_path_short}"

				path_list_lang.append(conllu_path_short_link)
				conllu_path_long_link = os.path.realpath(os.sep.join(path_list_lang))

				cmd_ = f"ln -sf {repr(conllu_path)} {repr(conllu_path_long_link)}"

				print(cmd_)

				os.system(cmd_)

				path_list_lang.pop()

				# ========

				path_list_all.append(conllu_path_short_link)
				conllu_path_long_link = os.path.realpath(os.sep.join(path_list_all))

				cmd_ = f"ln -sf {repr(conllu_path)} {repr(conllu_path_long_link)}"

				print(cmd_)

				os.system(cmd_)

				path_list_all.pop()

			path_list.pop()

			path_list_lang.pop()
			
		path_list.pop()

	return 0

if __name__ == "__main__":
	exit(main())
