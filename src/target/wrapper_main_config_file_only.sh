#!/bin/bash
#SBATCH --begin=now
#SBATCH --comment="Pipeline to run STARK and diversutils on UD treebanks."
#SBATCH --cpus-per-task=16
#SBATCH --chdir=.
#SBATCH --job-name=delta_compute_diversities_config_file_only
#SBATCH --mail-user=louis.esteve@lisn.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=64g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal
#SBATCH --verbose
#SBATCH --time=24:00:00

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

set -eu

# ================ PLOT ================

plot=1

df1="INDEX_RICHNESS"
#df1="INDEX_TYPE_TOKEN_RATIO"
#df1="DISPARITY_PAIRWISE"
df2="INDEX_SHANNON_EVENNESS"
#df2="META_TOKEN"

df1_log=1
df2_log=0

# ================ DIVERSITY ================

compute_diversities=0

enable_disparities=0
tree_edit_distance_timeout=0.5

# ================ DEBUG ================

debug=0

if [[ ${debug} -eq 1 ]] ;
then
    python_debug_flag=""
else
    python_debug_flag="-OO"
fi

# ================ DATA ================

union="none"
#union="all"
#union="lang"

target_treebank="PUD"
#target_treebank="{SSJ,SST,Sequoia,Rhapsodie}"
#target_treebank="ParTUT"
#target_treebank="GSD"
#target_treebank="*"
target_subtreebank="all"

organization="UniversalDependencies"
#organization="UniversalDependencies-dev"
#organization="UppsalaNLP"
#organization="exp-fr"

if [[ "${organization}" = "UniversalDependencies" || "${organization}" = "UniversalDependencies-dev" ]] ;
then
    db_name_extra=${organization}
    is_macro_dir=0
    if [[ "${union}" = "lang" ]] ;
    then
        ud_path="data/target/union/${organization}/lang/*"
    elif [[ "${union}" = "all" ]] ;
    then
        ud_path="data/target/union/${organization}/UD_All_Languages-UNION"
    else
        if [[ ${debug} -eq 1 ]] ;
        then
            ud_path="data/input/${organization}/*French*${target_treebank} data/input/${organization}/*Slovenian*${target_treebank} data/input/${organization}/*English*${target_treebank}"
        else
            ud_path="data/input/${organization}/*${target_treebank}"
        fi
    fi
elif [[ "${organization}" = "UppsalaNLP" ]] ;
then
    db_name_extra="uppsalanlp"
    is_macro_dir=1
    if [[ "${union}" = "none" ]] ;
    then
        ud_multigenre_path="data/input/UppsalaNLP/UD-MULTIGENRE"
        #ud_path="${ud_multigenre_path}/train:dev/* ${ud_multigenre_path}/test/*"
        ud_path="${ud_multigenre_path}/union/*"
    else
        echo "Undefined behaviour: union mode ${union} for organization UppsalaNLP"
        exit 1
    fi
elif [[ "${organization}" = "exp-fr" ]] ;
then
    db_name_extra="exp-fr"
    is_macro_dir=0
    ud_path="data/input/${organization}"
else
    echo "Failed to recognize organization: ${organization}"
    exit 1
fi

echo "ud_path: ${ud_path}"

# ================ SLURM ================

# ---------------- CONFIG ----------------

# **************** CONFIG STARK ****************

array_config_stark_index=(a1 a2 b1 c1 c2 c3 c4 c5 d1 d2 d3 d4 d5 e1 e2 e3 e4 e5 c2small c2tiny c3small c3tiny c4small c4tiny)

cardinality_array_config_stark_index=${#array_config_stark_index[@]}

#config_stark_index=${array_config_stark_index[${SLURM_ARRAY_TASK_ID}]}
config_stark_index=${array_config_stark_index[$((${SLURM_ARRAY_TASK_ID} % ${cardinality_array_config_stark_index}))]}

config_stark_directory="config/stark"

cmd_="ls ${config_stark_directory}/${config_stark_index}_*.ini"
echo "cmd_: ${cmd_}"
config_stark_path=$(${cmd_})

# **************** CONFIG DIVERSUTILS ****************

config_diversutils_path=$1

if ! [[ -f "${config_diversutils_path}" ]] ;
then
    echo "config_diversutils_path (\"${config_diversutils_path}\") does not exist"
    exit 1
fi

# ---------------- TREEBANK ----------------

cmd_="ls -d ${ud_path}"
echo "cmd_: ${cmd_}"
array_treebank=($(${cmd_}))

if [[ "${organization}" = "UppsalaNLP" ]] ;
then
    echo "array_treebank: ${array_treebank}"
    
    cardinality_array_treebank=${#array_treebank[@]}
    
    treebank=${array_treebank[$(((${SLURM_ARRAY_TASK_ID} - (${SLURM_ARRAY_TASK_ID} % ${cardinality_array_config_stark_index})) / ${cardinality_array_config_stark_index}))]}
    
    treebank_genre=$(echo ${treebank} | cut -d "/" -f 6)
    db_name_extra="${db_name_extra} ${treebank_genre}"
    ud_path=${treebank}
fi

# ================ LOGGING ================

echo "======== SLURM_JOB_ID: ${SLURM_JOB_ID} ========"
echo "SLURM_ARRAY_JOB_ID: ${SLURM_ARRAY_JOB_ID}"
echo "SLURM_ARRAY_TASK_ID: ${SLURM_ARRAY_TASK_ID}"
echo "SLURM_ARRAY_TASK_COUNT: ${SLURM_ARRAY_TASK_COUNT}"
echo "SLURM_ARRAY_TASK_MIN: ${SLURM_ARRAY_TASK_MIN}"
echo "SLURM_ARRAY_TASK_MAX: ${SLURM_ARRAY_TASK_MAX}"
echo "cardinality_array_config_stark_index: ${cardinality_array_config_stark_index}"
if [[ "${organization}" = "UppsalaNLP" ]] ;
then
    echo "cardinality_array_treebank: ${cardinality_array_treebank}"
fi
echo "compute_diversities: ${compute_diversities}"
echo "config_stark_directory: ${config_stark_directory}"
echo "config_stark_index: ${config_stark_index}"
echo "config_stark_path: ${config_stark_path}"
echo "config_diversutils_path: ${config_diversutils_path}"
echo "db_name_extra: ${db_name_extra}"
echo "df1: ${df1}"
echo "df1_log: ${df1_log}"
echo "df2: ${df2}"
echo "df2_log: ${df2_log}"
echo "debug: ${debug}"
echo "enable_disparities: ${enable_disparities}"
echo "is_macro_dir: ${is_macro_dir}"
echo "organization: ${organization}"
echo "plot: ${plot}"
echo "target_subtreebank: ${target_subtreebank}"
echo "target_treebank: ${target_subtreebank}"
if [[ "${organization}" = "UppsalaNLP" ]] ;
then
    echo "treebank: ${treebank}"
fi
echo "tree_edit_distance_timeout: ${tree_edit_distance_timeout}"
echo "ud_path: ${ud_path}"
echo "union: ${union}"

# ================ COMPUTATION ================

if [[ ${compute_diversities} -eq 1 ]] ;
then
    echo "-------- COMPUTING DIVERSITIES --------"
    cmd_="python3 ${python_debug_flag} src/target/main.py --extract-lang 1 --ud_path ${ud_path} --config_stark_path ${config_stark_path} --config_diversutils_path ${config_diversutils_path} --config_file_only 1 --threads ${SLURM_CPUS_PER_TASK} --enable_disparities ${enable_disparities} --tree_edit_distance_timeout ${tree_edit_distance_timeout} --target_subtreebank ${target_subtreebank} --db_name_extra ${db_name_extra} --is_macro_dir ${is_macro_dir}"
    echo "cmd_: ${cmd_}"
    ${cmd_}
fi

# ================ VISUALISATION ================

if [[ ${plot} -eq 1 ]] ;
then
    echo "-------- PLOTTING --------"
    
    for error_bars in 0 1 ;
    do
        for plot_mode in tag lang ;
        do
    #        if [[ "${plot_mode}" = "lang" ]] ; then
    #            split_by_starting_char=1
    #        else
    #            split_by_starting_char=0
    #        fi
            for split_by_starting_char in 0 1 ;
            do
                cmd_="python3 ${python_debug_flag} src/analysis/plot_variety_balance.py --config_path ${config_stark_path} --config_file_only 1 --subtreebank ${target_subtreebank} --div_name ${df1} ${df2} --div_log ${df1_log} ${df2_log} --error_bars ${error_bars} --plot_mode ${plot_mode} --split_by_starting_char ${split_by_starting_char} --db_name_extra ${db_name_extra}"
                echo "cmd_: ${cmd_}"
                ${cmd_}
            done
        done
    done
fi

