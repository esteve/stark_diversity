#!/bin/bash
#SBATCH --begin=now
#SBATCH --comment="Pipeline to run STARK and diversutils on UD treebanks."
#SBATCH --cpus-per-task=16
#SBATCH --chdir=.
#SBATCH --job-name=delta_compute_diversities
#SBATCH --mail-user=louis.esteve@universite-paris-saclay.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=64g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=low
#SBATCH --verbose
#SBATCH --time=12:00:00

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

#ts_min_array=(1 1 1 1)
#ts_max_array=(5 10 50 100)
ts_min_array=(1)
ts_max_array=(1)
#ts_min_array=(1)
#ts_max_array=(100000)

ts_max=${ts_max_array[${SLURM_ARRAY_TASK_ID}]}
ts_min=${ts_min_array[${SLURM_ARRAY_TASK_ID}]}

#head="deprel=root"
#head="upos=NOUN"
head="None"
#node_type="lemma+upos"
#node_type="upos"
#node_type="deprel"
#node_type="deprel+upos"
#node_type="lemma"
#node_type="form"
node_type="upos+feats"
#node_type="deprel+upos"
#node_type="deprel"

target_subtreebank="all"

#complete_tree_type=1
complete_tree_type=0

enable_disparities=0
tree_edit_distance_timeout=0.5

union="none"
#union="all"
#union="lang"

debug=1

if [[ "${union}" = "lang" ]] ;
then
    ud_path="data/target/union/UniversalDependencies/lang/*"
elif [[ "${union}" = "all" ]] ;
then
    ud_path="data/target/union/UniversalDependencies/UD_All_Languages-UNION"
else
    if [[ ${debug} -eq 1 ]] ;
    then
        ud_path="data/input/UniversalDependencies/*French* data/input/UniversalDependencies/*Slovenian*"
    else
        ud_path="data/input/UniversalDependencies/*"
    fi
fi

echo "======== SLURM_JOB_ID: ${SLURM_JOB_ID} ========"
set -eu
echo "SLURM_ARRAY_JOB_ID: ${SLURM_ARRAY_JOB_ID}"
echo "SLURM_ARRAY_TASK_ID: ${SLURM_ARRAY_TASK_ID}"
echo "SLURM_ARRAY_TASK_COUNT: ${SLURM_ARRAY_TASK_COUNT}"
echo "SLURM_ARRAY_TASK_MIN: ${SLURM_ARRAY_TASK_MIN}"
echo "SLURM_ARRAY_TASK_MAX: ${SLURM_ARRAY_TASK_MAX}"
echo "ts_min: ${ts_min}"
echo "ts_max: ${ts_max}"
echo "head: ${head}"
echo "node_type: ${node_type}"
echo "target_subtreebank: ${target_subtreebank}"
echo "complete_tree_type: ${complete_tree_type}"

cmd_="python3 -OO src/target/main.py --tree_size_min ${ts_min} --tree_size_max ${ts_max} --head ${head} --node_type ${node_type} --is_macro_dir 0 --ud_path ${ud_path} --extract-lang 1 --config_path config_UniDive-WG4_full-sentences.ini --threads ${SLURM_CPUS_PER_TASK} --enable_disparities ${enable_disparities} --tree_edit_distance_timeout ${tree_edit_distance_timeout} --target_subtreebank ${target_subtreebank} --complete_tree_type ${complete_tree_type}"
echo "cmd_: ${cmd_}"
${cmd_}

