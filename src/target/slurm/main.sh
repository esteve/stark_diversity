#!/usr/bin/env sh

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#


if [ -z "${debug}"                      ] ; then debug=0                                                              ; fi
if [ -z "${organization}"               ] ; then organization="UniversalDependencies"                                 ; fi
if [ -z "${ud_path}"                    ] ; then ud_path=""                                                           ; fi
if [ -z "${ud_path_base}"               ] ; then ud_path_base=""                                                      ; fi
if [ -z "${ud_path_extension}"          ] ; then ud_path_extension=""                                                 ; fi
if [ -z "${is_macro_dir}"               ] ; then is_macro_dir=1                                                       ; fi
if [ -z "${extract_lang}"               ] ; then extract_lang=1                                                       ; fi
if [ -z "${db_name_extra}"              ] ; then db_name_extra=""                                                     ; fi
if [ -z "${partition}"                  ] ; then partition="all"                                                      ; fi
if [ -z "${qos}"                        ] ; then qos="default"                                                        ; fi
if [ -z "${gpus}"                       ] ; then gpus=0                                                               ; fi
if [ -z "${time}"                       ] ; then time="1-00:00:00"                                                    ; fi
if [ -z "${min_index}"                  ] ; then min_index=0                                                          ; fi
if [ -z "${max_index}"                  ] ; then max_index=0                                                          ; fi
if [ -z "${config_file_only}"           ] ; then config_file_only=1                                                   ; fi
if [ -z "${config_path_diversutils}"    ] ; then config_path_diversutils="config/diversutils/A1_linear_mt_exp-fr.ini" ; fi
if [ -z "${config_path_sampling}"       ] ; then config_path_sampling="config/sampling/gradient_based/base.ini"       ; fi
if [ -z "${tree_edit_distance_timeout}" ] ; then tree_edit_distance_timeout=0.05                                      ; fi
if [ -z "${target_subtreebank}"         ] ; then target_subtreebank="all"                                             ; fi
if [ -z "${threads}"                    ] ; then threads=-1                                                           ; fi
if [ -z "${save_mode}"                  ] ; then save_mode="none"                                                     ; fi
if [ -z "${pattern_treebank}"           ] ; then pattern_treebank="^.*$"                                              ; fi
if [ -z "${pattern_lang}"               ] ; then pattern_lang="^.*$"                                                  ; fi
if [ -z "${action}"                     ] ; then action="bulk_diversity_computation"                                  ; fi
if [ -z "${batch_size}"                 ] ; then batch_size=64                                                        ; fi
if [ -z "${token_only}"                 ] ; then token_only=0                                                         ; fi

set -eu

if [ ${debug} -eq 0            ] ; then export PYTHONOPTIMIZE=1  ; else export PYTHONOPTIMIZE=0                                          ; fi
if [ -z "${db_name_extra}"     ] ; then arg_db_name_extra=""     ; else arg_db_name_extra="--db_name_extra ${db_name_extra}"             ; fi
if [ -z "${ud_path}"           ] ; then arg_ud_path=""           ; else arg_ud_path="--ud_path ${ud_path}"                               ; fi
if [ -z "${ud_path_base}"      ] ; then arg_ud_path_base=""      ; else arg_ud_path_base="--ud_path_base ${ud_path_base}"                ; fi
if [ -z "${ud_path_extension}" ] ; then arg_ud_path_extension="" ; else arg_ud_path_extension="--ud_path_extension ${ud_path_extension}" ; fi
if [ -z "${organization}"      ] ; then arg_organization=""      ; else arg_organization="--organization ${organization}"                ; fi

export TQDM_DISABLE=1

cmd_="sbatch --array=${min_index}-${max_index} \
	--verbose \
	--partition=${partition} \
	--qos=${qos} \
	--time=${time} \
	--gpus-per-node=${gpus} \
	stark_diversutils/__main__.py \
	--action ${action} \
	${arg_organization} \
	${arg_ud_path} \
	${arg_ud_path_base} \
	${arg_ud_path_extension} \
	--is_macro_dir ${is_macro_dir} \
	--extract_lang ${extract_lang} \
	${arg_db_name_extra} \
	--config_path_diversutils ${config_path_diversutils} \
	--config_path_sampling ${config_path_sampling} \
	--config_file_only ${config_file_only} \
	--target_subtreebank ${target_subtreebank} \
	--tree_edit_distance_timeout ${tree_edit_distance_timeout} \
	--threads ${threads} \
	--save_mode ${save_mode} \
	--pattern_treebank ${pattern_treebank} \
	--pattern_lang ${pattern_lang} \
	--batch_size ${batch_size} \
	--token_only ${token_only}
"
${cmd_}

