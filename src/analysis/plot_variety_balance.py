#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import os
import numpy as np
import matplotlib.pyplot as plt 
import sqlite3 as sql
import argparse
import stark
import pandas as pd
import re
import logging
import math

logging.getLogger("matplotlib").setLevel(logging.WARNING)

MARKERS = ".,ov^<>12348spP*hH+xXDd|_"
MARKERSIZE = 1024
GOLDEN_RATIO = (1.0 + math.sqrt(5.0)) / 2.0
FIGSIZE_DIM0 = 8.0
FIGSIZE_DIM1 = FIGSIZE_DIM0 / 1.618
FIGSIZE = (FIGSIZE_DIM0, FIGSIZE_DIM1)
LEGEND_NCOLS = 4
LEGEND_LOCATION = "lower center"

def prepare_wals_language_reference(branch):
	branch = branch.replace("/", "").replace(" ", "-").replace(".", "-")
	url_languages = f"https://github.com/cldf-datasets/wals/raw/{branch}/cldf/languages.csv"

	path_language_list = ["data", "meta", "languages", branch]
	path_language = os.sep.join(path_language_list)
	cmd_ = f"set -eu ; mkdir -p {repr(path_language)}"
	prcs_res = os.system(cmd_)
	if prcs_res != 0:
		raise Exception("Failed to call: {repr(cmd_)}")

	path_language_list.append("languages.csv")
	path_language = os.sep.join(path_language_list)
	if not (os.path.exists(path_language) and os.path.isfile(path_language)):
		cmd_ = f"set -eu ; wget -O {repr(path_language)} {repr(url_languages)}"
		prcs_res = os.system(cmd_)
		if prcs_res != 0:
			raise Exception("Failed to call: {repr(cmd_)}")
		print(f"Downloaded {repr(url_languages)} to {repr(path_language)}")

	f = open(path_language, "rt", encoding="utf-8")
	df = pd.read_csv(f)
	f.close()

	return df

def _wrapper_get_language_attribute():
	branch_wise_df = {}

	def get_language_attribute(key, branch="master"):
		if branch not in branch_wise_df:
			branch_wise_df[branch] = prepare_wals_language_reference(branch)
		# ...

def main_udm(db_path_list, div_names, div_log, treebank_filter=".*"):
	fig, ax = plt.subplots(figsize=FIGSIZE)

	marker_count = 0

	config_index = None

	colours = plt.cm.tab20(np.linspace(0, 1, len(db_path_list)))

	colour_index = 0

	for db_path in db_path_list:
		colour = colours[colour_index]

		sql_conn = sql.connect(db_path)
		sql_conn.create_function("REGEXP", 2, lambda x,y: 0 if re.search(x, y) == None else 1)

		sql_curs = sql_conn.cursor()

		db_path_split = db_path.split(os.sep)[-1].split("_")
		config_index = db_path_split[1]
		if len(db_path_split) >= 3:
			db_name_extra = db_path_split[2].split("-")
		else:
			db_name_extra = ["undefined", "undefined"]
		organization = db_name_extra[0]
		tag = db_name_extra[1]
		if tag.endswith(".db"):
			tag = tag[:-3]

		print(tag.center(64, "="))

		x = {}
		y = {}

		for div_name, series in zip(div_names, [x, y]):
			for row in sql_curs.execute("""
				SELECT div_value, lang, treebank FROM diversities WHERE div_name=? AND treebank REGEXP ?;
			""", (div_name, treebank_filter)):
				div_value = row[0]
				lang = row[1]
				treebank = row[2]
			
				if lang not in series:
					series[lang] = {}
				series[lang][treebank] = div_value

		sql_conn.close()

		x_ = []
		y_ = []

		for lang in x.keys():
			if lang not in y.keys():
				continue

			for treebank in x[lang].keys():
				if treebank not in y[lang].keys():
					continue

				if x[lang][treebank] == None or y[lang][treebank] == None:
					continue

				x_.append(x[lang][treebank])
				y_.append(y[lang][treebank])

				#print(lang, treebank, x[lang][treebank], y[lang][treebank])

				ax.annotate(lang, (x[lang][treebank], y[lang][treebank]), color=colour)
				
		print(x_)
		#ax.scatter(x=x_, y=y_, label=tag, marker=MARKERS[marker_count], s=MARKERSIZE, color=colour)
		ax.scatter(x=x_, y=y_, label=None, marker=MARKERS[marker_count], s=MARKERSIZE, color=colour)
		#ax.scatter(x=np.mean(x_), y=np.mean(y_), label=f"{tag} cent.", marker=MARKERS[marker_count], s=MARKERSIZE * 10, color=colour)
		ax.scatter(x=np.mean(x_), y=np.mean(y_), label=tag, marker=MARKERS[marker_count], s=MARKERSIZE ** 1.5, color=colour)

		marker_count += 1
		marker_count %= len(MARKERS)

		colour_index += 1
			
	ax.grid()
	ax.set_xlabel(div_names[0])
	ax.set_ylabel(div_names[1])
	if div_log[0]:
		ax.set_xscale("log")
	if div_log[1]:
		ax.set_yscale("log")
	ax.legend(ncols=LEGEND_NCOLS, loc=LEGEND_LOCATION)
	plt.tight_layout()
	path_out = f"img{os.sep}plot_udm_{config_index}_df{div_names[0]}_df{div_names[1]}.png"
	print(f"path_out: {path_out}")
	fig.savefig(path_out)
	fig.clear()
	plt.close(fig)
		
	return 0
				

		
		

def main(ts_min=None, ts_max=None, subtreebank=None, complete_tree_type=None, root_whitelist=None, node_type=None, error_bars=None, plot_mode=None, split_by_starting_char=None, div_names=None, div_log=None, config_path=None, config_file_only=None, db_name_extra=[], treebank_filter=".*"):
	if plot_mode not in ["tag", "lang"]:
		print(f"plot_mode argument must be either \"tag\" or \"lang\"")
		return 1

	if len(div_names) != 2:
		print(f"div_names must be of length 2")
		return 1

	if complete_tree_type == None:
		complete_tree_type = 1

	if config_path != None:
		print(f"config_path: {config_path}")

		config = stark.read_settings(config_path, stark.parse_args([]))
		print(f"config: {repr(config)}")
		if ts_min == None or ts_max == None:
			ts = config["tree_size"]
			if "-" in ts:
				ts_min, ts_max = ts.split("-")
			else:
				ts_min = ts_max = int(ts)
		if ("complete" in config or "complete_tree_type" in config) and config_file_only:
			#complete_tree_type = 0 if config["complete"] in ["no", False, 0] else 1
                	complete_tree_type = 0 if (("complete" in config and config["complete"] in ["no", False, 0]) or ("complete_tree_type" in config and config["complete_tree_type"] in ["no", False, 0])) else 1
		if "root_whitelist" in config and config_file_only:
			root_whitelist = str(config["root_whitelist"])
		if "node_type" in config and config_file_only:
			node_type = config["node_type"]

	db_path_list = ["data", "target"]
	if config_file_only:
		config_index = config_path.split(os.sep)[-1].split("_")[0]
                #db_name_extra = [config_index] + db_name_extra
		db_path_list.append(f"diversities_{config_index}_{'-'.join(db_name_extra)}.db")
	else:
		db_path_list.append("diversities.db")
	db_path = os.sep.join(db_path_list)

	print(f"db_path: {db_path}")

	sql_conn = sql.connect(db_path)
	sql_conn.create_function("REGEXP", 2, lambda x,y: 0 if re.search(x, y) == None else 1)

	print(ts_min)
	print(ts_max)
	print(subtreebank)
	print(complete_tree_type)
	print(root_whitelist)
	print(node_type)

	if not (os.path.exists("img") and os.path.isdir("img")):
		os.mkdir("img")

	values = {}

	sql_curs = sql_conn.cursor()

	for div_name in div_names:
		if div_name.startswith("META_"):
			sql_r = sql_curs.execute("""
	SELECT t1.lang, t1.treebank, t1.tag, t2.div_value
	FROM   (SELECT * FROM ud_tags) AS t1
	       JOIN
	       (SELECT * FROM diversities WHERE div_name=? AND subtreebank=? AND treebank REGEXP ?) AS t2
	ON t1.lang=t2.lang AND t1.treebank=t2.treebank;""", (div_name, subtreebank, treebank_filter))
		else:
			sql_r = sql_curs.execute("""
	SELECT t1.lang, t1.treebank, t1.tag, t2.div_value
	FROM   (SELECT * FROM ud_tags) AS t1
	       JOIN
	       (SELECT * FROM diversities WHERE div_name=? AND div_subindex=0 AND ts_min=? AND ts_max=? AND subtreebank=? AND node_type=? AND complete_tree_type=? AND root_whitelist=? AND treebank REGEXP ?) AS t2
	ON t1.lang=t2.lang AND t1.treebank=t2.treebank;""", (div_name, ts_min, ts_max, subtreebank, node_type if node_type != None else "none", complete_tree_type, root_whitelist, treebank_filter))
		for row in sql_r:
			lang = row[0]
			treebank = row[1]
			tag = row[2]
			div_value = row[3]

			if div_value == None:
				continue

			if plot_mode == "tag":
				key = tag
			elif plot_mode == "lang":
				key = lang

			if key not in values:
				values[key] = {}
			if div_name not in values[key]:
				values[key][div_name] = {}
			values[key][div_name][treebank] = div_value

	print(f"found {len(values)} values")

	fig, ax = plt.subplots(figsize=FIGSIZE)
	marker_count = 0
	artists = []
	legend = []
	previous_starting_char = None
	for key in sorted(list(values.keys())):
		print(f"handling {key}")
		if split_by_starting_char and type(key) == str and len(key) > 0 and key[0] != previous_starting_char:
			marker_count = 0
			artists = []
			legend = []
			if previous_starting_char != None:
				ax.set_xlabel(div_names[0])
				ax.set_ylabel(div_names[1])
				if div_log[0]:
					ax.set_xscale("log")
				if div_log[1]:
					ax.set_yscale("log")
				"""
				ax.set_xlim(1, 1e6)
				ax.set_ylim(-0.05, 1.25)
				"""
				plt.grid()
				ax.legend(ncols=LEGEND_NCOLS, loc=LEGEND_LOCATION)
				plt.tight_layout()
				singlequote = "'"
				out_path = os.sep.join(["img", f"plot{'_' + config_index + '_' + '-'.join(db_name_extra) if config_file_only else ''}_{'alltreebanks' if treebank_filter == '.*' else treebank_filter}_{plot_mode}_df{div_names[0]}_df{div_names[1]}_startingchar{previous_starting_char}_errorbars{error_bars}.png"])
				fig.savefig(out_path)
				fig.clear()
				plt.close(fig)

				print(f"generated {repr(out_path)}")
			previous_starting_char = key[0]
			fig, ax = plt.subplots(figsize=FIGSIZE)
		try:
			if error_bars == 0:
				artist = ax.scatter(list(values[key][div_names[0]].values()), list(values[key][div_names[1]].values()), marker=MARKERS[marker_count], label=key)
			elif error_bars == 1:
				series_x = list(values[key][div_names[0]].values())
				series_y = list(values[key][div_names[1]].values())
				artist = ax.errorbar(x=np.mean(series_x), y=np.mean(series_y), xerr=np.std(series_x), yerr=np.std(series_y), marker=MARKERS[marker_count], label=key)
			artists.append(artist)
			legend.append(key)
			marker_count += 1
			marker_count %= len(MARKERS)
		except Exception as e:
			print(e)
	ax.set_xlabel(div_names[0])
	ax.set_ylabel(div_names[1])
	if div_log[0]:
		ax.set_xscale("log")
	if div_log[1]:
		ax.set_yscale("log")
	"""
	ax.set_xlim(1, 1e6)
	ax.set_ylim(-0.05, 1.25)
	"""
	plt.grid()
	ax.legend(ncols=LEGEND_NCOLS, loc=LEGEND_LOCATION)
	plt.tight_layout()
	singlequote = "'"
	out_path = os.sep.join(["img", f"plot{'_' + config_index + '_' + '-'.join(db_name_extra) if config_file_only else ''}_{'alltreebanks' if treebank_filter == '.*' else treebank_filter}_{plot_mode}_df{div_names[0]}_df{div_names[1]}_startingchar{previous_starting_char}_errorbars{error_bars}.png"])
	fig.savefig(out_path)
	fig.clear()
	plt.close(fig)

	print(f"generated {repr(out_path)}")

	sql_conn.close()

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("-a", "--ts_min", type=int, required=False)
	ap.add_argument("-b", "--ts_max", type=int, required=False)
	ap.add_argument("-c", "--complete_tree_type", type=int, required=False, default=1)
	ap.add_argument("-d", "--subtreebank", type=str, required=False, default="all")
	ap.add_argument("-e", "--root_whitelist", type=str, required=False, default="['deprel=root']")
	ap.add_argument("-f", "--node_type", type=str, required=False)
	ap.add_argument("-g", "--error_bars", type=int, required=False, default=0)
	ap.add_argument("-j", "--plot_mode", type=str, required=False, default="tag")
	ap.add_argument("-k", "--split_by_starting_char", type=int, required=False, default=0)
	ap.add_argument("-l", "--div_name", type=str, required=False, nargs="+", default=["INDEX_RICHNESS", "INDEX_SHANNON_EVENNESS"])
	ap.add_argument("-m", "--config_path", type=str, required=False)
	ap.add_argument("-n", "--config_file_only", type=int, required=False, default=0)
	ap.add_argument("-o", "--div_log", type=int, nargs="+", default=[0, 1])
	ap.add_argument("-p", "--db_name_extra", type=str, nargs="+", default=[])
	ap.add_argument("-q", "--mode", type=str, default="ud")
	ap.add_argument("-r", "--db_path", type=str, nargs="+")
	ap.add_argument("-t", "--treebank", type=str, default=".*")

	args = ap.parse_args()

	if args.mode == "ud":
		if args.config_file_only:
			exit(main(subtreebank=args.subtreebank, error_bars=args.error_bars, plot_mode=args.plot_mode, split_by_starting_char=args.split_by_starting_char, div_names=args.div_name, config_path=args.config_path, config_file_only=args.config_file_only, div_log=args.div_log, db_name_extra=args.db_name_extra, treebank_filter=args.treebank))
		else:
			exit(main(ts_min=args.ts_min, ts_max=args.ts_max, subtreebank=args.subtreebank, complete_tree_type=args.complete_tree_type, root_whitelist=args.root_whitelist, node_type=args.node_type, error_bars=args.error_bars, plot_mode=args.plot_mode, split_by_starting_char=args.split_by_starting_char, div_names=args.div_name, config_path=args.config_path, config_file_only=args.config_file_only, div_log=args.div_log, db_name_extra=args.db_name_extra, treebank_filter=args.treebank))
	elif args.mode == "udm":
		exit(main_udm(db_path_list=args.db_path, div_names=args.div_name, div_log=args.div_log, treebank_filter=args.treebank))
