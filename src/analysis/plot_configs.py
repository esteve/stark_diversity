#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import glob
import matplotlib.pyplot as plt
import os
import sqlite3 as sql
import re

MARKERS = ".,ov^<>12348spP*hH+xXDd|_"
FIGSIZE=(12, 12)

def main(organization, db_name_extra, div_names, div_log, lang_filter, treebank_filter):
	agg_db_name_extra = "-".join(db_name_extra)
	ptrn = os.sep.join(["data", "target", f"diversities_*{'_' + organization if organization != None else ''}{'_' + agg_db_name_extra if len(db_name_extra) > 0 else ''}.db"])

	print(f"ptrn: {repr(ptrn)}")

	x = {}
	y = {}

	marker_count = 0

	for db_path in glob.glob(ptrn):
		config_index = db_path.split(os.sep)[-1].split("_")[1]

		print(f"db_path: {repr(db_path)}")
		
		sql_conn = sql.connect(db_path)
		sql_conn.create_function("REGEXP", 2, lambda x,y: 0 if re.search(x, y) == None else 1)

		sql_curs = sql_conn.cursor()

		for div_name, series in zip(div_names, [x, y]):
			"""
			if config_index not in series:
				series[config_index] = {}
			"""

			for row in sql_curs.execute("""
				SELECT div_value, lang, treebank FROM diversities WHERE div_name=? AND lang REGEXP ? AND treebank REGEXP ?;
			""", (div_name, lang_filter, treebank_filter)):
				div_value = row[0]
				lang = row[1]
				treebank = row[2]

				#series[config_index][treebank] = div_value

				if treebank not in series:
					series[treebank] = {}
				series[treebank][config_index] = div_value

			

		sql_conn.close()

	fig, ax = plt.subplots(figsize=FIGSIZE)

	"""
	for config_index in x.keys():
		if config_index not in y.keys():
			continue
		for treebank in x[config_index].keys():
			if treebank not in y[config_index].keys():
				continue
			x_.append(x[config_index][treebank])
			y_.append(y[config_index][treebank])
	"""

	for treebank in x.keys():
		if treebank not in y.keys():
			continue

		x_ = []
		y_ = []

		for config_index in x[treebank].keys():
			if config_index not in y[treebank].keys():
				continue
			x_.append(x[treebank][config_index])
			y_.append(y[treebank][config_index])

			ax.annotate(config_index, (x[treebank][config_index], y[treebank][config_index]))

		ax.scatter(x=x_, y=y_, label=treebank, marker=MARKERS[marker_count])

		marker_count += 1
		marker_count %= len(MARKERS)

	path_out = f"img{os.sep}plot_cfg_lang-{lang_filter.replace('|', '-')}_org-{organization}_df{div_names[0]}_df{div_names[1]}.png"
	print(path_out)

	ax.grid()
	ax.legend()
	ax.set_xlabel(div_names[0])
	ax.set_ylabel(div_names[1])
	if div_log[0]:
		ax.set_xscale("log")
	if div_log[1]:
		ax.set_yscale("log")
	plt.tight_layout()

	fig.savefig(path_out)

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("-a", "--div_name", type=str, nargs="+", default=["INDEX_RICHNESS", "INDEX_SHANNON_EVENNESS"])
	ap.add_argument("-b", "--div_log", type=int, nargs="+", default=[1, 0])
	ap.add_argument("-l", "--lang", type=str, required=True)
	ap.add_argument("-t", "--treebank", type=str, required=True)
	ap.add_argument("-o", "--organization", type=str, default=None)
	ap.add_argument("-p", "--db_name_extra", type=str, nargs="+", default=[])

	args = ap.parse_args()

	exit(main(organization=args.organization, db_name_extra=args.db_name_extra, div_names=args.div_name, div_log=args.div_log, lang_filter=args.lang, treebank_filter=args.treebank))
