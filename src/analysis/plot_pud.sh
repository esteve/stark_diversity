#!/bin/sh
#SBATCH --begin=now
#SBATCH --comment="Pipeline to plot DUST's output."
#SBATCH --cpus-per-task=2
#SBATCH --chdir=.
#SBATCH --job-name=plot_pud
#SBATCH --mail-user=louis.esteve@lisn.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=8g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal
#SBATCH --verbose
#SBATCH --time=1:00:00
#SBATCH --array=0

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

set -eu

debug=0

df1="INDEX_RICHNESS"
#df1="INDEX_TYPE_TOKEN_RATIO"
#df2="ENTROPY_SHANNON_WEAVER"
df2="INDEX_SHANNON_EVENNESS"

df1_log=1
df2_log=0

org="UniversalDependencies"


array_config_path=($(ls config/stark/{a1,a2,b1,c1,c2,c3,c4,c5,d1,d2,d3,d4,d5,e1,e2,e3,e4,e5}_*.ini))
cardinality_array_config_path=${#array_config_path[@]}

if [ ${debug} -eq 0 ] ; then
	python_debug_flag="-OO"
else
	python_debug_flag=""
fi

i=0
while [[ ${i} -lt ${cardinality_array_config_path} ]] ;
do
	config_path=${array_config_path[${i}]}
	python3 ${python_debug_flag} src/analysis/plot_variety_balance.py \
		--div_name ${df1} ${df2} \
		--div_log ${df1_log} ${df2_log} \
		--treebank '^PUD$' \
		--mode "ud" \
		--db_path data/target/diversities_{a1,a2,b1,c1,c2,c3,c4,c5,d1,d2,d3,d4,d5,e1,e2,e3,e4,e5}_${org}.db \
		--config_path ${config_path} \
		--config_file_only 1 \
		--plot_mode "lang" \
		--subtreebank "all" \
		--error_bars 0 \
		--db_name_extra ${org}
	i=$((${i} + 1))
done
