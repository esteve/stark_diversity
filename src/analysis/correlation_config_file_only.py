#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import math
import os
import pandas as pd
import sqlite3 as sql
import scipy
import re

def diversity_value_is_usable(x):
	if x == None:
		return False
	if math.isnan(x):
		return False
	if math.isinf(x):
		return False
	return True

def parse_config_equations(config_equations):
	# parse config_equations
	configs = set()
	for i in range(len(config_equations)):
		config_equations[i] = config_equations[i].replace("+", " + ").replace("-", " - ")
		equation = config_equations[i]
		"""
		for a in equation.split("+"):
			for b in a.split("-"):
				configs |= {b}
		"""
		for a in equation.split(" "):
			if a == "" or a == "+" or a == "-":
				continue
			configs |= {a}
	configs = sorted(list(configs))
	print(configs)
	return configs

def main(config_equations, diversity_function, lang_filter, treebank_filter, genre_positive_filter, genre_negative_filter, correlation_object, organization):
	if correlation_object == "config":
		return correlation_between_configurations(config_equations=config_equations, diversity_function=diversity_function, lang_filter=lang_filter, treebank_filter=treebank_filter, genre_positive_filter=genre_positive_filter, genre_negative_filter=genre_negative_filter, organization=organization)
	elif correlation_object == "function":
		return correlation_between_functions(config_equations=config_equations, diversity_function=diversity_function, lang_filter=lang_filter, treebank_filter=treebank_filter, genre_positive_filter=genre_positive_filter, genre_negative_filter=genre_negative_filter, organization=organization)
	else:
		raise Exception("Unknown correlation_object: {repr(correlation_object)}")

def correlation_between_functions(config_equations, diversity_function, lang_filter, treebank_filter, genre_positive_filter, genre_negative_filter, organization):
	configs = parse_config_equations(config_equations)

	db_content = {}

	db_path = os.sep.join(["data", "target", f"diversities{'_' + organization if organization != 'none' else ''}.db"])

	if not (os.path.exists(db_path) and os.path.isfile(db_path)):
		raise Exception(f"Does not exist: {repr(db_path)}")

	sql_conn = sql.connect(db_path)

	sql_conn.create_function("REGEXP", 2, lambda x,y: 0 if re.search(x, y) == None else 1)

	sql_curs_df = sql_conn.cursor()

	for row_df in sql_curs_df.execute("""
		SELECT DISTINCT div_name, div_subindex, alpha, beta FROM diversities ORDER BY div_name, div_subindex;
	"""):
		print(row_df)
		div_name = row_df[0]
		div_subindex = row_df[1]
		alpha = row_df[2]
		beta = row_df[3]

		qry_args = [div_name, div_subindex]
		if alpha != None:
			qry_args.append(alpha)
		if beta != None:
			qry_args.append(beta)

		sql_curs_values = sql_conn.cursor()
		local_results = {}

		for row_values in sql_curs_values.execute(f"""
			SELECT * FROM diversities WHERE div_name=? AND div_subindex=?{' AND alpha=?' if alpha != None else ''}{' AND beta=?' if beta != None else ''};
		""", tuple(qry_args)):
			#print(row_values)
			div_name = row_values[0]
			div_subindex = row_values[1]
			div_value = row_values[2]
			alpha = row_values[3]
			beta = row_values[4]
			lang = row_values[5]
			treebank = row_values[6]
			subtreebank = row_values[7]
			ts_min = row_values[8]
			ts_max = row_values[9]
			node_type = row_values[10]
			complete_tree_type = row_values[11]
			root_whitelist = row_values[12]

			key = "/".join([str(x) for x in [lang, treebank, subtreebank, ts_min, ts_max, node_type, complete_tree_type, root_whitelist]])

			local_results[key] = div_value

		#print(local_results)

		db_content[f"{div_name}-{div_subindex}-{'none' if alpha == None else format(alpha, '.3f')}-{'none' if beta == None else format(beta, '.3f')}"] = local_results

	sql_conn.close()

	correlation_computation(db_content)

	return 0

def correlation_between_configurations(config_equations, diversity_function, lang_filter, treebank_filter, genre_positive_filter, genre_negative_filter, correlation, organization):
	configs = parse_config_equations(config_equations)

	db_content = {}

	qry_f = open(os.sep.join(["src", "sql", "query_correlation_config_file_only.sql"]), "rt", encoding="utf-8")
	qry_s = qry_f.read()
	qry_f.close()

	for config_index in configs:
		#config_path = glob.glob(os.sep.join(["config", f"{config_index}_*.ini"]))[0]
		db_path = os.sep.join(["data", "target", f"diversities_{config_index}{'_' + organization if organization != 'none' else ''}.db"])

		sql_conn = sql.connect(db_path)

		sql_conn.create_function("REGEXP", 2, lambda x,y: 0 if re.search(x, y) == None else 1)

		sql_curs = sql_conn.cursor()

		local_results = {}

		for row in sql_curs.execute(qry_s, (diversity_function, lang_filter, treebank_filter, genre_positive_filter, genre_negative_filter)):
			div_name = row[0]
			div_subindex = row[1]
			div_value = row[2]
			alpha = row[3]
			beta = row[4]
			lang = row[5]
			treebank = row[6]
			subtreebank = row[7]
			ts_min = row[8]
			ts_max = row[9]
			node_type = row[10]
			complete_tree_type = row[11]
			root_whitelist = row[12]

			key = "/".join([str(alpha), str(beta), lang, treebank, subtreebank])

			local_results[key] = div_value

		db_content[config_index] = local_results

		sql_conn.close()

	correlation_computation(db_content)

	return 0

def correlation_computation(db_content):
	results = {"pearson_r": {}, "pearson_p": {}, "pearson_d": {}, "pearson_s": {}, "spearman_r": {}, "spearman_p": {}, "spearman_d": {}, "spearman_s": {}}

	db_content_keys = list(db_content.keys())

	for i in range(len(db_content_keys)):
		config_index_a = db_content_keys[i]
		if "GOOD" in config_index_a or "HILL" in config_index_a or "STIRLING" in config_index_a:
			continue
		print(config_index_a)
		for j in range(i+1, len(db_content_keys)):
			"""
			config_index_a = configs[i]
			config_index_b = configs[j]
			"""

			config_index_b = db_content_keys[j]
			if "GOOD" in config_index_b or "HILL" in config_index_b or "STIRLING" in config_index_b:
				continue

			print(config_index_a, repr(db_content[config_index_a])[:100], "VS", config_index_b, repr(db_content[config_index_b])[:100])

			"""
			config_equation_index_a = config_equations[i]
			config_equation_index_b = config_equations[j]
			"""

			intersect_db_content_a = {}
			intersect_db_content_b = {}

			for key, value in db_content[config_index_a].items():
				if not diversity_value_is_usable(value):
					continue
				if key in db_content[config_index_b] and diversity_value_is_usable(db_content[config_index_b][key]):
					intersect_db_content_a[key] = value
			for key, value in db_content[config_index_b].items():
				if not diversity_value_is_usable(value):
					continue
				if key in db_content[config_index_a] and diversity_value_is_usable(db_content[config_index_a][key]):
					intersect_db_content_b[key] = value

			intersect_db_content_a_list = list(intersect_db_content_a.items())
			intersect_db_content_b_list = list(intersect_db_content_b.items())

			intersect_db_content_a_list = list(sorted(intersect_db_content_a_list, key=lambda x:x[0]))
			intersect_db_content_b_list = list(sorted(intersect_db_content_b_list, key=lambda x:x[0]))

			intersect_db_content_a_list_values = [x[1] for x in intersect_db_content_a_list]
			intersect_db_content_b_list_values = [x[1] for x in intersect_db_content_b_list]

			if __debug__:
				#print(intersect_db_content_a_list_values, intersect_db_content_b_list_values)
				print(len(intersect_db_content_a_list_values), len(intersect_db_content_b_list_values))

			if len(intersect_db_content_a_list_values) == len(intersect_db_content_b_list_values) and len(intersect_db_content_a_list_values) >= 2:
				pearson_r, pearson_p = scipy.stats.pearsonr(intersect_db_content_a_list_values, intersect_db_content_b_list_values)
				spearman_r, spearman_p = scipy.stats.spearmanr(intersect_db_content_a_list_values, intersect_db_content_b_list_values)
				#print(f"{config_index_a:<16} / {config_index_b:<16} -- r={pearson_r:+.3f} p={pearson_p:.3e} -- r={spearman_r:+.3f} p={spearman_p:.3e}")
	
				result_key = f"{config_index_a} / {config_index_b}"
	
				results["pearson_r"][result_key] = pearson_r
				results["pearson_p"][result_key] = pearson_p
				results["pearson_d"][result_key] = "Negative" if pearson_r < 0.0 else ("Positive" if pearson_r > 0.0 else "Neutral")
				results["pearson_s"][result_key] = "Extremely significant" if pearson_p < 1e-3 else ("Very significant" if pearson_p < 1e-2 else ("Significant" if pearson_p < 5e-2 else "Not significant"))
				results["spearman_r"][result_key] = spearman_r
				results["spearman_p"][result_key] = spearman_p
				results["spearman_d"][result_key] = "Negative" if spearman_r < 0.0 else ("Positive" if spearman_r > 0.0 else "Neutral")
				results["spearman_s"][result_key] = "Extremely significant" if spearman_p < 1e-3 else ("Very significant" if spearman_p < 1e-2 else ("Significant" if spearman_p < 5e-2 else "Not significant"))
		

	pd.set_option("display.max_rows", None)
	df = pd.DataFrame(results)
	print(df)

	df.to_csv(os.sep.join(["data", "target", "correlation_output.tsv"]), sep="\t")

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("-a", "--genre_positive", type=str, default=".*")
	ap.add_argument("-b", "--genre_negative", type=str, default="^$")
	#ap.add_argument("-c", "--config", type=str, nargs="+", default=["a1", "a2", "b1", "c1", "c2", "c3", "c4", "c4small", "c5", "d1", "d2", "d3", "d4", "d5", "e1", "e2", "e3", "e4", "e5"])
	#ap.add_argument("-c", "--config", type=str, nargs="+", default=["a1", "b1", "c1", "c2", "c3", "c4", "c5", "d1", "d2", "d3", "d4", "d5", "e1", "e2", "e3", "e4", "e5"])
	ap.add_argument("-c", "--config_equation", type=str, nargs="+", default=["a1", "b1", "c1", "c2", "c3", "c4", "c5", "d1", "d2", "d3", "d4", "d5"])
	ap.add_argument("-d", "--diversity_function", type=str, default="INDEX_RICHNESS")
	ap.add_argument("-e", "--correlation_object", type=str, default="config")
	ap.add_argument("-l", "--lang", type=str, default=".*")
	ap.add_argument("-t", "--treebank", type=str, default=".*")
	ap.add_argument("-o", "--organization", type=str, default="none")

	args = ap.parse_args()

	exit(main(config_equations=args.config_equation, diversity_function=args.diversity_function, lang_filter=args.lang, treebank_filter=args.treebank, genre_positive_filter=args.genre_positive, genre_negative_filter=args.genre_negative, correlation_object=args.correlation_object, organization=args.organization))
