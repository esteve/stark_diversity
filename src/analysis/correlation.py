#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import sqlite3 as sql
import scipy



def main():
    sql_conn = sql.connect("data/target/diversities.db")

    sql_curs = sql_conn.cursor()

    f = open("src/sql/query_correlation.sql", "rt", encoding="utf-8")
    q = f.read()
    f.close()

    diversities = {}

    n = 0
    for row in sql_curs.execute(q):
        #print(row)
        #exit()
        """
        if n > 16000:
            break
        """
        key = tuple(row[:3])
        if key not in diversities:
            diversities[key] = []
        diversities[key].append(row[-1])
        n += 1

    print("n:", n)

    for a in diversities:
        for b in diversities:
            if len(diversities[a]) != len(diversities[b]):
                #print("unequal length", a, b, len(diversities[a]), len(diversities[b]))
                continue
            try:
                corr, p = scipy.stats.pearsonr(diversities[a], diversities[b])
                #if p < 0.05:
                #    print(a, b, corr, p)
                if p < 0.05:
                    print(a, b, corr, p, "<====")
                else:
                    print(a, b, corr, p)
            except Exception as e:
                """
                print(e)
                print(diversities[a])
                print(diversities[b])
                break
                """
                pass

    sql_conn.close()
    return 0

if __name__ == "__main__":
    exit(main())
