#!/usr/bin/env python3
#SBATCH --begin=now
#SBATCH --comment="Pipeline to evaluate diversity of random blocks of data."
#SBATCH --cpus-per-task=4
#SBATCH --chdir=.
#SBATCH --job-name=evalutate_diversity_small_random_sampling
#SBATCH --mail-user=louis.esteve@lisn.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=32g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#


import argparse
import numpy as np
import os
import glob
import conllu
import diversutils
import time
import scipy
import matplotlib.pyplot as plt
import logging
from collections import Counter

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG if __debug__ else logging.INFO)

def main(seeds, treebank, language, conllu_key="form", alpha_min=0.0, alpha_max=3.0, alpha_step=0.1):
	all_alphas = np.linspace(alpha_min, alpha_max, int(np.ceil((alpha_max - alpha_min) / alpha_step) + 1))

	path_list = ["data", "input", "evaluation_small_random_sampling"]
	path = os.sep.join(path_list)
	os.makedirs(path, exist_ok=True)

	ud_name = f"UD_{language}-{treebank}"
	path_list.append(ud_name)
	path = os.sep.join(path_list)
	if not os.path.isdir(path):
		cmd_ = f"git clone https://github.com/UniversalDependencies/{ud_name} {repr(path)}"
	else:
		#cmd_ = f"set -eu ; wd_=$(pwd) ; cd {repr(path)} ; git pull ; cd $wd_"
		cmd_ = None

	if cmd_ != None:
		print(f"cmd_: {cmd_}")
		prcs_res = os.system(cmd_)
	
		if prcs_res != 0:
			raise Exception(f"Call failed (exit status: {prcs_res}): {repr(cmd_)}")

	#path_list += ["**", "*.conllu"]
	path_list += [".", "*.conllu"]
	path = os.sep.join(path_list)

	print(f"Searching: {repr(path)}")

	all_sentences = []
	for conllu_path in glob.glob(path):
		print(conllu_path)

		f = open(conllu_path, "rt", encoding="utf-8")
		#all_sentences += conllu.parse(f.read())
		all_sentences += [Counter(tok[conllu_key] for tok in sent) for sent in conllu.parse_incr(f)]
		f.close()

	print(f"Number of sentences to select from: {len(all_sentences)}")

	fig, ax = plt.subplots(figsize=(8,8))

	colours = ["blue", "red", "green", "yellow", "orange", "brown", "pink"]

	colour_count = 0

	"""
	for num_sentences in array_num_sentences:
		print(f" num_sentences: {num_sentences} ".center(64, "="))

		if num_sentences > len(all_sentences):
			raise Exception(f"num_sentences ({num_sentences}) > len(all_sentences) ({len(all_sentences)})")
	"""
	
	h_array = np.zeros((len(all_alphas), len(seeds)), dtype=np.float64)
	m_array = np.zeros(len(seeds), dtype=np.float64)
	n_array = np.zeros(len(seeds), dtype=np.float64)
	array_num_sentences = np.zeros(len(seeds), dtype=np.int64)

	for seed_count, seed in enumerate(seeds):
		t_start = time.time()

		np.random.seed(seed)
		#chosen_indices = np.random.shuffle(np.arange(len(all_sentences)))[:num_sentences]
		chosen_indices = np.arange(len(all_sentences))
		np.random.shuffle(chosen_indices)
		num_sentences = 1 + int(np.floor(np.random.random() * (len(all_sentences) - 1)))
		#array_num_sentences.append(num_sentences)
		array_num_sentences[seed_count] = num_sentences
		chosen_indices = chosen_indices[:num_sentences]
		
		m = 0
		n_set = set()

		work_counter = Counter()
		for index in chosen_indices:
			work_counter += all_sentences[index]
			m += sum(all_sentences[index].values())
			n_set |= set(all_sentences[index].keys())

		n = len(n_set)

		g_index = diversutils.create_empty_graph(0, 0)

		for key, value in work_counter.items():
			diversutils.add_node(g_index, value)
		
		diversutils.compute_relative_proportion(g_index)

		for alpha_count, alpha in enumerate(all_alphas):
			h = diversutils.individual_measure(g_index, diversutils.DF_ENTROPY_RENYI, alpha)[0]	
			h_array[alpha_count, seed_count] = h
		m_array[seed_count] = m
		n_array[seed_count] = n

		t_end = time.time()

		t_delta = t_end - t_start

		print(f"{seed:>8}: {h:.4f} ({t_delta:.3f}s)")

	#h_array = np.array(h_array)

	mu = np.mean(h_array)
	sigma = np.std(h_array)
	min_ = np.min(h_array)
	max_ = np.max(h_array)

	print(f"mu: {mu:.3f}")
	print(f"sigma: {sigma:.3e}")
	print(f"min_: {min_:.3f}")
	print(f"max_: {max_:.3f}")

	#v, p = scipy.stats.normaltest(h_array)
	#print(f"{v:.3f}; {p:.3f}")

	m_log_array = np.log(m_array)
	n_log_array = np.log(n_array)

	alpha_index = 10

	alpha = all_alphas[alpha_index]

	#ax.scatter([num_sentences] * len(h_array), h_array, color=colours[colour_count], label=f"{num_sentences} sent.")
	ax.scatter(array_num_sentences, h_array[alpha_index], s=1.0, color="blue", marker=".")
	ax.scatter(array_num_sentences, m_log_array, s=1.0, color="red", marker="d")
	ax.scatter(array_num_sentences, n_log_array, s=1.0, color="green", marker="+")

	colour_count += 1
	colour_count %= len(colours)

	plt.title(f"{language} / {treebank} / {len(seeds)} seeds / {conllu_key} / " + "$\\alpha=" + f"{alpha:.3f}$")
	ax.set_xlabel("Number of sentences selected")
	ax.set_ylabel(r"$H_{\alpha=" + f"{alpha:.3f}" + "}$")
	plt.tight_layout()

	fig.savefig(f"plot_evaluate_small_random_sampling_{language}_{treebank}_{len(seeds)}seeds_{conllu_key}_alpha{alpha:.3f}.png")

	# ================

	fig, ax = plt.subplots(figsize=(8,8))

	ratio_h_m_log_mu_array = np.zeros(len(all_alphas), dtype=np.float64)
	ratio_h_m_log_sigma_array = np.zeros(len(all_alphas), dtype=np.float64)
	ratio_h_n_log_mu_array = np.zeros(len(all_alphas), dtype=np.float64)
	ratio_h_n_log_sigma_array = np.zeros(len(all_alphas), dtype=np.float64)

	for alpha_count, alpha in enumerate(all_alphas):
		names = [f"h_{alpha:.3f}", "m_log", "n_log"]
		arrays = [h_array[alpha_count], m_log_array, n_log_array]
		#for name_a, array_a in zip(names, arrays):
		for name_a, array_a in zip(names[:1], arrays[:1]):
			#for name_b, array_b in zip(names, arrays):
			for name_b, array_b in zip(names[1:], arrays[1:]):
				ratio_array = array_a / array_b
				mu_local = np.mean(ratio_array)
				sigma_local = np.std(ratio_array)
	
				print(f"{name_a:>8} / {name_b:>8} | mu={mu_local:.3f}; sigma={sigma_local:.3e}")

				if name_a.startswith("h_"):
					if name_b.startswith("m_log"):
						ratio_h_m_log_mu_array[alpha_count] = mu_local
						ratio_h_m_log_sigma_array[alpha_count] = sigma_local
					elif name_b.startswith("n_log"):
						ratio_h_n_log_mu_array[alpha_count] = mu_local
						ratio_h_n_log_sigma_array[alpha_count] = sigma_local

	ax.errorbar(x=all_alphas, y=ratio_h_m_log_mu_array, yerr=ratio_h_m_log_sigma_array, ecolor="black", capsize=0.5, color="blue", label=r"$H_{\alpha} / m_{log}$")
	ax.errorbar(x=all_alphas, y=ratio_h_n_log_mu_array, yerr=ratio_h_n_log_sigma_array, ecolor="black", capsize=0.5, color="red", label=r"$H_{\alpha} / n_{log}$")

	plt.title(f"{language} / {treebank} / {len(seeds)} seeds / {conllu_key} / " + "$\\alpha=" + f"{alpha:.3f}$")
	ax.set_xlabel(r"$\alpha$")
	ax.set_ylabel("Ratio")
	plt.legend()
	plt.tight_layout()

	fig.savefig(f"plot_evaluate_small_random_sampling_{language}_{treebank}_{len(seeds)}seeds_{conllu_key}_alpha{alpha:.3f}_errorbars.png")

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()

	ap.add_argument("--seed", type=int, nargs="+", default=list(range(2048)))
	ap.add_argument("--treebank", type=str, default="Sequoia")
	ap.add_argument("--language", type=str, default="French")
	#ap.add_argument("--num_sentences", type=int, nargs="+", default=[100, 500, 1000, 1500, 2000])
	ap.add_argument("--conllu_key", type=str, default="form")
	#ap.add_argument("--alpha", type=float, default=1.0)
	ap.add_argument("--alpha_min", type=float, default=0.0)
	ap.add_argument("--alpha_max", type=float, default=3.0)
	ap.add_argument("--alpha_step", type=float, default=0.1)

	args = ap.parse_args()

	#exit(main(seeds=args.seed, treebank=args.treebank, language=args.language, array_num_sentences=args.num_sentences, alpha=args.alpha))
	#exit(main(seeds=args.seed, treebank=args.treebank, language=args.language, conllu_key=args.conllu_key, alpha=args.alpha))
	exit(main(seeds=args.seed, treebank=args.treebank, language=args.language, conllu_key=args.conllu_key, alpha_min=args.alpha_min, alpha_max=args.alpha_max, alpha_step=args.alpha_step))
