#!/usr/bin/env python3

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#


import argparse
import os
import configparser
import conllu
import string
import re
from lxml import etree

def main(files, config_xpath, output_dir):
    output_dir = output_dir.rstrip(os.sep)

    os.makedirs(output_dir, exist_ok=True)

    cfg_xpath = configparser.ConfigParser()

    cfg_xpath.read(config_xpath)

    sentence_query = cfg_xpath["xpath_queries"]["sentence_query"]
    token_query = cfg_xpath["xpath_queries"]["token_query"]
    sentence_num_key = cfg_xpath["xpath_keys"]["sentence_num_key"]
    lemma_key = cfg_xpath["xpath_keys"]["lemma_key"]
    xpos_key = cfg_xpath["xpath_keys"]["xpos_key"]

    #mult_ws_regex = re.compile(r"\s{2,}")
    mult_ws_regex = re.compile(r" {2,}")

    for f in files:
        filename = f.split(os.sep)[-1]
        tree = etree.parse(f)

        name_f_out = os.sep.join([output_dir, filename[:-4] + ".conllu"])

        print(f"{f} -> {name_f_out}")

        f_out = open(name_f_out, "wt", encoding="utf-8")


        for sent in tree.xpath(sentence_query):
            sent_num = sent.attrib.get(sentence_num_key)

            sent_id = f"{filename}-{sent_num}"

            token_list = conllu.TokenList(
                metadata = {
                    "sent_id": sent_id
                }
            )

            for id_, token in enumerate(sent.xpath(token_query), start=1):
                #if not token.is_text:
                if token == None:
                    continue
                form = token.text.strip(" ") if token.text != None else "_"
                for ws in string.whitespace:
                    form = form.replace(ws, " ")
                form = mult_ws_regex.sub(" ", form)
                lemma = token.attrib.get(lemma_key)
                upos = "_"
                xpos = token.attrib.get(xpos_key)
                feats = "_"
                head = "_"
                deprel = "_"
                deps = "_"
                misc = "_"

                token_list.append(
                    conllu.Token(
                        id=id_,
                        form=form,
                        lemma=lemma,
                        upos=upos,
                        xpos=xpos,
                        feats=feats,
                        head=head,
                        deprel=deprel,
                        deps=deps,
                        misc=misc
                    )
                )

            serialization = conllu.serializer.serialize(token_list)

            f_out.write(serialization)

        f_out.close()


    return 0

if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("-f", "--file", nargs="+", type=str, required=True)
    ap.add_argument("-c", "--config_xpath", type=str, required=True)
    ap.add_argument("-o", "--output_dir", type=str, required=True)

    args = ap.parse_args()

    exit(main(files=args.file, config_xpath=args.config_xpath, output_dir=args.output_dir))
