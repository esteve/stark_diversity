#!/bin/sh

file_base=" "
file_extension="data/input/bnc/conllu/*.conllu"
target="best"
voc_set_cardinality=1
num_tokens_to_select="1000000 10000000"
exhaustivity="4096 2048 1024 512 256 128 64 32 16 8 4 2 1 1 1 1"
ud_column="UD_FORM"
step_display=10000

. src/utils/tei/diverscontrol_analysis.sh

