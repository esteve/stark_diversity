#!/usr/bin/env sh

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

# compilation options

if [ -z "${debug}"                      ] ; then debug=0                                                              ; fi
if [ -z "${pedantic}"                   ] ; then pedantic=1                                                           ; fi
if [ -z "${harden}"                     ] ; then harden=0                                                             ; fi
if [ -z "${compact}"                    ] ; then compact=0                                                            ; fi
if [ -z "${optim}"                      ] ; then optim=1                                                              ; fi
if [ -z "${compiler}"                   ] ; then compiler="GNU"                                                       ; fi
if [ -z "${legacy_compiler}"            ] ; then legacy_compiler=0                                                    ; fi
if [ -z "${native}"                     ] ; then native=1                                                             ; fi
if [ -z "${avx}"                        ] ; then avx=0                                                                ; fi
if [ -z "${pcre2_code_unit_width}"      ] ; then pcre2_code_unit_width=8                                              ; fi
if [ -z "${tokenization_method}"        ] ; then tokenization_method=0                                                ; fi
if [ -z "${enable_filter}"              ] ; then enable_filter=0                                                      ; fi


# diverscontrol options

if [ -z "${num_tokens_to_select}"       ] ; then num_tokens_to_select="1000 10000 100000 1000000 10000000"            ; fi
#if [ -z "${num_tokens_to_select}"       ] ; then num_tokens_to_select="1000 5000 10000 50000 100000 500000 1000000 5000000 10000000 50000000"            ; fi
if [ -z "${file_base}"                  ] ; then file_base=""                                                         ; fi
if [ -z "${file_extension}"             ] ; then file_extension="data/input/bnc/conllu/*.conllu"                      ; fi
#if [ -z "${exhaustivity}"               ] ; then exhaustivity="100 50 25 10 5 1 1 1"                                  ; fi
if [ -z "${exhaustivity}"               ] ; then exhaustivity="1024 512 256 128 64 32 16 8 4 2 1"                     ; fi
if [ -z "${jsonl_content_key}"          ] ; then jsonl_content_key="text"                                             ; fi
if [ -z "${utf8_normalisation}"         ] ; then utf8_normalisation=0                                                 ; fi
if [ -z "${step_display}"               ] ; then step_display=1000                                                    ; fi
if [ -z "${lowercase}"                  ] ; then lowercase=0                                                          ; fi
if [ -z "${target}"                     ] ; then target="best"                                                        ; fi
if [ -z "${voc_set_cardinality}"        ] ; then voc_set_cardinality=1                                                ; fi
if [ -z "${seeds}"                      ] ; then seeds="$(seq -s " " 1 ${voc_set_cardinality})"                       ; fi
if [ -z "${probability}"                ] ; then probability=0.01                                                     ; fi
if [ -z "${ud_column}"                  ] ; then ud_column="UD_FORM"                                                  ; fi


# slurm options

if [ -z "${partition}"                  ] ; then partition="all"                                                      ; fi
if [ -z "${qos}"                        ] ; then qos="default"                                                        ; fi
if [ -z "${gpus}"                       ] ; then gpus=0                                                               ; fi
if [ -z "${time}"                       ] ; then time="1-00:00:00"                                                    ; fi
if [ -z "${threads}"                    ] ; then threads=16                                                           ; fi

set -eu

if [ ${debug} -eq 0            ] ; then export PYTHONOPTIMIZE=1  ; else export PYTHONOPTIMIZE=0                       ; fi
#if [ -z "${compiler}"          ] ; then arg_compiler=""          ; else arg_compiler="COMPILER=${compiler}"           ; fi
if [ "${target}" == "random"   ] ; then use_random=1             ; else use_random=0                                  ; fi

export TQDM_DISABLE=1

cd diverscontrol
make -B bin/diverscontrol				\
	TOKENIZATION_METHOD=${tokenization_method}	\
	DEBUG=${debug}					\
	PEDANTIC=${pedantic}				\
	HARDEN=${harden}				\
	COMPACT=${compact}				\
	OPTIM=${optim}					\
	PYTHON_BUILD=0					\
	LEGACY_COMPILER=${legacy_compiler}		\
	NATIVE=${native}				\
	AVX=${avx}					\
	PCRE2_CODE_UNIT_WIDTH=${pcre2_code_unit_width}	\
	COMPILER=${compiler}				\
	ENABLE_FILTER=${enable_filter}			\
	UD_COLUMN=${ud_column}
cd ..
	
log_dir="log/diverscontrol_analysis"
mkdir -p ${log_dir}

tmp_dir="tmp/run"
mkdir -p ${tmp_dir}

for ntts in ${num_tokens_to_select} ; do
sh_to_exec="#!/bin/sh
set -eu
diverscontrol/bin/diverscontrol \
-i ${file_base} \
-f ${file_extension} \
-u ${utf8_normalisation} \
-k ${jsonl_content_key} \
-t ${threads} \
-n ${exhaustivity} \
-y ${ntts} \
-d ${step_display} \
-l ${lowercase} \
-g ${voc_set_cardinality} \
-v ${seeds} \
-r ${use_random} \
-p ${probability}
"

identifier="${target}_ntts${ntts}_${exhaustivity// /-}"

echo "identifier: ${identifier}"

sh_filename="${tmp_dir}/${identifier}.sh"

echo "${sh_to_exec}" > "${sh_filename}"

cat ${sh_filename}

cmd_="sbatch \
--array=0 \
--verbose \
--partition=${partition} \
--qos=${qos} \
--time=${time} \
--gpus-per-node=${gpus} \
--cpus-per-task=${threads} \
--job-name=diverscontrol_analysis \
--output=${log_dir}/%x-%A-%j-%a-stdout_${identifier}.log \
--error=${log_dir}/%x-%A-%j-%a-stderr_${identifier}.log \
${sh_filename}
"

echo "${cmd_}"

${cmd_}
done
