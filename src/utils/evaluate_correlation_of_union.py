#!/usr/bin/env python3
#SBATCH --begin=now
#SBATCH --comment="Pipeline to evaluate diversity correlation between lexicon and syntax, with unions of datasets."
#SBATCH --cpus-per-task=8
#SBATCH --chdir=.
#SBATCH --job-name=evaluate_correlation_with_union
#SBATCH --mail-user=louis.esteve@lisn.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=32g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#


import os
os.environ["TQDM_DISABLE"] = "1"

import argparse
import numpy as np
import glob
import conllu
import diversutils
import time
import scipy as sp
import matplotlib.pyplot as plt
import seaborn as sns
import logging
import tempfile
import stark
import pandas as pd
import bz2
from collections import Counter, deque

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG if __debug__ else logging.INFO)

#if not __debug__:
stark_logger = logging.getLogger("stark")
stark_logger.setLevel(logging.WARNING)
del(stark_logger)

NP_DTYPE = np.float32

def ud_get_paths(language, treebank):
    path_list = ["data", "input", "evaluation_correlation_with_union"]
    path = os.sep.join(path_list)
    os.makedirs(path, exist_ok=True)

    ud_name = f"UD_{language}-{treebank}"
    path_list.append(ud_name)
    path = os.sep.join(path_list)
    if not os.path.isdir(path):
        cmd_ = f"git clone https://github.com/UniversalDependencies/{ud_name} {repr(path)}"
    else:
        #cmd_ = f"set -eu ; wd_=$(pwd) ; cd {repr(path)} ; git pull ; cd $wd_"
        cmd_ = None

    if cmd_ != None:
        print(f"cmd_: {cmd_}")
        prcs_res = os.system(cmd_)
    
        if prcs_res != 0:
            raise Exception(f"Call failed (exit status: {prcs_res}): {repr(cmd_)}")

    #path_list += ["**", "*.conllu"]
    path_list.append("*.conllu")
    path = os.sep.join(path_list)

    print(f"Searching: {repr(path)}")

    return glob.glob(path)

def extract_elements(language, treebank, config_dict):
    all_sentences_per_config = {key: [] for key in config_dict.keys()}

    if language == "none" and treebank == "none":
        return all_sentences_per_config

    path_list = ["data", "target", "evaluation_correlation_with_union"]
    path = os.sep.join(path_list)
    os.makedirs(path, exist_ok=True)

    path_list.append(f"save_{language}_{treebank}.tsv.bz2")
    path_out = os.sep.join(path_list)
    opener = bz2.open

    if os.path.isfile(path_out):
        print(f"Reading {repr(path_out)}")
        f_in = opener(path_out, "rt", encoding="utf-8")
        df_in = pd.read_csv(path_out, sep="\t", index_col=False)
        for id_, df_group in df_in.groupby(["config_stark", "sentence_id"]):
            config_stark = id_[0]
            sentence_id = id_[1]
            all_sentences_per_config[config_stark].append(Counter({row__["Tree"]: row__["Absolute frequency"] for id__, row__ in df_group.iterrows()}))
        f_in.close()
        return all_sentences_per_config

    #all_sentences_per_config = {key: deque() for key in config_dict.keys()}

    sentence_counter = 0

    t_start = time.time()

    output_df = pd.DataFrame({"config_stark": [], "sentence_id": [], "Tree": [], "Absolute frequency": []})

    to_concat = [output_df]

    for conllu_path in ud_get_paths(language=language, treebank=treebank):
        print(conllu_path)

        f = open(conllu_path, "rt", encoding="utf-8")
        #all_sentences += conllu.parse(f.read())
        #all_sentences += [Counter(tok[conllu_key] for tok in sent) for sent in conllu.parse_incr(f)]

        for sent in conllu.parse_incr(f):
            tmp_f = tempfile.NamedTemporaryFile("wt")
            tmp_f.write(conllu.serializer.serialize(sent))

            for config_path in config_dict.keys():
                tmp_f.seek(0)
                config_dict[config_path]["input_path"] = tmp_f.name
                #if "output" in config_dict[config_path]:
                #    config_dict[config_path].pop("output")
                #print(config_dict[config_path])

                rows = list(stark.run(config_dict[config_path]))

                #df = pd.DataFrame(rows[:1])
                #df.columns = rows[0]
                df = pd.DataFrame(rows[1:], columns=rows[0])
                df["Absolute frequency"] = df["Absolute frequency"].astype(int)
                df["config_stark"] = [config_path] * len(df)
                df["sentence_id"] = [sent.metadata["sent_id"]] * len(df)

                to_concat.append(df)

                #print(df)

                all_sentences_per_config[config_path].append(Counter({row["Tree"]: row["Absolute frequency"] for id_, row in df.iterrows()}))                

            tmp_f.close()

            sentence_counter += 1

            if sentence_counter % 100 == 0:
                print(f"Sentences: {sentence_counter}; ({sentence_counter / (time.time() - t_start)} sent/s)")

        f.close()

    output_df = pd.concat(to_concat, ignore_index=True)

    for colname in output_df.columns:
        if colname not in ["config_stark", "sentence_id", "Tree", "Absolute frequency"]:
            del(output_df[colname])

    f_out = opener(path_out, "wt", encoding="utf-8")
    output_df.to_csv(f_out, sep="\t", index=False)
    f_out.close()

    print(f"Wrote to {repr(path_out)}")

    return all_sentences_per_config

def main(seeds, treebank_a, language_a, treebank_b, language_b, config_path_stark_0, config_path_stark_1, alpha_min=0.0, alpha_max=3.0, alpha_step=0.1, block_size=32, ratio_b_min=0.0, ratio_b_max=1.0, ratio_b_step=0.05):
    if language_a == "none" and treebank_a == "none" and ratio_b_min == 0.0:
        raise Exception(f"If A is an empty corpus, ratio_b_min must be strictly greater than 0.")

    config_dict = {
        config_path_stark_0: stark.read_settings(config_path_stark_0, stark.parse_args([])),
        config_path_stark_1: stark.read_settings(config_path_stark_1, stark.parse_args([])),
    }
    for config_path in config_dict:
        config_dict[config_path]["output"] = None

    all_alphas = np.linspace(alpha_min, alpha_max, int(np.ceil((alpha_max - alpha_min) / alpha_step) + 1))

    datasets = {
        'a': extract_elements(language=language_a, treebank=treebank_a, config_dict=config_dict),
        'b': extract_elements(language=language_b, treebank=treebank_b, config_dict=config_dict),
    }

    ############################################################################

    # for each percentage, take the percentage of b

    ratios_b = np.linspace(ratio_b_min, ratio_b_max, int(np.ceil((ratio_b_max - ratio_b_min) / ratio_b_step)) + 1)

    correlations = {x: np.zeros((len(ratios_b), len(all_alphas)), dtype=NP_DTYPE) for x in ["pearson_r_mu", "pearson_p_mu", "spearman_r_mu", "spearman_p_mu", "pearson_r_sigma", "pearson_p_sigma", "spearman_r_sigma", "spearman_p_sigma"]}

    for ratio_b_i, ratio_b in enumerate(ratios_b):
        print(f"ratio_b: {ratio_b:.3f}")
        size_from_b = int(np.floor(len(datasets["b"][config_path]) * ratio_b))
        size_from_b -= size_from_b % block_size
        dataset_union = {config_path: datasets["a"][config_path] + datasets["b"][config_path][:size_from_b] for config_path in config_dict}
        len_dataset_union = len(dataset_union[list(dataset_union.keys())[0]])
        #print(f"len_dataset_union: {len_dataset_union}")

        num_blocks = int(np.ceil(len_dataset_union / block_size))

        #print(f"num_blocks: {num_blocks}")

        #diversity_scores = {config_path: np.zeros((len(ratios_b), len(seeds), len(
        diversity_scores = {config_path: np.zeros((len(all_alphas), len(seeds), num_blocks), dtype=NP_DTYPE) for config_path in config_dict}

        for seed_i, seed in enumerate(seeds):
            selected_indices = np.arange(len_dataset_union)
            np.random.seed(seed)
            np.random.shuffle(selected_indices)

            #print(f"selected_indices: {selected_indices}")
            
            """
            vocab_per_block = {config_path: [Counter()] * num_blocks for config_path in config_dict}

            for index_i, index in enumerate(selected_indices):
                print(f"block: {index_i // block_size}; sentence: {index}")
                for config_path in config_dict:
                    vocab_per_block[config_path][index_i // block_size] += dataset_union[config_path][index]
            """

            vocab_per_block = {}

            for config_path in config_dict:
                #vocab_per_block[config_path] = [Counter()] * num_blocks
                vocab_per_block[config_path] = [None] * num_blocks

                for block_i in range(num_blocks):
                    vocab_per_block[config_path][block_i] = Counter()
                    for j in dataset_union[config_path][block_i * block_size:min(len(dataset_union[config_path]), (block_i+1) * block_size)]:
                        vocab_per_block[config_path][block_i] += j
                        #print(config_path, block_i, "adding", j)
            """
                    if block_i == 1:
                        print(vocab_per_block[config_path_stark_0][0], "\n", vocab_per_block[config_path_stark_0][1])
                        assert(vocab_per_block[config_path_stark_0][0] != vocab_per_block[config_path_stark_0][1])
            print(vocab_per_block[config_path_stark_0][0], "\n", vocab_per_block[config_path_stark_0][1])
            assert(vocab_per_block[config_path_stark_0][0] != vocab_per_block[config_path_stark_0][1])
            """

            #print(vocab_per_block)
            #print(vocab_per_block[config_path_stark_0][0], vocab_per_block[config_path_stark_0][1])

            # compute diversity per config_path, per block, and per alpha

            for config_path in config_dict:
                for block_i in range(num_blocks):
                    g_index = diversutils.create_empty_graph(0, 0)

                    for k, v in vocab_per_block[config_path][block_i].items():
                        diversutils.add_node(g_index, int(v))

                    diversutils.compute_relative_proportion(g_index)

                    for alpha_i, alpha in enumerate(all_alphas):
                        h_alpha = diversutils.individual_measure(g_index, diversutils.DF_ENTROPY_RENYI, alpha)[0]
                        #print(f"h_alpha: {h_alpha:.3f}")

                        diversity_scores[config_path][alpha_i, seed_i, block_i] = h_alpha
                        #diversity_scores[config_path][alpha_i][seed_i][block_i] = h_alpha
                        #print(config_path, alpha_i, seed_i, block_i, h_alpha)

                    diversutils.free_graph(g_index)

        #correlations_per_alpha_r = np.zeros((len(all_alphas), len(seeds)), dtype=NP_DTYPE)

        #print(diversity_scores[config_path_stark_0])

        for alpha_i in range(len(all_alphas)):
            local_array = {x: np.zeros(len(seeds), dtype=NP_DTYPE) for x in ["pearson_r", "pearson_p", "spearman_r", "spearman_p"]}
            for seed_i in range(len(seeds)):
                for correlation_name, correlation_function in zip(["pearson", "spearman"], [sp.stats.pearsonr, sp.stats.spearmanr]):
                    #print(diversity_scores[config_path_stark_0][alpha_i, seed_i, :], diversity_scores[config_path_stark_1][alpha_i, seed_i, :])
                    r, p = correlation_function(diversity_scores[config_path_stark_0][alpha_i, seed_i, :], diversity_scores[config_path_stark_1][alpha_i, seed_i, :])
                    for spec, value in zip(["r", "p"], [r, p]):
                        local_array[f"{correlation_name}_{spec}"][seed_i] = value
            for key_local_array in local_array:
                mu = np.mean(local_array[key_local_array])
                sigma = np.std(local_array[key_local_array])

                for a_, b_ in zip(["mu", "sigma"], [mu, sigma]):
                    correlations[f"{key_local_array}_{a_}"][ratio_b_i, alpha_i] = b_

    fig, ax = plt.subplots(figsize=(16,16))
    ax = sns.heatmap(correlations["pearson_r_mu"], linewidth=0.0, yticklabels=[f"{y:.3f}" for y in ratios_b], xticklabels=[f"{x:.3f}" for x in all_alphas], vmin=-1.0, vmax=1.0, ax=ax)
    plt.ylabel("Ratio of B dataset (no partial block)")
    plt.xlabel(r"$\alpha$")
    plt.title(f"Pearson / {len(seeds)} seeds / bs: {block_size} / A: UD_{language_a}-{treebank_a} / B: UD_{language_b}-{treebank_b} / {config_path_stark_0.split(os.sep)[-1]} / {config_path_stark_1.split(os.sep)[-1]}")
    plt.tight_layout()

    plt.savefig(f"plot_correlation_{len(seeds)}seeds_{language_a}_{treebank_a}_{language_b}_{treebank_b}.png")

    return 0

if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("--seed", type=int, nargs="+", default=list(range(32)))
    ap.add_argument("--treebank_a", type=str, default="PUD")
    ap.add_argument("--language_a", type=str, default="French")
    ap.add_argument("--treebank_b", type=str, default="Sequoia")
    ap.add_argument("--language_b", type=str, default="French")
    ap.add_argument("--alpha_min", type=float, default=0.0)
    ap.add_argument("--alpha_max", type=float, default=3.0)
    ap.add_argument("--alpha_step", type=float, default=0.05)
    ap.add_argument("--ratio_b_min", type=float, default=0.0)
    ap.add_argument("--ratio_b_max", type=float, default=1.0)
    ap.add_argument("--ratio_b_step", type=float, default=0.025)
    ap.add_argument("--config_path_stark_0", type=str, default=os.sep.join(["config", "stark", "a2_config_DiversUtils_forms.ini"]))
    ap.add_argument("--config_path_stark_1", type=str, default=os.sep.join(["config", "stark", "d1_config_DiversUtils_upos_all-trees.ini"]))
    ap.add_argument("--block_size", type=int, default=32)

    args = ap.parse_args()

    exit(main(seeds=args.seed, treebank_a=args.treebank_a, language_a=args.language_a, treebank_b=args.treebank_b, language_b=args.language_b, alpha_min=args.alpha_min, alpha_max=args.alpha_max, alpha_step=args.alpha_step, config_path_stark_0=args.config_path_stark_0, config_path_stark_1=args.config_path_stark_1, block_size=args.block_size, ratio_b_min=args.ratio_b_min, ratio_b_max=args.ratio_b_max, ratio_b_step=args.ratio_b_step))
