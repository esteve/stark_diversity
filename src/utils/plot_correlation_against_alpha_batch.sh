set -eu

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

#python3 src/utils/plot_correlation_against_alpha.py --path /people/scholivet/data_Louis/*_{a2,d1}_*_src_*_00000_*.bz2 /people/scholivet/data_Louis/*_{a2,d1}_*_src_*_00001_*.bz2 /people/scholivet/data_Louis/*_{a2,d1}_*_src_*_00002_*.bz2 --output correlations_against_alpha_src_0-2.png &> plot_alpha_src_0-2.log &


#python3 src/utils/plot_correlation_against_alpha.py --path /people/scholivet/data_Louis/*_{a2,d1}_*europarl*_00000_*.bz2 --output correlations_against_alpha_europarl_0.png &> plot_alpha_europarl_0.log &
#python3 src/utils/plot_correlation_against_alpha.py --path /people/scholivet/data_Louis/*_{a2,d1}_*uncorpus*_0000{0,1,2}_*.bz2 --output correlations_against_alpha_uncorpus_0-2.png &> plot_alpha_uncorpus_0-2.log &
#python3 src/utils/plot_correlation_against_alpha.py --path /people/scholivet/data_Louis/*_{a2,d1}_*wikipedia*_0000{0,1,2}_*.bz2 --output correlations_against_alpha_wikipedia_0-2.png &> plot_alpha_wikipedia_0-2.log &
#python3 src/utils/plot_correlation_against_alpha.py --path /people/scholivet/data_Louis/*_{a2,d1}_*europarl*_00000_*.bz2 /people/scholivet/data_Louis/*_{a2,d1}_*{uncorpus,wikipedia}*_0000{0,1,2}_*.bz2 --output correlations_against_alpha_mixed_intermediate.png &> plot_alpha_mixed_intermediate.log &

# final plots?
#time python3 src/utils/plot_correlation_against_alpha.py --path /people/scholivet/data_Louis/*_{a2,d1}_*europarl*.bz2 --output correlations_against_alpha_europarl_full.png &> plot_alpha_europarl_full.log &
#time python3 src/utils/plot_correlation_against_alpha.py --path /people/scholivet/data_Louis/*_{a2,d1}_*uncorpus*.bz2 --output correlations_against_alpha_uncorpus_full.png &> plot_alpha_uncorpus_full.log &
time python3 src/utils/plot_correlation_against_alpha.py --path /work/esteve/tmpBlocks/*.tsv /people/scholivet/data_Louis/*_{a2,d1}_*wikipedia*.bz2 --output correlations_against_alpha_wikipedia_full.png &> plot_alpha_wikipedia_full2.log &
time python3 src/utils/plot_correlation_against_alpha.py --path /work/esteve/tmpBlocks/*.tsv /people/scholivet/data_Louis/*_{a2,d1}_*.bz2 --output correlations_against_alpha_mixed_full.png &> plot_alpha_mixed_full2.log &





#python3 src/utils/plot_correlation_against_alpha.py --path /people/scholivet/data_Louis/*_{a2,d1}_*_00000_*.bz2 --output correlations_against_alpha_mixed_small.png &> plot_alpha_mixed_small.log &
