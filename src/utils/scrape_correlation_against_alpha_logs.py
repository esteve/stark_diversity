#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math

font = {'size': 16}

matplotlib.rc('font', **font)

LINE_STYLES = {
    "Europarl": "dotted",
    "UN corpus": "dashed",
    "Wikipedia": "dashdot",
    "Mixed": "solid"
}

def main(logfiles, names, alpha_min, alpha_max, alpha_step):
    number_of_alpha_iterations = int(np.ceil((alpha_max - alpha_min) / alpha_step)) + 1
    all_alphas = np.linspace(alpha_min, alpha_max, number_of_alpha_iterations)

    width = 8.0

    golden_ratio = (1.0 + math.sqrt(5.0)) / 2.0

    fig, ax = plt.subplots(figsize=(width, width / (golden_ratio ** 1.0)))

    for logfile, name in zip(logfiles, names):
        f_log = open(logfile, "rt", encoding="utf-8")

        #line_reader = f_log.readlines()

        #line = next(line_reader)
        line = f_log.readline()

        while not (line.startswith("(") and line.rstrip("\n").endswith(")")):
            #line = next(line_reader)
            line = f_log.readline()

        print(name)

        rows = []

        for line in f_log.readlines():
            line_split = [float(x) for x in line.rstrip("\n").split(" ")]
            print(line_split)
            rows.append(line_split)

        f_log.close()

        pearson_r = [row[0] for row in rows]
        pearson_p = [row[1] for row in rows]
        spearman_r = [row[2] for row in rows]
        spearman_p = [row[3] for row in rows]

        line_style = LINE_STYLES[name]

        ax.plot(all_alphas, pearson_r, linestyle=line_style, color="blue", label=f"{name} - Pearson")
        ax.plot(all_alphas, spearman_r, linestyle=line_style, color="red", label=f"{name} - Spearman")

    ax.set_ylim(-1.05, +1.05)
    ax.set_xlabel(r"$\alpha$")
    ax.set_ylabel(r"$\rho$")
    #plt.legend(ncol=2)
    plt.tight_layout()
    plt.savefig("correlations_unified.png")


    return 0

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--logfile", type=str, nargs="+")
    ap.add_argument("--name", type=str, nargs="+")
    ap.add_argument("--alpha_min", type=float, default=0.0)
    ap.add_argument("--alpha_max", type=float, default=5.0)
    ap.add_argument("--alpha_step", type=float, default=0.05)

    args = ap.parse_args()

    assert(len(args.logfile) == len(args.name))

    exit(main(logfiles=args.logfile, names=args.name, alpha_min=args.alpha_min, alpha_max=args.alpha_max, alpha_step=args.alpha_step))

