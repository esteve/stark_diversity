#!/bin/bash
#SBATCH --begin=now
#SBATCH --comment="Pipeline to download UD-MULTIGENRE treebanks."
#SBATCH --cpus-per-task=4
#SBATCH --chdir=.
#SBATCH --job-name=delta_download_ud-multigenre_treebanks
#SBATCH --mail-user=louis.esteve@universite-paris-saclay.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=64g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal
#SBATCH --verbose
#SBATCH --time=24:00:00

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

# ================

set -eu

org="UppsalaNLP"
repo="UD-MULTIGENRE"
union_name="union"

#python3 src/utils/gitretrieve.py \
#	--organization ${org} \
#	--prefix UD- \
#	--per_page 100

mkdir -p data/input/${org}/${repo}/${union_name}

initial_pwd=$(pwd)

for a in "train:dev" "test" ;
do
    cd ${initial_pwd}
    cd data/input/${org}/${repo}/${a}
    echo "pwd: $(pwd)"
    b_list=$(ls -d *)
    cd ${initial_pwd}
    for b_ in ${b_list} ;
    do
        b="data/input/${org}/${repo}/${a}/${b_}"
        echo "b: ${b}"
        if [[ -d ${b} ]] ;
        then
            previous_directory=$(pwd)
            cd "${b}"
            for c in * ;
            do
                echo "b/c: ${b}/${c}"
                if ! [[ "${c}" = "${c/ /_}" ]] ;
                then
                    #cmd_="mv \"${b}/${c}\" \"${b}/${c/ /_}\""
                    cmd_="mv ${b}/${c/ /\\ } ${b}/${c/ /_}"
                    echo "cmd_: ${cmd_}"
                    sh -c "${cmd_}"
                fi
                cmd_="mkdir -p ${initial_pwd}/data/input/${org}/${repo}/${union_name}/${b_}/${c/ /_}"
                echo "cmd_: ${cmd_}"
                ${cmd_}
                previous_directory_second=$(pwd)
                cd "${c}"
                for d in *.conllu ;
                do
                    #cmd_="ln -sf '${initial_pwd}/data/input/${org}/${repo}/${a/:/\\:}/${b_}/${c}/${d}' ${initial_pwd}/data/input/${org}/${repo}/${union_name}/${b_}/${c/ /_}/${d}"
                    #original="${initial_pwd}/data/input/${org}/${repo}/${a/:/\\:}/${b_}/${c}/${d}"
                    original="${initial_pwd}/data/input/${org}/${repo}/${a}/${b_}/${c}/${d}"
                    new="${initial_pwd}/data/input/${org}/${repo}/${union_name}/${b_}/${c/ /_}/${d}"
                    #cmd_="ln -sf '${new}' '${original}'"
                    cmd_="cp ${original} ${new}"
                    echo "cmd_: ${cmd_}"
                    ${cmd_}
                done
                cd "${previous_directory_second}"
            done
            cd "${previous_directory}"
        fi
    done
done
