#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import os
import glob
import diversutils
import re
import pandas as pd
import scipy

FUNCTIONS_TO_COMPUTE = ["INDEX_RICHNESS", "INDEX_SHANNON_EVENNESS", "ENTROPY_SHANNON_WEAVER"]

def main(org, index_a, index_b, db_name_extra, config_index_stark):
	reg = re.compile(r"(?P<METHOD>base|best|random)-(?P<ID>\d+)")

	diversity_series = {}

	path_dict = {}

	path_list = ["data", "target"]
	path_list.append(f"output_*{'-'.join(db_name_extra) if db_name_extra != [] else 'None'}*_{config_index_stark}_*.tsv")
	path = os.sep.join(path_list)
	for tsv_path in glob.glob(path):
		reg_res = reg.search(tsv_path)
		if reg_res == None:
			continue

		method = reg_res.group("METHOD")
		id_ = reg_res.group("ID")

		print(tsv_path, method, id_)

		if method not in path_dict:
			path_dict[method] = {}
		if id_ in path_dict[method]:
			raise Exception(f"already present: {method} {id_}")
		path_dict[method][int(id_)] = {
			"path": tsv_path,
			"df": pd.read_csv(tsv_path, sep="\t", usecols=["Tree", "Absolute frequency"]),
		}

		
	path_list.pop()

	for method in path_dict.keys():
		if method == "base":
			continue

		for id_ in sorted(path_dict[method].keys()):
			count_dict = {}

			print(f" {method}-{id_} ".center(64, "="))

			for df in [path_dict["base"][0]["df"], path_dict[method][id_]["df"]]:
				for rowid, row in df.iterrows():
					tree = row["Tree"]
					count_ = row["Absolute frequency"]

					if tree not in count_dict:
						count_dict[tree] = 0
					count_dict[tree] += count_

			g = diversutils.create_empty_graph(0, 0)

			for key, value in count_dict.items():
				diversutils.add_node(g, value)

			diversutils.compute_relative_proportion(g)

			for function_to_compute in FUNCTIONS_TO_COMPUTE:
				tup = diversutils.individual_measure(g, getattr(diversutils, f"DF_{function_to_compute}"))
				print(function_to_compute, tup)

				if method == "random":
					if function_to_compute not in diversity_series:
						diversity_series[function_to_compute] = {}
					for x, i in zip(tup, range(len(tup))):
						if i not in diversity_series[function_to_compute]:
							diversity_series[function_to_compute][i] = []
						diversity_series[function_to_compute][i].append(x)

			diversutils.free_graph(g)

	print("Normal distribution test for the 'random' data")

	for function_to_compute in diversity_series:
		for i in diversity_series[function_to_compute].keys():
			stat, pvalue = scipy.stats.normaltest(diversity_series[function_to_compute][i])
			print(function_to_compute, i, float(stat), float(pvalue))

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()

	ap.add_argument("-o", "--organization", type=str, required=True)
	ap.add_argument("-a", "--index_a", type=str, default="0")
	ap.add_argument("-b", "--index_b", type=str, default="1")
	ap.add_argument("-d", "--db_name_extra", type=str, nargs="+", default=[])
	ap.add_argument("-c", "--config_index_stark", type=str, required=True)

	args = ap.parse_args()

	exit(main(org=args.organization, index_a=args.index_a, index_b=args.index_b, db_name_extra=args.db_name_extra, config_index_stark=args.config_index_stark))
