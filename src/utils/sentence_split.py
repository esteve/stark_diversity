#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import os
import conllu
import argparse

def main(file_paths):
	out_path_list = ["data", "input", "sentence-wise-split"]
	out_path = os.sep.join(out_path_list)
	if not (os.path.exists(out_path) and os.path.isdir(out_path)):
		os.mkdir(out_path)


	for path in file_paths:
		if not (os.path.exists(path) and os.path.isfile(path)):
			raise Exception(f"Does not exist: {repr(path)}")
		
		f = open(path, "rt", encoding="utf-8")
		"""
		s = f.read()
		f.close()
		"""

		in_file_name = path.split(os.sep)[-1]
		out_path_list.append(in_file_name)
		out_path = os.sep.join(out_path_list)
		if not (os.path.exists(out_path) and os.path.isdir(out_path)):
			os.mkdir(out_path)

		num_sentences = 0

		#for sent in conllu.parse(s):
		for sent in conllu.parse_incr(f):
			sent.metadata["sent_id"] = f"{in_file_name}-{sent.metadata['sent_id']}"
			sent_id = sent.metadata["sent_id"]
			out_file_name = f"{sent_id}.conllu"
			out_path_list.append(out_file_name)
			out_path = os.sep.join(out_path_list)

			f_out = open(out_path, "wt", encoding="utf-8")
			f_out.write(conllu.serializer.serialize(sent))
			f_out.close()

			#print(f"generated: {repr(out_path)}")

			out_path_list.pop()

			num_sentences += 1

			if num_sentences % 1000 == 0:
				print(f"{num_sentences} sentences", end="\r")

		f.close()

		out_path_list.pop()

		
	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()

	ap.add_argument("-f", "--file", type=str, nargs="+", required=True)

	args = ap.parse_args()

	exit(main(file_paths=args.file))
