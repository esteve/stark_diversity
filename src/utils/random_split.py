#!/usr/bin/python3

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import os
import glob
import numpy as np
import argparse
import conllu

def main(organization, probabilities, seed):
    n = len(probabilities)

    np.random.seed(seed)

    #probabilities = np.ndarray(sorted(probabilities[:], reverse=True))
    probabilities = np.array(probabilities[:])
    probabilities /= np.sum(probabilities)

    print(probabilities)

    path_list = ["data", "input", organization]
    path = os.sep.join(path_list)

    if not (os.path.exists(path) and os.path.isdir(path)):
        raise Exception(f"Does not exist: {repr(path)}")

    all_open_files = []
    for i in range(n):
        path_list.append(f"{organization}-{i}")
        path = os.sep.join(path_list)
        if not os.path.exists(path):
            os.mkdir(path)

        path_list.append("data.conllu")
        path = os.sep.join(path_list)

        all_open_files.append(open(path, "wt", encoding="utf-8"))

        path_list.pop()

        path_list.pop()

    accepted_extensions = ["conllu", "cupt"]

    for extension in accepted_extensions:
        path_list.append(f"*.{extension}")
        path = os.sep.join(path_list)
        for input_path in glob.glob(path):
            print(f"handling {repr(input_path)}")

            f = open(input_path, "rt", encoding="utf-8")

            filename = input_path.split(os.sep)[-1]

            for sent in conllu.parse_incr(f):
                for k in sent.metadata.keys():
                    if k.endswith("_id"):
                        sent.metadata[k] = f"{filename}-{sent.metadata[k]}"

                proba = np.random.random_sample(size=(1,))[0]

                prob_sum = 0.0
                for i in range(n):
                    prob_sum += probabilities[i]
                    if prob_sum >= proba:
                        all_open_files[i].write(conllu.serializer.serialize(sent))
                        break

            f.close()

        path_list.pop()

    path_list.pop()

    for i in range(n):
        all_open_files[i].close()

    return 0

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--probability", type=float, nargs="+")
    ap.add_argument("-o", "--organization", type=str, required=True)
    ap.add_argument("-s", "--seed", type=int, default=1)

    args = ap.parse_args()

    exit(main(probabilities=args.probability, organization=args.organization, seed=args.seed))

