#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import os
import argparse
import conllu

def main(files, selected_ids, path_out):
	f_selected_ids = open(selected_ids, "rt", encoding="utf-8")
	selected_ids = {x.rstrip("\n") for x in f_selected_ids.readlines()}
	f_selected_ids.close()

	f_out = open(path_out, "wt", encoding="utf-8")

	for path in files:
		print(path)
		f = open(path, "rt", encoding="utf-8")
		for sent in conllu.parse_incr(f):
			id_ = sent.metadata["sent_id"]

			if id_ not in selected_ids:
				continue

			f_out.write(conllu.serializer.serialize(sent))
		f.close()

	f_out.close()

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("-f", "--file", type=str, nargs="+", required=True)
	ap.add_argument("-s", "--selected_ids", type=str, required=True)
	ap.add_argument("-o", "--path_out", type=str, required=True)

	args = ap.parse_args()

	exit(main(files=args.file, selected_ids=args.selected_ids, path_out=args.path_out))
