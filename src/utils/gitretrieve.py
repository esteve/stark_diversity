#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import os
import requests
import json
import time

TIME_DELAY_API_SECONDS = 90.0 # needs at least 60.0
TIME_DELAY_PULL_SECONDS = 60.0

def retrieve_org_metadata(org="UniversalDependencies", per_page=100, rename=None):
    path_list = ["data", "meta"]
    path_dir = os.sep.join(path_list)
    if not (os.path.exists(path_dir) and os.path.isdir(path_dir)):
        os.mkdir(path_dir)

    if rename == None:
        rename = org

    #path_list.append(org)
    path_list.append(rename)
    path_dir = os.sep.join(path_list)
    if not (os.path.exists(path_dir) and os.path.isdir(path_dir)):
        os.mkdir(path_dir)

    concat_r_json = []

    page = 0
    while True:
        path_list.append(f"{org}_page{page}.json")
        path_meta = os.sep.join(path_list)
        path_list.pop()
        sent_request = False
        if not (os.path.exists(path_meta) and os.path.isfile(path_meta)):
            url = f"https://api.github.com/orgs/{org}/repos?per_page={int(per_page)}&page={int(page)}"
            print(f"{repr(path_meta)} does not exist, trying to fetch {repr(url)}")
    
            proxies = {}
            for potential_proxy in ["http_proxy", "https_proxy"]:
                for option in [potential_proxy.lower(), potential_proxy.upper()]:
                    if option in os.environ:
                        print(f"Detected proxy: {option}={os.environ[option]}")
                        proxies[option] = os.environ[option]
    
            r = requests.get(url, proxies=proxies)
            if r.status_code != 200:
                raise Exception(f"Failed to query {repr(url)}")
            r_json = r.json()
            f = open(path_meta, "wt", encoding="utf-8")
            f.write(r.text)
            f.close()

            sent_request = True
        else:
            f = open(path_meta, "rt", encoding="utf-8")
            r_json = json.load(f)
            f.close()

        concat_r_json += r_json

        if len(r_json) == per_page:
            page += 1
            if sent_request:
                time.sleep(TIME_DELAY_API_SECONDS)
        else:
            break

    return concat_r_json

def retrieve_org_repos(org="UniversalDependencies", prefix="UD_", per_page=100, branch="master", rename=None):
    path_list = ["data"]
    path_dir = os.sep.join(path_list)
    if not (os.path.exists(path_dir) and os.path.isdir(path_dir)):
        os.mkdir(path_dir)
    path_list.append("input")
    path_dir = os.sep.join(path_list)
    if not (os.path.exists(path_dir) and os.path.isdir(path_dir)):
        os.mkdir(path_dir)

    if rename == None:
        rename = org

    #path_list.append(org)
    path_list.append(rename)
    path_dir = os.sep.join(path_list)
    if not (os.path.exists(path_dir) and os.path.isdir(path_dir)):
        os.mkdir(path_dir)

    r_obj = retrieve_org_metadata(org=org, per_page=per_page, rename=rename)

    for potential_repo in r_obj:
        if not potential_repo["name"].startswith(prefix):
            print(f"Ignoring: {repr(potential_repo['html_url'])}")
            continue

        path_list.append(potential_repo["name"])
        local_path_for_repo = os.sep.join(path_list)
        path_list.pop()

        if not (os.path.exists(local_path_for_repo) and os.path.isdir(local_path_for_repo)):
            #cmd_ = f"set -eu ; git clone {repr(potential_repo['html_url'])} {repr(local_path_for_repo)} ; cd {repr(local_path_for_repo)} ; git checkout -b {repr(branch)} ; git pull origin {repr(branch)}"
            cmd_ = f"set -eu ; git clone -b {branch} {repr(potential_repo['html_url'])} {repr(local_path_for_repo)}"
            print(f"cmd_: {cmd_}")
            prcs_res = os.system(cmd_)
            if prcs_res != 0:
                raise Exception(f"Failed to clone repo: {repr(potential_repo['html_url'])}")
        else:
            #cmd_ = f"set -eu ; cd {repr(local_path_for_repo)} ; git checkout -b {repr(branch)} ; git pull origin {repr(branch)}"
            cmd_ = f"set -eu ; cd {repr(local_path_for_repo)} ; git pull"
            print(f"cmd: {cmd_}")
            prcs_res = os.system(cmd_)
            if prcs_res != 0:
                raise Exception(f"Failed to pull repo: {repr(local_path_for_repo)}")

        path_not_to_release = os.sep.join([local_path_for_repo.rstrip(os.sep), 'not-to-release'])
        cmd_ = f"set -eu ; if [[ -d {repr(path_not_to_release)} ]] ; then rm -r {repr(path_not_to_release)} ; fi"
        print(f"cmd: {cmd_}")
        prcs_res = os.system(cmd_)
        if prcs_res != 0:
            raise Exception("Failed to clean repo for 'not-to-release' data: {repr(potential_repo['html_url'])}")

        time.sleep(TIME_DELAY_PULL_SECONDS)

    return 0

def main(org, prefix, per_page, branch, rename):
    print(f"org: {org}")
    print(f"prefix: {prefix}")
    print(f"per_page: {per_page}")
    print(f"branch: {branch}")
    print(f"rename: {rename}")
    return retrieve_org_repos(org=org, prefix=prefix, per_page=per_page, branch=branch, rename=rename)

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-b", "--branch", type=str, default="master")
    ap.add_argument("-o", "--organization", type=str, default="UniversalDependencies")
    ap.add_argument("-p", "--prefix", type=str, default="UD_")
    ap.add_argument("-n", "--per_page", type=int, default=100)
    ap.add_argument("-a", "--rename", type=str, default=None)

    args = ap.parse_args()

    exit(main(org=args.organization, prefix=args.prefix, per_page=args.per_page, rename=args.rename, branch=args.branch))
