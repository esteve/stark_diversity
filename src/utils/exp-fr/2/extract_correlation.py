#!/usr/bin/env python3
#SBATCH --begin=now
#SBATCH --comment="Pipeline to compute correlations based on blocks of data."
#SBATCH --cpus-per-task=2
#SBATCH --chdir=.
#SBATCH --job-name=correlation_blocks
#SBATCH --mail-user=louis.esteve@lisn.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=8g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal
#SBATCH --verbose
#SBATCH --time=24:00:00

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import os
import argparse
import glob
import conllu
import scipy
import sqlite3 as sql

def main(organization="exp-fr", block_size=10000, array_config_path_stark=[], div_name="INDEX_RICHNESS", div_subindex=0, alpha=None, beta=None):
	new_organization = f"{organization}-block-resample-bs{block_size}"

	path_list = ["data", "input", new_organization]
	root_dir_new = os.sep.join(path_list)

	if not (os.path.exists(root_dir_new) and os.path.isdir(root_dir_new)):
		raise Exception(f"Does not exist: {repr(root_dir_new)}")

	for config_path_stark in array_config_path_stark:
		if not (os.path.exists(config_path_stark) and os.path.isfile(config_path_stark)):
			raise Exception(f"Does not exist: {repr(config_path_stark)}")

	config_indices = [config_path_stark.split(os.sep)[-1].split("_")[0] for config_path_stark in array_config_path_stark]

	all_series = {}

	for config_index in config_indices:
		db_path = os.sep.join(["data", "target", f"diversities_{config_index}_{new_organization}.db"])
		if not (os.path.exists(db_path) and os.path.isfile(db_path)):
			raise Exception(f"Does not exist: {repr(db_path)}")

		print(db_path)

		sql_conn = sql.connect(db_path)

		sql_curs = sql_conn.cursor()

		if config_index not in all_series:
			all_series[config_index] = {}

		list_args = [div_name]
		for x in [div_subindex, alpha, beta]:
			if x != None:
				list_args.append(x)

		tuple_args = tuple(list_args)

		qry = f"""SELECT treebank, div_value FROM diversities WHERE div_name=? AND div_subindex {'IS NULL' if div_subindex == None else '= ?'} AND alpha {'IS NULL' if alpha == None else '= ?'} AND beta {'IS NULL' if beta == None else '= ?'};"""
		for row in sql_curs.execute(qry, tuple_args):
			treebank = row[0]
			div_value = row[1]

			if treebank not in all_series[config_index]:
				all_series[config_index][treebank] = div_value
			else:
				raise Exception(f"Duplicate: {repr(config_index)}, {repr(treebank)}")

	#print(all_series)

	keys = sorted(all_series.keys())
	for a in range(len(keys)):
		config_index_a = keys[a]
		for b in range(a+1, len(keys)):
			config_index_b = keys[b]
			series_a = []
			series_b = []

			for k in sorted(list(all_series[config_index_a].keys())):
				if k in all_series[config_index_b]:
					series_a.append(all_series[config_index_a][k])
					series_b.append(all_series[config_index_b][k])

			#print(series_a)
			#print(series_b)

			pearson_r, pearson_p = scipy.stats.pearsonr(series_a, series_b)
			spearman_r, spearman_p = scipy.stats.spearmanr(series_a, series_b)
			print(f"{config_index_a:<8} / {config_index_b:<8} | {pearson_r:+.3f}, {pearson_p:.3e} | {spearman_r:+.3f}, {spearman_p:.3e}")

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("--organization", type=str, default="exp-fr")
	ap.add_argument("--block_size", type=int, default=100000)
	ap.add_argument("--config_path_stark", type=str, nargs="+")
	ap.add_argument("--alpha", type=float, default=None)
	ap.add_argument("--beta", type=float, default=None)
	ap.add_argument("--div_name", type=str, default="INDEX_RICHNESS")
	ap.add_argument("--div_subindex", type=int, default=0)

	args = ap.parse_args()

	exit(main(organization=args.organization, block_size=args.block_size, array_config_path_stark=args.config_path_stark, alpha=args.alpha, beta=args.beta, div_name=args.div_name, div_subindex=args.div_subindex))
