#!/usr/bin/env python3
#SBATCH --begin=now
#SBATCH --comment="Pipeline to run DUST on blocks of data."
#SBATCH --cpus-per-task=16
#SBATCH --chdir=.
#SBATCH --job-name=dust_blocks
#SBATCH --mail-user=louis.esteve@lisn.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=64g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal
#SBATCH --verbose
#SBATCH --time=24:00:00

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import os
import argparse
import glob
import conllu

def main(organization="exp-fr", block_size=10000, max_block=-1, array_config_path_stark=[]):
	new_organization = f"{organization}-block-resample-bs{block_size}"

	path_list = ["data", "input", new_organization]
	root_dir_new = os.sep.join(path_list)

	if not (os.path.exists(root_dir_new) and os.path.isdir(root_dir_new)):
		raise Exception(f"Does not exist: {repr(root_dir_new)}")

	config_rank = int(os.environ["SLURM_ARRAY_TASK_ID"])

	if config_rank >= len(array_config_path_stark):
		raise Exception(f"Cannot access STARK config of index {config_rank}")

	config_path_stark = array_config_path_stark[config_rank]

	if not (os.path.exists(config_path_stark) and os.path.isfile(config_path_stark)):
		raise Exception(f"Does not exist: {repr(config_path_stark)}")

	cmd_ = f"python3 {'' if __debug__ else '-OO'} src/target/main.py --ud_path {repr(root_dir_new)} --is_macro_dir 1 --config_file_only 1 --config_stark_path {repr(config_path_stark)} --extract-lang 0 --threads {os.environ['SLURM_CPUS_PER_TASK']} --config_diversutils_path config/diversutils/A1_*.ini --db_name_extra {repr(new_organization)}"
	print("cmd_:", cmd_)
	prcs_res = os.system(cmd_)

	if prcs_res != 0:
		return 1

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("--organization", type=str, default="exp-fr")
	ap.add_argument("--block_size", type=int, default=100000)
	ap.add_argument("--max_block", type=int, default=-1)
	ap.add_argument("--config_path_stark", type=str, nargs="+")

	args = ap.parse_args()

	exit(main(organization=args.organization, block_size=args.block_size, max_block=args.max_block, array_config_path_stark=args.config_path_stark))
