#!/usr/bin/env python3

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import os
import argparse
import glob
import conllu

def main(organization="exp-fr", block_size=10000, max_block=-1):
	root_dir = os.sep.join(["data", "input", organization])

	all_conllu_paths = [os.sep.join([root_dir, x]) for x in glob.glob("**/*.conllu", root_dir=root_dir, recursive=True)]

	new_organization = f"{organization}-block-resample-bs{block_size}"

	path_list = ["data", "input", new_organization]
	root_dir_new = os.sep.join(path_list)

	if not (os.path.exists(root_dir_new) and os.path.isdir(root_dir_new)):
		os.makedirs(root_dir_new)

	sent_counter = 0

	block_counter = 0

	path_list.append(f"block_{block_counter}")

	path_dir = os.sep.join(path_list)

	
	if not (os.path.exists(path_dir) and os.path.isdir(path_dir)):
		os.makedirs(path_dir)

	path_list.append("data.conllu")

	path_data = os.sep.join(path_list)

	print(path_data)

	f_out = open(path_data, "wt", encoding="utf-8")

	path_list.pop() # data.conllu
	path_list.pop() # block_x

	for conllu_path in all_conllu_paths:
		must_break = False
		f = open(conllu_path, "rt", encoding="utf-8")
		for sent in conllu.parse_incr(f):
			f_out.write(conllu.serializer.serialize(sent))

			sent_counter += 1

			if sent_counter % block_size == 0:
				f_out.close()
				block_counter += 1

				if max_block != -1 and block_counter >= max_block:
					must_break = True
					break

				path_list.append(f"block_{block_counter}")
				path_dir = os.sep.join(path_list)
				if not (os.path.exists(path_dir) and os.path.isdir(path_dir)):
					os.makedirs(path_dir)
			
				path_list.append("data.conllu")
			
				path_data = os.sep.join(path_list)

				path_list.pop() # data.conllu
				path_list.pop() # block_x
			
				print(path_data)

				f_out = open(path_data, "wt", encoding="utf-8")
		f.close()

		if must_break:
			break

	f_out.close()

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()
	ap.add_argument("--organization", type=str, default="exp-fr")
	ap.add_argument("--block_size", type=int, default=100000)
	ap.add_argument("--max_block", type=int, default=-1)

	args = ap.parse_args()

	exit(main(organization=args.organization, block_size=args.block_size, max_block=args.max_block))
