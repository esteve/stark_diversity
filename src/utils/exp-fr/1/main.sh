#!/bin/bash
#SBATCH --begin=now
#SBATCH --comment="Pipeline to run diverscontrol."
#SBATCH --cpus-per-task=16
#SBATCH --chdir=.
#SBATCH --job-name=diverscontrol
#SBATCH --mail-user=louis.esteve@lisn.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=64g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal
#SBATCH --verbose
#SBATCH --time=24:00:00

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

set -eu

debug=0

if [[ ${debug} -eq 1 ]] ; then
	python_debug_flag=""
else
	python_debug_flag="-OO"
fi

run_diverscontrol=1
run_on_base=0
run_on_extension=0

org="exp-fr"
index_a=0
index_b=1

#array_config_stark=(a1 a2 b1 c1 c2 c3 c4 c5 d1 d2 d3 d4 d5 e1 e2 e3 e4 e5)
#array_config_stark=(a1 c1)
#array_config_stark=(c2) # this one was computed (c2)
array_config_stark=(a1)
#array_config_stark=(d2)
array_config_stark_length=${#array_config_stark[@]}


if [[ ${run_diverscontrol} -eq 1 ]] ; then
	bash src/utils/run_diverscontrol.sh ${org} ${index_a} ${index_b} random
	bash src/utils/run_diverscontrol.sh ${org} ${index_a} ${index_b} best
fi

if [[ ${run_on_base} -eq 1 ]] ; then
	j=0
	while [[ ${j} -lt ${array_config_stark_length} ]] ;
	do
		python3 ${python_debug_flag} src/target/main.py \
			--ud_path data/input/${org}-${index_a} \
			--is_macro_dir 0 \
			--config_stark_path config/stark/${array_config_stark[${j}]}_*.ini \
			--config_diversutils_path config/diversutils/A1_*.ini \
			--config_file_only 1 \
			--threads 16 \
			--db_name_extra ldmsdbase ${org} ${index_a} ${index_b} base 0
		j=$((${j} + 1))
	done
fi

if [[ ${run_on_extension} -eq 1 ]] ; then
	for method in random best ;
	do
		echo "method: ${method}"
	
		if [[ "${method}" = "random" ]] ; then
			n_voc=10
		elif [[ "${method}" = "best" ]] ; then
			n_voc=1
		fi
	
		i=0
		while [[ ${i} -lt ${n_voc} ]] ;
		do
			dir_name="data/input/${org}/rebuilt/${method}/${i}"
			mkdir -p ${dir_name}
			echo "${dir_name}"
			path_out="${dir_name}/data.conllu"
			if ! [[ -f ${path_out} ]] ; then
				python3 ${python_debug_flag} src/utils/filter_based_on_sentence_id.py \
					--selected_ids data/target/${org}-${index_a}-${index_b}-${method}/selected_ids_${i}.txt \
					--file data/input/${org}-${index_b}/*.conllu \
					--path_out ${path_out}
			fi
	
			j=0
			while [[ ${j} -lt ${array_config_stark_length} ]] ;
			do
				python3 ${python_debug_flag} src/target/main.py \
					--ud_path ${dir_name} \
					--is_macro_dir 0 \
					--config_stark_path config/stark/${array_config_stark[${j}]}_*.ini \
					--config_diversutils_path config/diversutils/A1_*.ini \
					--config_file_only 1 \
					--threads 16 \
					--db_name_extra ldmsd ${org} ${index_a} ${index_b} ${method} ${i}
				j=$((${j} + 1))
			done
			i=$((${i} + 1))
		done
	done
fi

j=0
while [[ ${j} -lt ${array_config_stark_length} ]] ;
do
	config_index=${array_config_stark[${j}]}
	echo "======== config: ${config_index} ========"
	python3 ${python_debug_flag} src/utils/recompute_diversity_after_diverscontrol.py -o ${org} -a ${index_a} -b ${index_b} -c ${config_index} -d ${org} ${index_a} ${index_b}
	j=$((${j} + 1))
done
