#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import conllu
import diversutils
import numpy as np
from ups_lingdiv_utils import zipfian_fit
from collections import Counter

def compute_entropy_from_counter(counter_):
    counts = np.array(list(counter_.values()))
    p_i = counts / np.sum(counts)

    h = - np.sum(p_i * np.log(p_i))

    s = zipfian_fit.train(counts, verbose=False, n_iter=512, lr=0.02, enable_plot=False)

    n = len(p_i)

    return h, s, n

def compute_entropy_based_on_sentence_indices(sentence_counters, indices):
    counter_ = Counter()

    for index in indices:
        counter_ += sentence_counters[index]

    return compute_entropy_from_counter(counter_)


def main(filenames, conllu_key="form"):
    all_sentences = []
    for filename in filenames:
        f = open(filename, "rt", encoding="utf-8")
        all_sentences += conllu.parse(f.read())
        f.close()

    #category_counter = Counter(tok[conllu_key] for tok in sent \
    #for sent in all_sentences)

    counter_per_sentence = [Counter(tok[conllu_key] for tok in sent) for sent \
    in all_sentences]

    category_counter = Counter()

    for counter_ in counter_per_sentence:
        category_counter += counter_

    #print(category_counter)

    category_list = list(category_counter.items())
    #category_list.sort(key=lambda x:(x[1], x[0]), reverse=True) # sort first by
    # frequency and then by key
    category_list.sort(key=lambda x:(-x[1], x[0])) # sort first by
    # frequency and then by key

    mapper_key_to_zipf_rank = {item[0]: zipf_rank for zipf_rank, item in enumerate(category_list)}

    print(mapper_key_to_zipf_rank)

    avg_zipf_rank_per_sentence = []
    avg_zipf_rank_per_sentence_weighted = []

    for i, sent in enumerate(all_sentences):
        counter_ = counter_per_sentence[i]

        all_zipf_ranks_in_local_counter = []
        all_weights_in_local_counter = []

        for k, v in counter_.items():
            all_zipf_ranks_in_local_counter.append(mapper_key_to_zipf_rank[k])
            all_weights_in_local_counter.append(v)

        np_zipf_ranks = np.array(all_zipf_ranks_in_local_counter)
        np_weights = np.array(all_weights_in_local_counter)

        assert np_zipf_ranks.shape == np_weights.shape

        avg_zipf_rank_per_sentence.append(np.mean(np_zipf_ranks))
        avg_zipf_rank_per_sentence_weighted.append(np.mean(np_zipf_ranks * np_weights))

    print(np.std(avg_zipf_rank_per_sentence))
    print(np.std(avg_zipf_rank_per_sentence_weighted))

    #print(avg_zipf_rank_per_sentence)

    ########

    avg_zipf_rank_per_sentence_and_sentence_index = [(i, x) for i, x in \
    enumerate(avg_zipf_rank_per_sentence)]
    #avg_zipf_rank_per_sentence_and_sentence_index.sort(key=lambda x:x[1], \
    #reverse=True)
    avg_zipf_rank_per_sentence_and_sentence_index.sort(key=lambda x:x[1])

    #print(avg_zipf_rank_per_sentence_and_sentence_index)

    mapper_sentence_index_to_avg_zipf_rank_index = [i[0] for i in \
    avg_zipf_rank_per_sentence_and_sentence_index]

    #print(mapper_sentence_index_to_avg_zipf_rank_index)

    print("full:", compute_entropy_from_counter(category_counter))

    aggregated_counters_down = []
    for sent in all_sentences:
        aggregated_counters_down = Counter()
    i = len(all_sentences) - 1
    aggregated_counters_down[i] = \
    counter_per_sentence[mapper_sentence_index_to_avg_zipf_rank_index[i]]
    i -= 1
    while i >= 0:
        aggregated_counters_down[i] = \
        counter_per_sentence[mapper_sentence_index_to_avg_zipf_rank_index[i]] + \
        aggregated_counters_down[i+1]
        i -= 1

    for ratio in np.linspace(0.00, 1.00, 101):
        index_min = int(np.floor(len(all_sentences) * ratio))
        index_max = len(all_sentences)

        num_sentences = index_max - index_min
        ratio_of_used_sentences = num_sentences / len(all_sentences)

        try:
            #print(f"Using {ratio_of_used_sentences:.3f} of sentences;", compute_entropy_based_on_sentence_indices(counter_per_sentence, \
            #mapper_sentence_index_to_avg_zipf_rank_index[index_min:index_max]))
            print(f"Using {ratio_of_used_sentences:.3f} of sentences;",
            compute_entropy_from_counter(aggregated_counters_down[index_min]))
        except Exception as e:
            print(e)

    return 0

if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("--filename", type=str, nargs="+")
    ap.add_argument("--conllu_key", type=str, default="form")

    args = ap.parse_args()

    exit(main(filenames=args.filename, conllu_key=args.conllu_key))
