#!/usr/bin/env python3

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import os
import conllu
import diversutils
import matplotlib.pyplot as plt
from collections import Counter

def get_paths(paths):
	if type(paths) == list:
		for path in paths:
			yield get_paths(path)
	elif type(paths) == str:
		if os.path.isfile(paths):
			yield paths
		elif os.path.isdir(paths):
			for element in os.listdir(paths):
				for x in get_paths(os.sep.join([paths, element])):
					yield x

def main(paths, df0, df1="INDEX_RICHNESS", alpha=0.0, beta=0.0, conllu_key="form"):
	df0_key = getattr(diversutils, f"DF_{df0}")
	#df1_key = getattr(diversutils, f"DF_{df1}")
	
	axis0 = []
	axis1 = []
	plot_keys = []

	fig, ax = plt.subplots(figsize=(8,8))

	for path in paths:
		path = path.rstrip(os.sep)

		plot_key = path.split(os.sep)[-1]

		current_counter = Counter()

		token_count = 0

		for actual_path in get_paths(path):
			print(actual_path)

			f_in = open(actual_path, "rt", encoding="utf-8")
			for sent in conllu.parse_incr(f_in):
				print(sent.metadata["sent_id"], end="\n")
				token_count += len(sent)
				current_counter += Counter(tok[conllu_key] for tok in sent)
			f_in.close()

		g_index = diversutils.create_empty_graph(0, 0)
		for k, v in current_counter.items():
			diversutils.add_node(g_index, v)
		diversutils.compute_relative_proportion(g_index)

		div0 = diversutils.individual_measure(g_index, df0_key, alpha, beta)[0]
		#div1 = diversutils.individual_measure(g_index, df1_key, alpha, beta)[1]

		axis0.append(div0)
		#axis1.append(div1)
		axis1.append(token_count)
		plot_keys.append(plot_key)

		diversutils.free_graph(g_index)

	ax.scatter(axis0, axis1)
	plt.tight_layout()
	plt.savefig("plot_filewise.png")

	return 0

if __name__ == "__main__":
	ap = argparse.ArgumentParser()

	ap.add_argument("--path", type=str, nargs="+")
	ap.add_argument("--diversity_function", type=str, default="ENTROPY_SHANNON_WEAVER")

	args = ap.parse_args()

	exit(main(paths=args.path, df0=args.diversity_function))
