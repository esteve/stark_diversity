#!/bin/bash

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

# arguments:
# $1: organization (e.g. exp-fr)
# $2: index of base (e.g. 0)
# $3: index of extension (e.g. 1)
# $4: experiment (one of: random, best)

set -eu

dir_name="${1}-${2}-${3}-${4}"

mkdir -p "data/target/${dir_name}"

jsonl_content_key="text"
path_selected_ids="data/target/${dir_name}/selected_ids.txt"
lowercase=1
utf8_normalisation=1
maximum_number_of_documents=-1
maximum_number_of_tokens_globally=1600000000
maximum_number_of_tokens_per_file=-1
dump_in_voc="data/target/${dir_name}/dump_in_voc.bin"
dump_out_voc="data/target/${dir_name}/dump_out_voc.bin"
dump_in_id="data/target/${dir_name}/dump_in_id.bin"
dump_out_id="data/target/${dir_name}/dump_out_id.bin"
threads=4
display_interval=10000

if [[ "${4}" = "random" ]] ;
then
    use_random=1
    seed="1 2 3 4 5 6 7 8 9 10"
    selection_probability=0.1
    number_of_vocabularies_to_maintain=10
    exhaustivity="1 1 1 1 1"
elif [[ "${4}" = "best" ]] ;
then
    use_random=0
    seed="1"
    selection_probability=0.00
    number_of_vocabularies_to_maintain=1
    exhaustivity="100 50 25 10 1"
else
    echo "unknown experiment: ${4}"
    exit 1
fi

#git submodule update --recursive --init

pwd_=$(pwd)

cd diverscontrol
make bin/diverscontrol ENABLE_FILTER=0 TOKENIZATION_METHOD=0
cd ${pwd_}

cmd_="diverscontrol/bin/diverscontrol \
	-i $(find -L data/input/${1}-${2}/ -name "*.conllu") $(find -L data/input/${1}-${2}/ -name "*.jsonl") \
	-f $(find -L data/input/${1}-${3}/ -name "*.conllu") $(find -L data/input/${1}-${3}/ -name "*.jsonl") \
	-k ${jsonl_content_key} \
	-s ${path_selected_ids} \
	-l ${lowercase} \
	-u ${utf8_normalisation} \
	-n ${exhaustivity} \
	-m ${maximum_number_of_documents} \
	-y ${maximum_number_of_tokens_globally} \
	-z ${maximum_number_of_tokens_per_file} \
	-b ${dump_out_voc} \
	-B ${dump_out_id} \
	-t ${threads} \
	-d ${display_interval} \
	-r ${use_random} \
	-v ${seed} \
	-p ${selection_probability} \
	-g ${number_of_vocabularies_to_maintain}"

echo "cmd_: ${cmd_}"
${cmd_}
