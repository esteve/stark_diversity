#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import argparse
import pandas as pd
import bz2
import numpy as np
import math
import os
import scipy as sp
import matplotlib.pyplot as plt

def shannon_weaver_entropy(series, b=math.e):
    series_log = np.log(series) / np.log(b)
    neg_result = np.sum(np.multiply(series, series_log))
    result = -neg_result
    return result

def renyi_entropy(series, alpha, b=math.e):
    if alpha == 1.0:
        return shannon_weaver_entropy(series=series, b=b)
    series_to_alpha = np.power(series, alpha)
    result_log = np.log(np.sum(series_to_alpha)) / np.log(b)
    result = result_log / (1.0 - alpha)
    #print(result)
    return result

def main(config0, config1, alpha_min, alpha_max, alpha_step, paths, output):
    dict_config = {}
    set_identifiers = set()

    number_of_alpha_iterations = int(np.ceil((alpha_max - alpha_min) / alpha_step)) + 1

    all_alphas = np.linspace(alpha_min, alpha_max, number_of_alpha_iterations)

    print(all_alphas)

    for path in paths:
        filename = path.split(os.sep)[-1]

        print(filename)

        split_filename = filename.split("_", maxsplit=3)

        current_config = split_filename[2]
        current_identifier = split_filename[3]

        #print(current_config, current_identifier)

        #set_identifiers |= {current_identifier}

        if current_config not in dict_config:
            dict_config[current_config] = {}

        if path.endswith(".bz2"):
            opener = bz2.open
        else:
            opener = open

        f_in = opener(path, "rt", encoding="utf-8")
        df = pd.read_csv(f_in, sep="\t", usecols=["Absolute frequency"])
        f_in.close()

        series = np.array(df["Absolute frequency"], dtype=np.float64)

        series /= np.sum(series)

        """
        series_per_alpha = np.zeros(number_of_alpha_iterations)
        for i, alpha in enumerate(all_alphas):
            #print(i, type(i))
            series_per_alpha[i] = renyi_entropy(series=series, alpha=alpha, b=math.e)
        """
        series_per_alpha = np.array([renyi_entropy(series=series, alpha=alpha, b=math.e) for alpha in all_alphas])

        if current_identifier in dict_config[current_config]:
            raise Exception(f"Already present: {repr(current_identifier)}")

        dict_config[current_config][current_identifier] = series_per_alpha

    set_identifiers = set(dict_config[config0].keys()) & set(dict_config[config1].keys())

    if len(set_identifiers) != len(dict_config[config0].keys()):
        print("Warning: unequal length for config0")
    if len(set_identifiers) != len(dict_config[config1].keys()):
        print("Warning: unequal length for config1")

    sorted_keys = sorted(list(set_identifiers))

    #shape_probabilities = (len(sorted_keys)

    shape = (number_of_alpha_iterations, 2, len(sorted_keys))
    print(shape)

    
    tensor = np.ndarray(shape)
    for identifier_index, identifier_key in enumerate(sorted_keys):
        for config_index, config_key in enumerate([config0, config1]):
            #print(identifier_key, config_key)
            try:
                tensor[:, config_index, identifier_index] = dict_config[config_key][identifier_key]
            except Exception as e:
                print(e, identifier_key, config_key)

    #print(tensor)
    

    results_pearson_r = np.zeros(number_of_alpha_iterations, dtype=np.float64)
    results_pearson_p = np.zeros(number_of_alpha_iterations, dtype=np.float64)
    results_spearman_r = np.zeros(number_of_alpha_iterations, dtype=np.float64)
    results_spearman_p = np.zeros(number_of_alpha_iterations, dtype=np.float64)

    for i, alpha in enumerate(all_alphas):
        a = tensor[i, 0, :]
        b = tensor[i, 1, :]
        #print(a, a.shape, b, b.shape)
        pearson_r, pearson_p = sp.stats.pearsonr(a, b)
        results_pearson_r[i] = pearson_r
        results_pearson_p[i] = pearson_p
        spearman_r, spearman_p = sp.stats.spearmanr(a, b)
        results_spearman_r[i] = spearman_r
        results_spearman_p[i] = spearman_p

        print(pearson_r, pearson_p, spearman_r, spearman_p)

    fig, ax = plt.subplots(figsize=(8,4))
    ax.plot(all_alphas, results_pearson_r)
    ax.plot(all_alphas, results_spearman_r)
    plt.legend(["Pearson", "Spearman"])
    ax.set_xlabel(r"$\alpha$")
    ax.set_ylabel(r"Correlation over $H_{\alpha}$")
    try:
        ax.set_ylim(-1.05, +1.05)
    except Exception as e:
        print(e)
    plt.tight_layout()
    #plt.show()
    plt.savefig(output)

    return 0

if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("--config0", type=str, default="a2")
    ap.add_argument("--config1", type=str, default="d1")
    ap.add_argument("--alpha_min", type=float, default=0.0)
    ap.add_argument("--alpha_max", type=float, default=5.0)
    ap.add_argument("--alpha_step", type=float, default=0.05)
    ap.add_argument("--path", type=str, nargs="+")
    ap.add_argument("--output", type=str, default="correlations_against_alpha.png")

    args = ap.parse_args()

    exit(main(config0=args.config0, config1=args.config1, alpha_min=args.alpha_min, alpha_max=args.alpha_max, alpha_step=args.alpha_step, paths=args.path, output=args.output))
