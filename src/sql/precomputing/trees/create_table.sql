CREATE TABLE IF NOT EXISTS trees (
	desc    TEXT,
	size    INTEGER,
        CONSTRAINT      nn_trees_desc   CHECK   (desc NOT NULL),
        CONSTRAINT      ne_trees_desc   CHECK   (desc <> ''),
        CONSTRAINT      un_trees_desc   UNIQUE  (desc),
	CONSTRAINT      nn_trees_size   CHECK   (size NOT NULL),
	CONSTRAINT      ps_trees_size   CHECK   (size > 0)
) STRICT;
