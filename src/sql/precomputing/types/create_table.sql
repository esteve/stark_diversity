CREATE TABLE IF NOT EXISTS types (
	batch_id       INTEGER,
        tree_id        INTEGER,
        abs_freq       INTEGER,
        CONSTRAINT     fk_types_batch_id  FOREIGN KEY (batch_id) REFERENCES sentence_batches (rowid),
        CONSTRAINT     fk_types_tree_id   FOREIGN KEY (tree_id)  REFERENCES trees (rowid),
	CONSTRAINT     un_types_batch_tree_id    UNIQUE (batch_id, tree_id),
	CONSTRAINT     nn_types_abs_freq         CHECK  (abs_freq NOT NULL),
	CONSTRAINT     ps_types_abs_freq         CHECK  (abs_freq > 0)
) STRICT;
