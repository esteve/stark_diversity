CREATE TABLE IF NOT EXISTS batch_belonging (
	batch_id       INTEGER,
	sent_id        INTEGER,
        CONSTRAINT     fk_batch_belonging_batch_id  FOREIGN KEY (batch_id) REFERENCES sentence_batches (rowid),
        CONSTRAINT     fk_batch_belonging_sent_id   FOREIGN KEY (sent_id)  REFERENCES sentences (rowid),
	CONSTRAINT     un_batch_belonging_batch_sent_id    UNIQUE (batch_id, sent_id)
) STRICT;
