CREATE TABLE IF NOT EXISTS files (
	path    TEXT,
        CONSTRAINT      nn_files_path   CHECK   (path NOT NULL),
	CONSTRAINT      un_files_path   UNIQUE  (path)
) STRICT;
