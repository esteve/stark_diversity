CREATE TABLE IF NOT EXISTS sentences (
	sent_id        TEXT,
	file_id        INTEGER,
	num_tokens     INTEGER,
	CONSTRAINT     nn_sentences_sent_id       CHECK           (sent_id NOT NULL),
	CONSTRAINT     nn_sentences_file_id       CHECK           (file_id NOT NULL),
	CONSTRAINT     nn_sentences_num_tokens    CHECK           (num_tokens NOT NULL),
	CONSTRAINT     ps_sentences_num_tokens    CHECK           (num_tokens > 0),
	CONSTRAINT     fk_sentences_file_id       FOREIGN KEY     (file_id)                REFERENCES files (rowid),
	--CONSTRAINT     pk_sentences               PRIMARY KEY     (sent_id, file_id)
	CONSTRAINT     un_sentences_sent_and_file_id   UNIQUE     (sent_id, file_id)
) STRICT;
