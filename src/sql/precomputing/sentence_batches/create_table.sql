CREATE TABLE IF NOT EXISTS sentence_batches (
	name    TEXT,
	CONSTRAINT  nn_sentence_batches CHECK   (name NOT NULL),
	CONSTRAINT  un_sentence_batches UNIQUE  (name)
) STRICT;
