SELECT batch_id, tree_id, abs_freq FROM
    types AS t1
    JOIN
    (SELECT rowid FROM sentence_batches WHERE name LIKE ?) AS t2
    ON
    t1.batch_id=t2.rowid
ORDER BY batch_id;
