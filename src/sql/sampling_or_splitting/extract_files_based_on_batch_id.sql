SELECT DISTINCT (SELECT path FROM files WHERE rowid=t2.file_id) AS conllu_file, t2.sent_id AS conllu_sent_id FROM (
    (SELECT sent_id AS sentence_rowid FROM batch_belonging WHERE batch_id=?) AS t1
    JOIN
    sentences AS t2
    ON
    t1.sentence_rowid=t2.rowid
) AS t3;
--JOIN
--files AS t4
--ON
--t3.sent_id=
