SELECT t1.ts_min, t1.ts_max, t1.node_type, t1.div_name, t1.alpha, t1.beta, t2.tag, AVG(div_value) AS avg_ FROM (
	(SELECT * FROM diversities WHERE div_subindex=0 AND (alpha IS NULL OR (alpha>1.9 AND alpha<2.1)) AND subtreebank="all" AND ts_max=1) AS t1
	JOIN
	(SELECT * FROM ud_tags) AS t2
	ON
	t1.lang=t2.lang AND t1.treebank=t2.treebank
)
	GROUP BY t1.ts_min, t1.ts_max, t1.node_type, t1.div_name, t1.alpha, t1.beta, t2.tag
	ORDER BY t1.ts_min, t1.ts_max, t1.node_type, t1.div_name, t1.alpha, t1.beta, avg_
;
