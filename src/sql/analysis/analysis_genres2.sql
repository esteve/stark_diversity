SELECT t1.node_type, t2.tag, AVG(div_value) AS avg_ FROM (
	(SELECT * FROM diversities WHERE div_name="INDEX_SHANNON_EVENNESS" AND subtreebank="all") AS t1
	JOIN
	(SELECT * FROM ud_tags) AS t2
	ON
	t1.lang=t2.lang AND t1.treebank=t2.treebank
)
	GROUP BY t1.node_type, t2.tag
	ORDER BY t1.node_type, avg_
;
