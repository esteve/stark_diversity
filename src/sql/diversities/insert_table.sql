INSERT OR REPLACE INTO diversities (
	div_name,
	div_subindex,
	div_value,
	alpha,
	beta,
	lang,
	treebank,
	subtreebank,
	cfg_stark
)
VALUES (
	?, -- div_name
	?, -- div_subindex
	?, -- div_value
	?, -- alpha
	?, -- beta
	?, -- lang
	?, -- treebank
	?, -- subtreebank
	?  -- cfg_stark
);
