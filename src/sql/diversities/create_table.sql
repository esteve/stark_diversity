CREATE TABLE IF NOT EXISTS diversities (
	div_name		TEXT,
	div_subindex		INTEGER,
	div_value		REAL,
	alpha			REAL,
	beta			REAL,
	lang			TEXT,
	treebank		TEXT,
	subtreebank		TEXT,
	cfg_stark               TEXT,
	CONSTRAINT		nn_diversities_div_name    CHECK       (div_name NOT NULL),
	CONSTRAINT		nn_diversities_div_value   CHECK       (div_value NOT NULL),
	CONSTRAINT		nn_diversities_cfg_stark   CHECK       (cfg_stark NOT NULL),
	CONSTRAINT		pk_diversities             PRIMARY KEY (div_name, div_subindex, alpha, beta, lang, treebank, subtreebank, cfg_stark) ON CONFLICT REPLACE
);
