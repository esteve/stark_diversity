SELECT  t1.div_name,
        t1.div_subindex,
        t1.div_value,
        t1.alpha,
        t1.beta,
        t1.lang,
        t1.treebank,
        t1.subtreebank,
        t1.ts_min,
        t1.ts_max,
        t1.node_type,
        t1.complete_tree_type,
        t1.root_whitelist
FROM
(
SELECT  *
FROM    diversities
WHERE   div_name        =       ?
AND     lang            REGEXP  ?
AND     treebank        REGEXP  ?
)       AS t1
JOIN
(
SELECT  lang,
        treebank
FROM    ud_tags
WHERE   tag             REGEXP  ?
EXCEPT
SELECT  lang,
        treebank
FROM    ud_tags
WHERE   tag             REGEXP  ?
)       AS t2
ON      t1.lang         =       t2.lang
AND     t1.treebank     =       t2.treebank
;
