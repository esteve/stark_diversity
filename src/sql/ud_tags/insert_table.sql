INSERT OR REPLACE INTO ud_tags (
	lang,
	treebank,
	tag
) VALUES (
	?, -- lang
	?, -- treebank
	?  -- tag
);
