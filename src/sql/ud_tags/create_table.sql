CREATE TABLE IF NOT EXISTS ud_tags (
	lang		TEXT,
	treebank	TEXT,
	tag		TEXT,
	CONSTRAINT	nn_ud_tags_lang		CHECK		(lang NOT NULL),
	CONSTRAINT	nn_ud_tags_treebank	CHECK		(treebank NOT NULL),
	CONSTRAINT	nn_ud_tags_tag		CHECK		(tag NOT NULL),
	CONSTRAINT	pk_ud_tags		PRIMARY KEY	(lang, treebank, tag) ON CONFLICT REPLACE
) STRICT;
