#!/bin/bash

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

set -eu

PYTHON="python3"
NAME_ENV="delta"
ENABLE_PROXY=1


if [[ ${ENABLE_PROXY} -eq 1 ]] ; then
	#${PYTHON} -m pip config set global.proxy ${HTTPS_PROXY}
	PROXY_ARG="--proxy=${HTTPS_PROXY}"
else
	PROXY_ARG=""
fi

${PYTHON} -m pip install --upgrade ${PROXY_ARG} pip

mkdir -p ${NAME_ENV}
${PYTHON} -m venv ${NAME_ENV}
. ${NAME_ENV}/bin/activate
${PYTHON} -m pip install --upgrade ${PROXY_ARG} pip
${PYTHON} -m pip install --upgrade ${PROXY_ARG} -r requirements.txt
${PYTHON} -m pip install --upgrade ${PROXY_ARG} git+https://github.com/clarinsi/STARK@master
${PYTHON} -m pip install --upgrade ${PROXY_ARG} git+https://github.com/estevelouis/WG4@main

