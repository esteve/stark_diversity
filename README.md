# DELTA ($\Delta$)

This repository aims at measuring the diversity of
[STARK](https://github.com/clarinsi/STARK)'s output, _i.e._ the
diversity of syntactic structures.

# How to install

It is recommended to install using

```bash
bash environment.sh
```

This will pull the necessary repositories and packages in a `stark_diversity`
environment.

# How to run it

The main pipeline can be found under `src/target/main.py`.
It takes one argument:

```bash
python3 src/target/main.py --ud_path some/path/to/the/root/of/a/set/of/UD/treebanks
```

The script will apply stark to all treebanks in finds in `--ud_path`.
STARK's outputs are saved under `data/target/*.tsv` for future reuse.
For the ease of analysis, a `data/target/diversities.db` SQLite3 database is
generated, using what can be found under `src/sql/*.sql`.
The script can be run multiple times without fear, it will overwrite the
correct lines in the database, and thus data won't be duplicated.

# Testing

Tests are defined under `test/`.
They are used in the `.gitlab-ci.yml` for CI, and in `test/run_all_tests.sh`.
To ensure the pipeline is all set for you, run

```bash
bash test/run_all_tests.sh
```

# Experiments

## exp-fr

Create an environment by running

```bash
bash environment.sh
```

Put your data under `data/input/exp-fr`.

Then split data using

```bash
python3 src/utils/random_split.py --probability 0.5 0.5 --organization exp-fr --seed 1
```

or in SLURM

```bash
sbatch -v src/utils/wrapper_random_split.sh
```

This will generate `data/input/exp-fr-0` (base) and `data/input/exp-fr-1` (extension).

Update the `diverscontrol` submodule.

Then run

```bash
bash src/utils/exp-fr/main.sh
```

With `run_diverscontrol=1`, it will run 10 random samplings, and 1 optimized sampling.

With `run_on_base=1`, it will run STARK and DiversUtils on base (exp-fr-0).
With `run_on_extension=1`, it will run STARK and DiversUtils on extension (exp-fr-1).

At the end of the script, it runs
`src/utils/recompute_diversity_after_diverscontrol.py`, which retrieves STARK's
output and recomputes diversities.

## By block

First run
```bash
python3 src/utils/exp-fr/generate_blocks.py \
	--organization exp-fr \
	--block_size 10000
```
which will generate `data/input/exp-fr-block-resample-bs10000`, with
subdirectories `block_0`, `block_1`, etc.
You may add a `--max_block 5` to reduce the number of blocks it generates,
which is convenient for testing.

Then run
```
sbatch --verbose --array=0-1 src/utils/exp-fr/compute_block_diversity.py \
	--organization exp-fr \
	--block_size 10000 \
	--config_path_stark config/stark/a1_*.ini config/stark/d1_*.ini
```
but with `--array=0-1` changed to match the number of configurations you give
it.
