#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import scipy
import time
import networkx as nx
import zss
import numpy as np
import matplotlib.pyplot as plt
import math
import logging
import os

logging.getLogger("matplotlib").setLevel(logging.WARNING)

#TIMEOUT = 2.0
#TIMEOUT = 0.5
TIMEOUT = 0.1
#TIMEOUT = 0.05

ONLY_COMPUTE_UPPER_MATRIX_TRIANGLE = False

def matrix_to_plot(mat, trees, config_index, lang, treebank, computation_time_trees, computation_time_distances):
    #nt_val, nt_p = scipy.stats.normaltest(mat.flatten())

    values_no_trace = []
    for i in range(len(mat)):
        values_no_trace += list(mat[i][:i])
        values_no_trace += list(mat[i][i+1:])

    nt_val, nt_p = scipy.stats.normaltest(values_no_trace)

    avg = np.mean(values_no_trace)
    std = np.std(values_no_trace)

    #matmin = np.nanmin(mat)
    #matmax = np.nanmax(mat)
    matmin = 0
    matmax = np.nanmax(values_no_trace)
    fig, ax = plt.subplots(figsize=(16,16))
    #counts, bins = np.histogram(mat, bins=int(matmax)+1, range=(matmin,matmax))
    counts, bins = np.histogram(values_no_trace, bins=int(matmax)-int(matmin), range=(matmin,matmax))
    counts = counts.astype(np.float64) / np.sum(counts).astype(np.float64)
    ax.stairs(counts, bins, color="blue", label="edit_distance")

    tree_sizes = [len(t) for t in filter(lambda x:x != None, trees)]
    ts_max = np.max(tree_sizes)
    counts_ts, bins_ts = np.histogram(tree_sizes, bins=int(ts_max), range=(0, ts_max))
    counts_ts = counts_ts.astype(np.float64) / np.sum(counts_ts).astype(np.float64)
    ax.stairs(counts_ts, bins_ts, color="red", label="tree_size")

    ax.set_xlabel("distance")
    ax.set_ylabel("probability")
    ax.set_title(f"config: {config_index}, lang: {lang}, treebank: {treebank}, t_trees: {computation_time_trees:.3f}s, t_dists: {computation_time_distances:.3f}s, nt_p: {nt_p:.3e}, avg: {avg:.3f}, std: {std:.3f} (no trace)")
    ax.legend()
    plt.tight_layout()
    fig.savefig(os.sep.join(["img", f"distance_distribution_{config_index}_{lang}_{treebank}.png"]))
    fig.clear()
    plt.close(fig)


def zss_ts(t):
    count_ = 1
    for child_node in t.children:
        count_ += zss_ts(child_node)
    return count_
        

class Tree:
    def __init__(self, description, is_first_call=True, deprel=None):
        self.description = description.strip(" ")
        self.is_first_call = is_first_call
        self.deprel = deprel
        self.cardinality_nodes = 0
        self.dg = None
        self.zsst = None
        self.root_key = None
        self.parse_description(is_first_call=self.is_first_call)


    def __len__(self):
        if self.dg != None:
            return len(self.dg)
        if self.zsst != None:
            return zss_ts(self.zsst)
        return None    

    def to_nx(self, dg=None, index_parent=None):
        self.cardinality_nodes = 0
        return self._to_nx(df=dg, index_parent=index_parent)

    def _to_nx(self, dg=None, index_parent=None):
        if dg == None:
            if self.dg != None:
                return self.dg
            received_dg_is_none = True
            dg = nx.DiGraph()
        else:
            received_dg_is_none = False
        index_current = len(dg)
        dg.add_node(index_current, key=self.root_key)
        self.cardinality_nodes += 1
        if index_parent != None:
            dg.add_edge(index_parent, index_current, deprel=self.deprel)
        for subtree in self.subtrees:
            subtree._to_nx(dg, index_current)

        if received_dg_is_none:
            self.dg = dg

        return dg

    def to_zss(self):
        self.cardinality_nodes = 0
        return self._to_zss()

    def _to_zss(self):
        if self.zsst != None:
            return self.zsst

        self.zsst = zss.Node(f"{self.deprel}+{self.root_key}")
        self.cardinality_nodes += 1
        for subtree in self.subtrees:
            self.zsst.addkid(subtree._to_zss())

        return self.zsst


    def parse_description(self, is_first_call=True):
        if (not is_first_call) and self.description[0] == '(' and self.description[-1] == ')':
            self.description = self.description[1:-1]
        elements = []
        current_element = ""
        current_element_start = 0
        current_element_end = 0
        i = 0
        n = len(self.description)
        open_parenthesis_count = 0
        while i < n:
            ch = self.description[i]
            if ch == '(':
                if open_parenthesis_count == 0:
                    current_element_start = i
                open_parenthesis_count += 1
            elif ch == ')':
                open_parenthesis_count -= 1
                """
                if open_parenthesis_count == 0:
                    current_element_end = i + 1
                """
                current_element_end = i + 1
            elif ch == ' ' and open_parenthesis_count == 0:
                current_element = self.description[current_element_start:current_element_end]
                elements.append(current_element)
                current_element_start = i + 1
                current_element_end = i + 1
            else:
                current_element_end = i + 1
            i += 1
        if current_element_start != current_element_end:
            current_element = self.description[current_element_start:current_element_end]
            elements.append(current_element)
    
        #print(elements)
        if __debug__:
            for elem, count in zip(elements, range(len(elements))):
                print(count, elem)
    
        # find the root
        found_root = False
        root_index = -1
        for j in range(1, len(elements), 2):
            #print(elements[j])
            if elements[j][0] == '>':
                found_root = True
                root_index = j - 1
                break
        if not found_root:
            root_index = len(elements) - 1
        self.root_key = elements[root_index][:]

        if __debug__:
            print(f"root at index {root_index}: {elements[root_index]}")
    
        #tree = Tree(root_key=elements[root_index], subtrees
        indices_lower = list(range(root_index - 2, -1, -2))
        indices_upper = list(range(root_index + 2, len(elements), 2))
        self.subtrees = []
        for index in indices_lower + indices_upper:
            if index < root_index:
                index_relation = index + 1
            elif index > root_index:
                index_relation = index - 1
            else:
                raise Exception("incorrect index")
            subtree = Tree(description=elements[index], is_first_call=False, deprel=elements[index_relation][1:])
            """
            self.subtrees.append({
                "deprel": elements[index][1:],
                "tree": subtree,
            })
            """
            self.subtrees.append(subtree)
        if __debug__:
            print(f"subtrees: {self.subtrees}")

    def _display(self, depth=0):
        if __debug__:
            print(f"[{self.description}] [{self.root_key}] [{self.deprel}]")
        s = ("\t" * depth)
        if self.deprel == None:
            s += "ROOT "
        else:
            s += f"{self.deprel} "
        s += self.root_key + "\n"
        #s += "-" * 32 + "\n"
        for subtree in self.subtrees:
            s += subtree._display(depth=depth+1)
        return s

    def display(self):
        print(self._display())

encountered_distances = {}

#def _distance_two_trees(a, b):

encountered_nan = 0
encountered_not_nan = 0

def distance_two_trees(a, b, tree_edit_distance_timeout=TIMEOUT, graph_method="zss"):
    global encountered_nan
    global encountered_not_nan

    try:
        a_ = a.description
        b_ = b.description
        if a_ > b_:
            a, b = b, a
            a_, b_ = b_, a_
        #if (a not in encountered_distances) or (b not in encountered_distances[a]):
        if a_ not in encountered_distances:
            encountered_distances[a_] = {}
        if b_ not in encountered_distances[a_]:
            """
            #dist = _distance_two_trees(a, b)
            dist = 0
            dist += int(a.root_key != b.root_key)
            max_subtrees = max([len(a.elements), len(b.elements)])
    
            for a_i in range(max_subtrees):
                for b_i in range(max_subtrees):
            """
    
            if graph_method == "nx":
                g_a = a.to_nx()
                g_b = b.to_nx()
        
                dist = nx.graph_edit_distance(
                    g_a,
                    g_b,
                    #node_match=lambda x_,y_: x_["key"] == y_["key"],
                    #edge_match=lambda x_,y_: x_["deprel"] == y_["deprel"],
                    node_subst_cost=lambda x_,y_: int(x_["key"] != y_["key"]),
                    node_del_cost=lambda x_:1,
                    node_ins_cost=lambda x_:1,
                    edge_subst_cost=lambda x_,y_: int(x_["deprel"] != y_["deprel"]),
                    edge_del_cost=lambda x_:1,
                    edge_ins_cost=lambda x_:1,
                    roots=(0,0),
                    timeout=tree_edit_distance_timeout
                )
            elif graph_method == "zss":
                g_a = a.to_zss()
                g_b = b.to_zss()
    
                #dist = zss.distance(A=g_a, B=g_b, get_children=zss.Node.get_children, insert_cost=lambda x:1, remove_cost=lambda x:1, update_cost=lambda x,y: 1)
                dist = zss.simple_distance(A=g_a, B=g_b)
    
            if dist == None or math.isnan(dist):
                dist = max(len(g_a), len(g_b))
                encountered_nan += 1
            else:
                encountered_not_nan += 1
    
            encountered_distances[a_][b_] = dist
            return dist
        return encountered_distances[a_][b_]
    except Exception as e:
        print(e)
        return None


def tree_descriptions_to_distance_matrix(descriptions, config_index, lang, treebank, tree_edit_distance_timeout=TIMEOUT):
    global encountered_nan
    global encountered_not_nan

    t_start_trees = time.time()

    for desc in descriptions:
        t = Tree(desc)
        if __debug__:
            t.display()

    encountered_nan = 0
    encountered_not_nan = 0

    trees = [Tree(desc) for desc in descriptions]
    trees_scipy = [[t] for t in trees]

    t_end_trees = time.time()
    t_delta_trees = t_end_trees - t_start_trees

    if len(trees) > 0:
        t_start_distances = time.time()
        
        """ # works, do not remove
        mat = np.zeros((len(descriptions), len(descriptions)))
        for i in range(len(descriptions)):
            a = trees[i]
            if ONLY_COMPUTE_UPPER_MATRIX_TRIANGLE:
                min_index_ = i+1
            else:
                min_index_ = 0
            for j in range(min_index_, len(descriptions)):
                b = trees[j]
                t_start = time.time()
                d = distance_two_trees(a, b, tree_edit_distance_timeout=tree_edit_distance_timeout)
                t_end = time.time()
                t_delta = t_end - t_start
                if __debug__:
                    print(f"{i} & {j}: {t_delta:.3f}s")
                mat[i, j] = d
                if ONLY_COMPUTE_UPPER_MATRIX_TRIANGLE:
                    mat[j, i] = d
    
        if encountered_nan + encountered_not_nan > 0:
            logging.info(f"nan: {((encountered_nan)/(encountered_nan+encountered_not_nan))*100:.2f}%")
        """
    
        mat = scipy.spatial.distance.cdist(trees_scipy, trees_scipy, metric=lambda a, b: distance_two_trees(a[0], b[0], tree_edit_distance_timeout=tree_edit_distance_timeout))
    
        t_end_distances = time.time()
    
        t_delta_distances = t_end_distances - t_start_distances

        matrix_to_plot(mat=mat, trees=trees, config_index=config_index, lang=lang, treebank=treebank, computation_time_trees=t_delta_trees, computation_time_distances=t_delta_distances)

        return mat
    else:
        return np.zeros(0)
            

if __name__ == "__main__":
    """
    descriptions = [
#    "_ >amod _",
#    "_ >nmod _",
#    "_ <amod _ >nmod (_ <case _) >amod _ >acl (_ <punct _ <mark _)",
#    "_ >punct _",
#    "((_ <det _) <nsubj _ >obj (_ <nummod _) >punct _) <advcl _ <nsubj _ >obj (_ <det _) >xcomp (_ <obj _) >obl (_ <case _ <det _ >nmod (_ <case _ <det _)) >punct _",
#    "((_ >fixed _ >fixed _ >fixed _) <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _) >conj (_ <punct (_ >fixed _) <cc _ <det _ >nmod (_ <case _ <amod _) >acl (_ >obj (_ <det _) >obl (_ <case _ <det _ <nummod _)))) >punct _) <obl (_ <det _) <nsubj _ <cop _ >obl (_ <case _ <det _ <nummod _ >nmod (_ <case _)) >punct _",
#    "((_ >fixed _ >fixed _) <advmod _ <case _ <det _ <amod _ >acl (_ <mark _ <nsubj _ >obj (_ <det _)) >punct _) <obl (_ >flat _) <nsubj _ >obl (_ <case _) >obj (_ <det _ <amod _) >obl (_ <case _) >obl (_ <punct _ <case _ <det _ <amod _ >nmod (_ <case _ <det _)) >punct _",
#    "(_ <advmod _ <nummod _ >nmod (_ <case _ <det _ >acl (_ >obj ((_ >fixed _) <det _)))) <nsubj _ <aux _ >obj ((_ >fixed _ >fixed _) <advmod _ <det _ >amod _) >conj (_ <cc (_ <nummod _) <nsubj _ <aux _ >obj (_ <det _ >amod _ >nmod (_ <case _ <det _))) >punct _",
#    "(_ <advmod _ >punct _) <advcl (_ <det _) <nsubj _ <aux _ <aux _ >obj (_ <det _) >punct _",
#    "(_ <advmod _ >punct _) <advcl (_ <det _) <nsubj _ <aux _ >obl (_ <case _ <amod _ >appos (_ <punct _ >nmod _ >conj (_ <punct _ >nmod (_ <case _ <det _) >acl (_ <mark _ >obl (_ <case _ >flat _))) >conj (_ <punct _ >nmod (_ <case _ <det _ <amod _) >punct _) >punct _) >conj (_ <cc _ >appos (_ <punct _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _)) >punct _))) >punct _",
#    "(_ <det _ <amod _) <nsubj _ >ccomp (_ <mark _ >conj (_ <cc _ <mark _ >obj (_ <det _ <amod _ >amod _ >appos (_ <punct _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _)) >conj (_ <punct _ <det _ >acl (_ <nsubj _ >advmod _ >obj (_ <det _ >conj (_ <cc _ <det _) >nmod (_ <case _ <det _ >nmod (_ <case _ <det _))))) >conj (_ <punct _ <det _ >acl (_ <nsubj _ <expl _ >obl (_ <case _ <det _ >acl (_ <obl _ <expl _ >nsubj (_ <amod _ <det _ >amod _) >obl (_ <case _ <det _ >nmod (_ <case _ <amod _ <punct (_ <nsubj _ >punct _ >obj _) <parataxis _ <punct _ <det _ >nmod (_ <case _ <det _ >amod _))))))))))) >punct _",
    "_ >amod _",
    "_ >nmod _",
    "_ <amod _ >nmod (_ <case _) >amod _ >acl (_ <punct _ <mark _)",
    "_ >punct _",
    "((_ <det _) <nsubj _ >obj (_ <nummod _) >punct _) <advcl _ <nsubj _ >obj (_ <det _) >xcomp (_ <obj _) >obl (_ <case _ <det _ >nmod (_ <case _ <det _)) >punct _",
    "((_ >fixed _ >fixed _ >fixed _) <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _) >conj (_ <punct (_ >fixed _) <cc _ <det _ >nmod (_ <case _ <amod _) >acl (_ >obj (_ <det _) >obl (_ <case _ <det _ <nummod _)))) >punct _) <obl (_ <det _) <nsubj _ <cop _ >obl (_ <case _ <det _ <nummod _ >nmod (_ <case _)) >punct _",
    "((_ >fixed _ >fixed _) <advmod _ <case _ <det _ <amod _ >acl (_ <mark _ <nsubj _ >obj (_ <det _)) >punct _) <obl (_ >flat _) <nsubj _ >obl (_ <case _) >obj (_ <det _ <amod _) >obl (_ <case _) >obl (_ <punct _ <case _ <det _ <amod _ >nmod (_ <case _ <det _)) >punct _",
    "(_ <advmod _ <nummod _ >nmod (_ <case _ <det _ >acl (_ >obj ((_ >fixed _) <det _)))) <nsubj _ <aux _ >obj ((_ >fixed _ >fixed _) <advmod _ <det _ >amod _) >conj (_ <cc (_ <nummod _) <nsubj _ <aux _ >obj (_ <det _ >amod _ >nmod (_ <case _ <det _))) >punct _",
    "(_ <advmod _ >punct _) <advcl (_ <det _) <nsubj _ <aux _ <aux _ >obj (_ <det _) >punct _",
    "(_ <advmod _ >punct _) <advcl (_ <det _) <nsubj _ <aux _ >obl (_ <case _ <amod _ >appos (_ <punct _ >nmod _ >conj (_ <punct _ >nmod (_ <case _ <det _) >acl (_ <mark _ >obl (_ <case _ >flat _))) >conj (_ <punct _ >nmod (_ <case _ <det _ <amod _) >punct _) >punct _) >conj (_ <cc _ >appos (_ <punct _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _)) >punct _))) >punct _",
    "(_ <advmod _ >punct _) <advmod _ <expl _ <expl _ >ccomp (_ <mark _ >obj (_ <det _ >nmod (_ <case _) >amod (_ >obl (_ <case _ <det _)))) >punct _",
    "(_ <advmod _ >punct _) <advmod _ <nsubj _ >xcomp (_ >obl (_ <case _ <det _ >acl _)) >punct _",
    "(_ <amod _ <det _ >flat _) <nsubj _ <advmod _ <aux _ <advmod _ >obl (_ <case _ <det _) >obl (_ <case _ <det _ >nmod (_ <case _ <det _) >nmod (_ <punct _ <nummod _ >punct _)) >punct _",
    "(_ <amod _ <det _ >nmod (_ <case _ <det _) >nmod (_ <case _ <advmod _ <nummod _ >nmod (_ <case _)) >acl (_ >obj (_ <det _))) <nsubj _ <punct _ >obl (_ <case _ <det _ >amod _) >punct _",
    "(_ <amod _ <det _) <nsubj _ >xcomp (_ <advmod _ <advmod _ <aux _) >punct _",
    "(_ <amod _ <nummod _ <amod _ >amod (_ <punct _ <amod _ >obl (_ <case _ <det _))) <nsubj _ <aux _ >obl (_ <case _) >obl (_ <det _ >nmod (_ >nmod _)) >punct _",
    "(_ <case _ <amod _ <det _) <obl (_ <punct _ <mark (_ <det _) <nsubj _ <aux _ >obl (_ <case _ <det _ >amod _ >conj (_ <cc _ <case _ <det _)) >punct _) <advcl (_ <det _ >amod (_ >conj (_ <cc _)) >nmod (_ <case _ <det _)) <nsubj _ >xcomp (_ <aux _ <advmod _) >punct _",
    "(_ <case _ <amod _ >punct _) <obl (_ <det _ >conj (_ <cc _ <det _ >amod _)) <nsubj _ <cop _ >obl (_ <case _ <det _ >amod _ >nmod (_ <case _ <det _)) >punct _",
    "(_ <case _ <det _ <amod _ >punct _) <obl (_ <det _ >amod _) <nsubj _ >obj _ >xcomp (_ <mark _ >obj (_ <det _) >advcl (_ <mark _ >obj (_ <det _ >amod _))) >punct _",
    "(_ <case _ <det _ <amod _ >punct _) <obl (_ <det _ >nmod (_ <case _ <det _ >acl _)) <nsubj _ <aux _ >obj _ >obl (_ <case _ <det _) >punct _",
    "(_ <case _ <det _ <nummod _ >punct _) <obl (_ <det _) <nsubj _ <aux _ <aux _ >xcomp (_ <mark _ >obj (_ <det _ <amod _ >nmod (_ <case _ <det _) >conj (_ <punct _ <cc _ <advmod _ <det _ <amod _))) >punct _",
    "(_ <case _ <det _ >acl (_ <aux _ >advmod _ >obj (_ <det _ >nmod (_ <case _) >nmod (_ <case _ <amod _))) >punct _) <obl (_ <det _ >nmod (_ <case _ >nmod (_ <case (_ >dep (_ <dep _)) <det _ >nmod (_ <case _ >nmod _ >acl (_ >obl (_ <case _ >amod (_ >conj (_ <cc _)))))))) <nsubj _ <aux _ >obl (_ <case _ <det _ <amod _ >nmod (_ <case _)) >punct _",
    "(_ <case _ <det _ >acl (_ <aux _ >obj (_ <det _ >nmod (_ <case _) >amod _ >amod (_ >obl (_ <case _ <det _ >amod _)))) >punct _) <obl _ <expl _ <aux _ >ccomp (_ <mark _ >obj (_ <det _ >nmod (_ <case _) >nmod (_ <case (_ >obl (_ <dep _)) <det _ >nmod (_ <case _ >nmod _))) >obl (_ <case _ >amod (_ >conj (_ <cc _))) >obl (_ <punct _ <case _ <det _ <amod _ >nmod (_ <case _))) >punct _",
    "(_ <case _ <det _ >acl (_ >obj (_ <det _ >amod _)) >punct _) <obl _ <expl _ <advmod _ <expl _ >advmod _ >obj (_ <det _ >amod _ >acl (_ >ccomp (_ <mark (_ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _))) <nsubj _ >obj (_ <det _ >nmod (_ <case _ >nmod (_ <case _ <det _)))))) >punct _",
    "(_ <case _ <det _ >acl (_ >obl (_ <case _ <det _ >nmod (_ <case _))) >punct _) <obl (_ <det _ >amod _) <nsubj _ <cop _ <advmod _ >conj (_ <cc (_ <det _ >amod _) <nsubj _ <cop _ <advmod _ >dep (_ <case _ <det _ >amod _)) >punct _",
    "(_ <case _ <det _ >acl (_ >obl (_ <case _ >amod _ >amod _)) >acl (_ >obl (_ <case _ <det _ >nmod _ >nmod (_ <case _ >amod _) >nmod (_ <case _ <det _) >appos (_ <punct _ >punct _))) >punct _) <obl (_ <det _ >nmod _ >acl (_ >obl ((_ <nummod _) <nmod _ <case _ <det _ >nmod (_ <case _)))) <nsubj _ <cop (_ <case _) <obl _ <case (_ >conj (_ <cc _)) <nummod _ >obl (_ <case _ >nmod (_ <case _)) >punct _",
    "(_ <case _ <det _ >acl (_ >obl (_ <case _ >amod _)) >punct _) <obl _ <expl _ <advmod _ <cop _ <advmod _ >csubj (_ <mark _ >obj (_ <det _ >nmod _ >appos (_ <punct _ >nmod (_ <case _ <nummod _ >nmod (_ <case _)) >conj (_ <punct _ >nmod (_ <case _ <nummod _ >nmod (_ <case _) >nmod (_ <case _))) >punct _))) >punct _",
    "(_ <case _ <det _ >acl (_ >obl (_ <case _)) >punct _) <obl _ <expl _ <advmod _ <aux _ <advmod _ <aux _ >obj (_ <det _ >conj (_ <punct _ <det _ >amod _) >conj (_ <cc _ <det _ >nmod (_ <case _ >amod _))) >punct _",
    "(_ <case _ <det _ >acl (_ >xcomp (_ >obl (_ <case _ <det _))) >punct _) <obl (_ <det _ >amod _ >nmod (_ <case _ <nummod _ >nmod (_ <case _))) <nsubj _ <aux _ >conj (_ <cc (_ <det _) <nsubj _ <aux _ >obl (_ <case _ <nummod _ >nmod (_ <case _) >nmod (_ <case _)) >obl (_ <case _ <det _)) >punct _",
    "(_ <case _ <det _ >acl _ >punct _) <obl _ <nsubj _ <aux _ <cop _ <advmod _ >obl (_ <case _ <det _ >amod (_ >obl (_ <case _ <det _ >nmod (_ <case _)))) >punct _",
    "(_ <case _ <det _ >amod (_ <advmod _) >nmod (_ <case _ >conj (_ <cc _ <case _)) >punct _) <obl _ <nsubj _ <cop _ <det _ <amod _ >nmod (_ <case _) >acl (_ <nsubj _ >obj (_ <det _) >obl (_ <case _ <det (_ <advmod _) <amod _ >nmod (_ <case _ <det _ >nmod (_ >nmod _)) >nmod (_ <case _ <det _ >nmod (_ <case _)))) >punct _",
    "(_ <case _ <det _ >amod (_ <advmod _) >punct _) <obl _ <nsubj _ >advmod (_ >fixed _) >xcomp (_ >obj (_ <det _) >obl (_ <case _ <det _ >conj (_ <cc _ <case _ <det _))) >punct _",
    "(_ <case _ <det _ >amod _ >acl (_ >obj (_ <det _)) >punct _) <obl _ >xcomp (_ >obj _ >obl (_ <case _ <det _ >amod _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ >nmod (_ <case _ <det _)))))) >punct _",
    "(_ <case _ <det _ >amod _ >nmod (_ <case _ <det _) >punct _) <obl _ >obj (_ >nmod _) >punct _",
    "(_ <case _ <det _ >amod _ >nmod _ >nmod (_ <case _ <det _ >nmod (_ <case _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ >amod _))))) >punct _) <obl (_ <det _ >acl (_ >obl (_ <case _))) <nsubj _ >xcomp (_ <aux _ >obl (_ <case _ <amod _ >nmod (_ <case _ <det _))) >punct _",
    "(_ <case _ <det _ >conj (_ <cc _ <case _ <case _)) <obl (_ <punct _) <nsubj _ <obj _ <aux _ >obl (_ <case _) >advcl (_ <mark _ <mark _ >xcomp (_ >nmod (_ <case _) >nmod (_ <case _ <punct _ <det _ >punct _)) >punct _)",
    "(_ <case _ <det _ >nmod (_ <case _ <det _) >punct _) <obl (_ <det _ >nmod (_ <case _ >acl (_ >obl (_ <det _ >nmod (_ <case _))))) <nsubj _ <aux _ <aux _ >obl (_ <case _ >nmod (_ <case _ <det _ >appos (_ <punct _ >amod _ >punct _))) >obl (_ <case _ <det _ >nmod (_ <case _ <nummod _ >amod _)) >punct _",
    "(_ <case _ <det _ >nmod (_ <case _ <det _) >punct _) <obl _ <nsubj _ <advmod _ <aux _ <advmod _ <advmod _ >obj _ >obl (_ <case _ <det _ >amod _) >punct _",
    "(_ <case _ <det _ >nmod (_ <case _ >flat _ >nmod (_ <punct _ >punct _) >appos (_ <punct _ <amod _ >amod _ >amod _ >nmod (_ <case _) >nmod (_ <case _) >conj (_ <cc _ >amod _ >nmod (_ <case _ <det _ >appos (_ <punct _ >punct _))))) >punct _) <obl (_ <det _ >nmod (_ <case _ <det _ >acl (_ <obj _ <nsubj _ >obl (_ <case _ <det _ >nmod (_ <case _))))) <nsubj _ <advmod _ >advmod _ >obj (_ <det _ <amod _) >punct _",
    "(_ <case _ <det _ >nmod (_ <case _) >amod _ >punct _) <obl (_ <det _) <nsubj _ <advmod _ <cop _ <advmod _ <advmod _ >punct _",
    "(_ <case _ <det _ >nmod (_ <case _) >punct _) <obl (_ <det _ >amod _ >amod _ >nmod (_ <case _ <nummod _)) <nsubj _ <advmod _ >obj (_ <advmod _ <det _ >amod _ >conj (_ <punct (_ >fixed _) <cc _ <det _ >amod _ >acl (_ >obl (_ <case _ <det _ >amod _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _ >amod _)))))) >punct _",
    "(_ <case _ <det _ >nmod (_ <case _) >punct _) <obl _ <nsubj _ <aux _ >obl (_ <case _ <det _ <amod _) >punct _",
    "(_ <case _ <det _ >nmod (_ <case _) >punct _) <obl _ <nsubj _ >advmod _ >xcomp (_ <aux _ >obl (_ <case _ <det _ >amod (_ >obl (_ <case _ <det _ >nmod (_ <case _ <det _))))) >punct _",
    "(_ <case _ <det _ >nmod _ >punct _) <obl (_ <det _ >nmod (_ <case _ <det _)) <nsubj _ <aux _ >obj (_ <det _ >nmod (_ <case _ >nmod (_ <case _ >nmod _))) >obl (_ <case _ <det _ >nmod (_ <case _)) >advcl ((_ >fixed _) <mark (_ <det _ >nmod (_ <case _ >nmod _)) <nsubj _ <advmod _ <aux _ <advmod _ <aux _ >obl (_ <case _ >amod _)) >punct _ >parataxis (_ >obj (_ >nmod _)) >punct _ >punct _",
    "(_ <case _ <det _ >nmod _ >punct _) <obl (_ <det _ >nmod (_ <case _ <nummod _)) <nsubj _ <expl _ >obl (_ <case _ >nmod (_ <punct _ >punct _) >conj (_ <punct _ <case _ <det _ >nmod (_ <case _) >acl (_ <punct _ <obl _ <nsubj _ >xcomp (_ >obj (_ <det _))))) >punct _",
    "(_ <case _ <det _ >punct _) <obl (_ <det _ >nmod (_ <case _ <det _ <amod _) >acl (_ <punct _ >obl (_ <case _ <det _ >nmod (_ <case _)) >conj (_ <punct _ >obl (_ <case _ <det _ <amod _)) >punct _ >conj (_ <cc _ >obl (_ <case _ <det _) >obj (_ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _ >acl (_ >obl (_ <case _ <det _ >nmod (_ <case _) >amod _)))))) >punct _)) <nsubj _ >obl (_ <punct _ <det _ >advmod _ >punct _) >xcomp (_ <mark _ >obl (_ <case _ <det _)) >punct _",
    "(_ <case _ <det _ >punct _) <obl (_ <det _) <nsubj _ <cop _ <advmod _ <det _ >nmod (_ <case _) >conj (_ <cc _ <advmod _ <advmod _ <det _) >punct _",
    "(_ <case _ <det _ >punct _) <obl (_ <det _) <nsubj _ <cop _ <advmod _ >obl (_ <case _ <det _) >advcl (_ <punct _ <mark (_ <det _ >nmod (_ <case _ >amod _ >appos (_ <punct _ <det _ >punct _))) <nsubj _) >punct _",
    "(_ <case _ <det _ >punct _) <obl _ <expl _ <advmod _ >advmod _ >advmod _ >nsubj (_ <advmod _ <nummod _ >conj (_ <cc _ <det _)) >obl (_ <case _ <det _ >nmod (_ <case _ <det _)) >punct _",
    "(_ <case _ <det _ >punct _) <obl _ <nsubj _ >ccomp (_ <mark (_ <det _) <nsubj _ >obj (_ <dep _ <advmod _)) >punct _",
    "(_ <case _ <det _ >punct _) <obl _ <nsubj _ >obj (_ <nummod _ <amod _) >conj (_ <cc _ <case _ <det _ >nmod (_ <case _ >amod _) >acl (_ <nsubj _ <advmod _ >obj (_ <advmod (_ >conj (_ <punct _) >conj (_ <cc _)) <det _))) >punct _",
    "(_ <case _ <det _) <obl _ <nsubj _ <aux _ <expl _ >punct _",
    "(_ <case _ <det _) <obl _ <punct (_ <det _) <nsubj _ <aux _ >ccomp (_ <mark (_ <case _ <det _ >acl (_ <obl (_ <det _ >nmod (_ <case _ >nmod (_ <case _ <det _))) <nsubj _ <aux _ >obl (_ <case _ <det _ >conj (_ <cc _ <det _ >nmod (_ <case _)))) >punct _) <obl _ <expl _ <aux _ >xcomp _ >csubj (_ <mark _ <punct _ >obj (_ <det _ >acl (_ <obj (_ <det _ >nmod _) <nsubj _ >obl (_ <case _))) >punct _)) >punct _ >advcl (_ >ccomp (_ <mark (_ <det _ <amod _) <nsubj _ >advmod _ >obl (_ <punct _ <case _ >flat _ >conj (_ <cc _)) >obl (_ <punct _ <advmod _ <case _ >punct _)) >punct _)",
    "(_ <case _ <det _) <obl _ >nsubj (_ >appos (_ <punct _ >nmod (_ <case _ >punct _ >nmod _ >nmod (_ <case _ <det _))) >conj (_ <punct _ <det _ >appos (_ <punct _ >nmod (_ <case _))) >conj (_ <punct _ <det _ >appos (_ <punct _ >nmod (_ <case _ >amod _))) >conj (_ <punct _ <det _ >appos (_ <punct _ >nmod (_ <case _) >punct _)) >conj (_ <cc _ <det _)) >punct _",
    "(_ <case _ >flat _ >punct _) <obl (_ <det _) <nsubj _ >obj (_ <det _ >nmod (_ <case _ >nmod (_ <case _ <nummod _ >nmod (_ <case _)) >acl (_ >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ >flat _))))))) >punct _",
    "(_ <case _ >nmod (_ <case _ <det _ >amod _ >acl (_ >obj (_ <det _ >acl (_ <obl _ <expl _ >nsubj (_ <det _ >nmod (_ <case _ <det _ >nummod _)))))) >punct _) <obl (_ <det _ >nmod _) <nsubj _ <aux _ >obl (_ <case _) >xcomp (_ <mark _ <advmod _ <advmod _ >obj (_ <det _) >conj (_ <cc _ <mark _ <obj _)) >punct _",
    "(_ <case _ >obl (_ <case _) >punct _) <obl _ >obj (_ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _)) >acl (_ <punct _ <advmod _ >obl (_ <case _ <det _) >punct _)) >punct _",
    "(_ <case _ >punct _) <obl (_ <det _ >amod (_ >obl (_ <case _ <det _))) <nsubj _ <aux _ <aux _ >advmod (_ >obl (_ <case _ >acl (_ >obj (_ <det _ >amod _)))) >punct _",
    "(_ <case _ >punct _) <obl (_ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ >flat _))) <nsubj _ <iobj _ >xcomp _ >punct _",
    "(_ <case _ >punct _) <obl (_ <det _) <nsubj _ <cop _ <advmod _ >punct _",
    "(_ <case _ >punct _) <obl (_ >flat _ >appos (_ <punct _ <det _ >acl _ >nmod (_ <case _ >flat _ >nmod _))) <nsubj _ <aux _ >obj (_ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod _))) >conj (_ <cc _ <aux _ >obj _ >obl (_ <case _ >nmod (_ <case _))) >punct _",
    "(_ <case _ >punct _) <obl _ <nsubj _ >xcomp (_ >ccomp (_ <mark _ <punct (_ <case _ <det _ >conj (_ <cc _ <case _ <det _) >punct _) <obl (_ <det _) <nsubj _ >advmod _ >xcomp (_ <aux _ >obl (_ <case _ <det _ >amod _)))) >punct _",
    "(_ <case _) <obl (_ <punct _ <case _ <det _ >amod _ >amod (_ <punct _) >acl (_ <nsubj _ <expl _ >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod _))) >punct _) <obl (_ <det _ >nmod (_ <case _ <det _ >nmod _)) <nsubj _ <advmod _ <cop _ <advmod _ >obl (_ <case _ <det _ >amod (_ >conj (_ <cc _))) >punct _",
    "(_ <case _) <obl (_ <punct _ <case _ >nmod (_ <case _ <det _) >punct _) <obl _ <nsubj _ >obl (_ <case _ <det _ >amod _ >nmod (_ <case _ <det _)) >conj (_ <cc _ <punct (_ <mark _ <nsubj _ <aux _ >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod _)) >punct _) <advcl _ <nsubj _ <advmod _ >xcomp (_ >obj (_ <det _ >amod _))) >punct _",
    "(_ <det _ <amod _ >amod _ >nmod (_ <case _)) <nsubj _ >obj (_ <nummod _ >nmod (_ <case _ <det _ >nmod (_ <case _) >nmod (_ <case _ <det _) >nmod (_ <case _ <det _ >nmod (_ <case _)))) >punct _",
    "(_ <det _ <amod _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _)))) <nsubj _ <aux _ <cop _ <advmod _ >conj (_ <cc _ <punct (_ >fixed _ >punct _) <advmod _ <nsubj _ <cop _ <case _ >acl (_ <mark _ >obj (_ <det _ >nmod (_ <case _ <det _ <amod _ >nmod (_ <case _ <det _ >nmod (_ <case _) >amod _))))) >punct _",
    "(_ <det _ <amod _ >nmod (_ <case _ <det _)) <nsubj _ <cop _ <det _ >acl (_ <mark _ >ccomp (_ >obj ((_ >fixed _) <det _ >conj (_ <cc (_ >fixed _) <det _)) >obl (_ <punct _ <case _ <det _ >conj (_ <cc _ <case _)) >advmod (_ <punct _) >obl (_ <case _ <det _ >obl (_ <case _ <det _ >conj (_ <punct _ <case _ <det _))))) >punct _",
    "(_ <det _ <amod _ >nmod (_ <punct _ <case _ <det _ >nmod (_ <case _)) >conj (_ <punct _ <det _ >nmod _)) <nsubj _ <punct _ <aux _ >obl (_ <case _ <det _) >obj (_ <det _ >nmod (_ <case _)) >advmod (_ <punct _ >fixed _ >obl (_ <case _ <det _)) >punct _",
    "(_ <det _ <amod _) <nsubj _ <aux _ >advcl (_ <mark _ <expl _ <expl _ >conj (_ <cc _ <advmod _ <case _ <det _ <nummod _)) >obl (_ <punct _ >nmod (_ >nmod _)) >obl (_ <punct _ <case _ <det _ >nmod (_ <case _ <det _ >amod _ >amod _ >nmod (_ <case _ <det _ >nmod (_ <case _) >conj (_ <cc _ >nmod (_ <case _)) >nmod (_ <case _ <det _)) >acl (_ <nsubj _ <expl _ >obl (_ <det _ <amod _) >obl (_ <punct _ <case _ <det _ >nmod (_ <case _ >flat _)) >obl (_ <punct _ <case _ <det _ >amod _)))) >punct _",
    "(_ <det _ <amod _) <nsubj _ <aux _ >obj (_ <det _ >conj (_ <cc _) >acl (_ >obl (_ <case _) >conj (_ <punct _ >obl (_ <case _ >obl (_ <case _ <nummod _)))) >acl (_ <punct _ <nsubj _ <obl _ <aux _ <advmod _ >obj (_ <det _))) >conj (_ <punct _ <cc _ <expl _ <aux _ >obl (_ <case _ <det _ >nmod (_ <case _) >nmod (_ <case _ <det _ >nmod (_ <case _ <nummod _) >nmod (_ <case _ <det _))) >punct _)",
    "(_ <det _ <amod _) <nsubj _ <aux _ >obl (_ <case _ >obl (_ <case _ <det _ >amod _ >acl (_ >obl (_ <case _)))) >conj (_ <cc _ <aux _ >obj _ >obl (_ <case _ <det _ >nmod (_ <case _ >nmod (_ <case _ <det _ >amod _ >conj (_ <cc _ <case _ <det _)) >acl (_ >obl (_ <case _ <det _ >nmod (_ <case _ <nummod _)))))) >punct _",
    "(_ <det _ <amod _) <nsubj _ <cop _ <det _ >nmod (_ <case _ <det _ >amod _ >amod _) >punct _",
    "(_ <det _ <amod _) <nsubj _ <expl _ >obl (_ <case _ <det _) >advcl (_ <mark (_ <det _) <nsubj _ <aux _ >obl (_ <case _ >amod _) >obl (_ <case _ <det _ >nmod (_ <case _ >nmod (_ <case _ >nmod (_ <case _))))) >punct _",
    "(_ <det _ <amod _) <nsubj _ >ccomp (_ <mark _ >conj (_ <cc _ <mark _ >obj (_ <det _ <amod _ >amod _ >appos (_ <punct _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _)) >conj (_ <punct _ <det _ >acl (_ <nsubj _ >advmod _ >obj (_ <det _ >conj (_ <cc _ <det _) >nmod (_ <case _ <det _ >nmod (_ <case _ <det _))))) >conj (_ <punct _ <det _ >acl (_ <nsubj _ <expl _ >obl (_ <case _ <det _ >acl (_ <obl _ <expl _ >nsubj (_ <amod _ <det _ >amod _) >obl (_ <case _ <det _ >nmod (_ <case _ <amod _ <punct (_ <nsubj _ >punct _ >obj _) <parataxis _ <punct _ <det _ >nmod (_ <case _ <det _ >amod _))))))))))) >punct _",
    "(_ <det _ <amod _) <nsubj _ >obl (_ <case _) >parataxis (_ <punct (_ <case _ >punct _) <obl _ <nsubj _ <cop _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _ >nmod _) >acl (_ <nsubj _ >xcomp (_ <mark _ >obj (_ <det _ >nmod (_ <case _ <det _)))))) >punct _",
    "(_ <det _ >acl (_ <punct (_ <nmod _ <det _) <nsubj _ <aux _ >obj (_ <det _ >nmod (_ <case _ <det _)) >punct _)) <nsubj _ <aux (_ <case _) <obl _ <aux _ >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >amod _)) >punct _",
    "(_ <det _ >acl (_ >conj (_ <cc _))) <nsubj _ <cop _ <det _ >amod (_ >obl (_ <case _ <advmod _)) >amod (_ <punct _ >obl (_ <case _ <advmod _)) >punct _",
    "(_ <det _ >acl (_ >obl (_ <case _ <det _) >obl (_ <case _ <det _ >amod _))) <nsubj _ <aux _ >obl (_ <case _) >obj (_ <det _ >nmod (_ <case _ <det _) >acl (_ <punct _ <nmod _ <det _)) >punct _ >parataxis (_ >obj (_ >nmod _)) >punct _ >punct _",
    "(_ <det _ >acl (_ >xcomp (_ <case _) >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >amod _ >nmod (_ <case _ <nummod _))))) <nsubj _ <aux _ <cop _ >ccomp (_ <mark _ >obl (_ <case _ <det _ >acl _ >nmod (_ <case _))) >punct _",
    "(_ <det _ >acl _ >nmod (_ <case _) >nmod (_ <case _ <det _ >nmod _ >appos (_ <punct _ >nmod (_ >conj (_ <cc _)) >punct _))) <nsubj _ <cop _ <det _ >amod _ >amod (_ <punct _ >obl (_ <case _ <det _)) >nmod (_ <punct _ <case _ <nummod _ >nmod (_ <case _)) >acl (_ >obl (_ <case _ <det _ >nmod (_ <case _ <nummod _ >nmod (_ <case _) >nmod (_ <case _)) >nmod (_ <case _ <det _ >conj (_ <cc _ <mark _ <nsubj _ <expl _ >obl (_ <case _ <det _ >nmod (_ <case _) >amod _))))) >punct _",
    "(_ <det _ >acl _ >nmod (_ <case _)) <nsubj (_ <punct _ <case _ <det _ >acl (_ >obl (_ <case _ <det _)) >punct _) <obl _ <cop _ <det _ >amod _ >nmod (_ <case _ <nummod _ >nmod (_ <case _ >nmod (_ <case _ >amod _))) >acl (_ <advmod _ >obl (_ <case _ <det _ >amod _ >nmod (_ <case _ >nmod (_ <case _ <nummod _ >nmod (_ <case _) >nmod (_ <case _))) >nmod (_ <case (_ >fixed _ >fixed _) <advmod _ <det _ >nmod (_ <case _ <det _)))) >punct _",
    "(_ <det _ >amod (_ >fixed _)) <nsubj _ <aux _ <advmod _ >ccomp (_ <mark (_ <det _) <nsubj _ >obl (_ <case _ >amod (_ <obl _ <punct _)) >obj (_ <det _ >nmod (_ <case _ >amod _ >acl _) >appos (_ <punct _ >punct _) >conj (_ <punct _ <det _ >nmod (_ <case _) >appos (_ <punct _ >punct _)) >conj (_ <cc _ <det _ >nmod (_ <case _) >appos (_ <punct _ >punct _)) >nmod (_ <case _ <det _ >amod _ >amod _)) >conj (_ <punct _ <cc _ <mark _ <nsubj _ <advmod _ >advmod _ >obj (_ <det _ >amod _) >obl (_ <case _ >nmod (_ <case _ <det _ >nmod (_ <case _ >nmod (_ <case _ >nmod (_ <case _ >nmod (_ <case _ >acl (_ >obl (_ <case _ <det _ >nmod (_ <case _ >appos (_ <punct _ >conj (_ <cc _) >punct _)))))))))))) >punct _",
    "(_ <det _ >amod _ >acl (_ <nsubj _ <aux _ <aux _ >advmod (_ >fixed _ >obl (_ <case _ <det _ <amod _ >nmod _)) >conj (_ <cc _ <nsubj _ <advmod _ <aux _ <advmod _ <aux _ >advmod _))) <nsubj _ <aux _ >obl (_ <case _ >nmod (_ <case _ <det _ >nmod (_ <case _))) >obl (_ <case _ <det _ >nmod _) >punct _",
    "(_ <det _ >amod _ >acl (_ <punct _ >xcomp (_ <cop _) >punct _)) <nsubj _ <aux _ <aux _ >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod _ >nmod (_ <case _ <det _))) >punct _",
    "(_ <det _ >amod _ >acl (_ >obl (_ <case _ <det _ >nmod (_ >flat _)))) <nsubj _ >conj (_ <punct _ <cc (_ <det _ >nmod (_ <case _ <det _)) <nsubj _ <advmod _ <aux _ <advmod _ <aux _) >punct _",
    "(_ <det _ >amod _ >acl (_ >obl (_ <case _ <det _ >nmod _))) <nsubj _ <aux _ >ccomp (_ <mark _ <cop _ <det _ >amod _ >conj (_ <cc _ <det _ >nmod (_ <case _ <det _ >nmod (_ <case _) >nmod (_ <nummod _ >nmod (_ <case _))))) >punct _",
    "(_ <det _ >amod _ >acl (_ >obl (_ <case _ >amod _))) <nsubj _ <aux _ >obl (_ <case _ <det _ >amod _) >obl (_ <punct _ >amod _ >amod _ >nmod (_ <case _ <det _ >amod _) >nmod (_ <punct _ <case _ <det _ >nmod (_ >nmod (_ <case _) >conj (_ <cc _ >nmod (_ <case _ <nummod _))) >punct _) >acl (_ >obl (_ <case _ <det _ >nmod (_ <case _ >acl _ >nmod (_ <case _ <det _ >nmod (_ <case _ >amod _) >nmod _ >nmod (_ <case _ <nummod _)))))) >punct _",
    "(_ <det _ >amod _ >amod (_ >obl (_ <case _ <det _ >amod _))) <nsubj _ <aux _ >obl (_ <case _ <det _ >amod _ >acl (_ >obl (_ <case _ <det _) >obl (_ <case _ <det _ >acl (_ >obl (_ <case _))))) >punct _",
    "(_ <det _ >amod _ >amod (_ >obl (_ <case _))) <nsubj _ <cop _ >advmod _ >punct _",
    "(_ <det _ >amod _ >amod _) <nsubj _ <advmod _ <aux _ >xcomp (_ <aux _ >obl (_ <case _)) >punct _",
    "(_ <det _ >amod _ >nmod (_ <case _ <det _ >nmod (_ <case _ <det _ >amod (_ >conj (_ <cc _))))) <nsubj _ <aux _ <aux _ >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ >amod _))) >advmod (_ >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod (_ >nmod (_ <case _ >nmod (_ <case _ <det _ >nmod _)) >conj (_ <cc _ >nmod (_ <case _ >nmod (_ <case _ <det _ >nmod _))))))) >punct _ >parataxis (_ >obj (_ >nmod _)) >punct _ >punct _",
    "(_ <det _ >amod _ >nmod (_ <case _ <det _) >nmod (_ <case _ <det _ >nmod (_ <case _)) >nmod (_ <case _ >amod _ >amod _ >nmod (_ <case _ <nummod _ >nmod (_ <case _) >nmod (_ <case _)))) <nsubj _ <cop _ <case _ <nummod _ >nmod (_ <case _) >punct _",
    "(_ <det _ >amod _ >nmod (_ <case _ <det _)) <nsubj _ <aux _ <aux _ >obl (_ <case _ <nummod _) >obl (_ <case _ <nummod _ >nmod (_ <case _ <det _ >acl (_ >obl (_ <case _))) >nmod (_ <case _ <nummod _ >nmod (_ <case _ <det _ >acl (_ >obl (_ <case _ <det _))))) >punct _",
    "(_ <det _ >amod _ >nmod (_ <case _ <det _)) <nsubj _ <aux _ <aux _ >obl (_ <case _ <nummod _) >obl (_ <case _ <nummod _) >punct _",
    "(_ <det _ >amod _ >nmod (_ <case _ <det _)) <nsubj _ >xcomp (_ <aux _ >obl (_ <case _ <det _)) >punct _",
    "(_ <det _ >amod _ >nmod (_ <case _)) <nsubj _ <aux _ <advmod _ >advmod (_ <case _ >conj (_ <cc _ <case _)) >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >amod _)) >punct _",
    "(_ <det _ >amod _ >nmod (_ <case _)) <nsubj _ <cop _ <det _ >amod _ >nmod (_ <punct _ <case _ <det _ >nmod (_ <case _ <nummod _ >nmod (_ <case _ <nummod _)) >punct _) >punct _",
    "(_ <det _ >amod _ >nmod (_ <punct (_ >fixed _) <case _ <det _ >amod _ >amod _ >punct _)) <nsubj _ <aux _ <aux _ >advmod (_ <advmod _) >obl (_ <case _ <det _ >nmod _) >punct _ >parataxis (_ >obj (_ >nmod _)) >punct _ >punct _",
    "(_ <det _ >amod _) <nsubj _ <advmod _ <aux _ <advmod _ >obl (_ <case _ >amod _) >conj (_ <cc _ <advmod _ >advmod _ >obl (_ <case _ <det _ >nmod (_ <case _ <det _ >nmod _ >amod _ >amod (_ >fixed _)))) >parataxis (_ <punct _ >obj (_ >nmod _) >punct _) >punct _",
    "(_ <det _ >amod _) <nsubj _ <aux _ >obl (_ <case _ <det _ >amod _) >punct _",
    "(_ <det _ >amod _) <nsubj _ <aux _ >obl (_ <case _ <det _ >conj (_ <cc _ <det _ >nmod (_ <case _ >nmod (_ <case _)))) >obl (_ <punct _ <case _ <det _ >nmod (_ <case _ <det _)) >punct _",
    ][:]

    mat = tree_descriptions_to_distance_matrix(descriptions)

    print(mat)
    avg = np.mean(mat)
    std = np.std(mat)
    #"""

    """
    distances = scipy.spatial.distance.pdist([[t] for t in [trees]], metric=distance_two_trees)
    avg = np.mean(distances)
    std = np.std(distances)
    """

    """
    print(avg, std)
    matmin = np.nanmin(mat)
    matmax = np.nanmax(mat)
    counts, bins = np.histogram(mat, bins=int(matmax)+1, range=(matmin,matmax))
    plt.stairs(counts, bins)
    plt.tight_layout()
    plt.show()
    """

    exit(0)
