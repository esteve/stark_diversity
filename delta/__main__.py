#!/usr/bin/env python3
#SBATCH --begin=now
#SBATCH --comment="Pipeline to run STARK and diversutils."
#SBATCH --cpus-per-task=16
#SBATCH --chdir=.
#SBATCH --job-name=diversity
#SBATCH --mail-user=louis.esteve@lisn.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=64g
#SBATCH --nodes=1
#SBATCH --output=log/%x-%A-%j-%a-stdout.log
#SBATCH --error=log/%x-%A-%j-%a-stderr.log
#SBATCH --open-mode=append
#SBATCH --priority=normal

#
# 	DELTA - A pipeline to measure linguistic diversity
#
# Copyright (c) 2024  LISN / Université Paris-Saclay / CNRS  Louis Estève (louis.esteve@universite-paris-saclay.fr), Jozef Stefan Institute / University of Ljubljana  Kaja Dobrovoljc (kaja.dobrovoljc@ff.uni-lj.si)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    # Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    # Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#


import os
os.environ["TQDM_DISABLE"] = "1"

from typing import *
import string
import bz2
import stark
import diversutils
import logging
import argparse
import glob
import io
import tempfile
import re
import conllu
import pandas as pd
import numpy as np
import networkx as nx
import sqlite3 as sql
import time
import configparser
from lxml import etree
import torch
import json

from delta.utils import *
from delta.normalization import *
from delta.tree import *
from delta.database import *

if not __debug__:
    stark_logger = logging.getLogger("stark")
    stark_logger.setLevel(logging.WARNING)
    del(stark_logger)

logger = logging.getLogger("delta")
logging.basicConfig(encoding="utf-8", level=logging.DEBUG if __debug__ else logging.INFO, format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')

TORCH_DTYPE = torch.float32
TORCH_DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

logger.info(f"TORCH_DTYPE: {repr(TORCH_DTYPE)}")
logger.info(f"TORCH_DEVICE: {repr(TORCH_DEVICE)}")

def diversities_to_sql(g_index: int, cursor: sql.Cursor, sql_insert_table_diversities_query: str, treebank_name: str, subtreebank: str, lang: Union[str, None], config_stark: dict, config_diversutils: dict, cfg_id_stark: str) -> None:
    begin_transaction_trial_count = 0
    begin_transaction_trial_max = 4
    open_transaction_start = 0
    while True:
        try:
            cursor.execute("BEGIN TRANSACTION;")
            open_transaction_start += 1
            break
        except sql.OperationalError as e:
            logger.warning(f"sql.OperationalError: {repr(e)}")
            begin_transaction_trial_count += 1
            if begin_transaction_trial_count >= begin_transaction_trial_max:
                raise Exception(f"Could not begin a transaction even after {begin_transaction_trial_count} attempts")
            else:
                time.sleep(10.0)

    enable_disparities = config_diversutils["diversutils.diversity.function"]["enable_disparity_functions"]

    # Iterate over diversity functions

    for i in dir(diversutils):
        if not i.startswith("DF_"):
            continue

        key_part = "_".join(i.split("_")[2:]).lower()

        key_for_enable = f"enable_{key_part}"

        if key_for_enable not in config_diversutils["diversutils.diversity.function"]:
            logger.warning(f"Cannot know whether to run {i}, not running it")
            continue

        if config_diversutils["diversutils.diversity.function"][key_for_enable] == 0:
            continue

        t = time.time()

        if (not enable_disparities) and i.startswith("DF_DISPARITY"):
            continue
        if i == "DF_DISPARITY_SCHEINER": # this requires an actual vector space to compute hyperspheres
            continue
        if i in ["DF_ENTROPY_SHANNON_WEAVER", "DF_DISPARITY_PAIRWISE"] or (i.startswith("DF_INDEX") and i != "DF_INDEX_HILL_EVENNESS"):
            iter_alpha = False
            iter_beta = False
        elif i in ["DF_ENTROPY_GOOD", "DF_INDEX_HILL_EVENNESS", "DF_DISPARITY_STIRLING"]:
            iter_alpha = True
            iter_beta = True
        else:
            iter_alpha = True
            iter_beta = False

        # ======== PARAMETERS ========

        key_alpha_min = f"{key_part}_alpha_min"
        key_alpha_max = f"{key_part}_alpha_max"
        key_alpha_step = f"{key_part}_alpha_step"

        if key_alpha_min in config_diversutils["diversutils.diversity.parameter"]:
            alpha_min = float(config_diversutils["diversutils.diversity.parameter"][key_alpha_min])
        else:
            alpha_min = 0.0

        if key_alpha_max in config_diversutils["diversutils.diversity.parameter"]:
            alpha_max = float(config_diversutils["diversutils.diversity.parameter"][key_alpha_max])
        else:
            alpha_max = 3.0

        if key_alpha_step in config_diversutils["diversutils.diversity.parameter"]:
            alpha_step = float(config_diversutils["diversutils.diversity.parameter"][key_alpha_step])
        else:
            alpha_step = 0.2

        key_beta_min = f"{key_part}_beta_min"
        key_beta_max = f"{key_part}_beta_max"
        key_beta_step = f"{key_part}_beta_step"

        if key_beta_min in config_diversutils["diversutils.diversity.parameter"]:
            beta_min = float(config_diversutils["diversutils.diversity.parameter"][key_beta_min])
        else:
            beta_min = 0.0

        if key_beta_max in config_diversutils["diversutils.diversity.parameter"]:
            beta_max = float(config_diversutils["diversutils.diversity.parameter"][key_beta_max])
        else:
            beta_max = 3.0

        if key_beta_step in config_diversutils["diversutils.diversity.parameter"]:
            beta_step = float(config_diversutils["diversutils.diversity.parameter"][key_beta_step])
        else:
            beta_step = 0.2

        # ======== EXPLORING PARAMETERS ========

        for alpha in np.linspace(alpha_min, alpha_max, round((alpha_max - alpha_min) / alpha_step + 1)):
            if iter_alpha:
                alpha = float(alpha)
            else:
                alpha = None
            for beta in np.linspace(beta_min, beta_max, round((beta_max - beta_min) / beta_step + 1)):
                if iter_beta:
                    beta = float(beta)
                else:
                    beta = None

                if iter_alpha:
                    if iter_beta:
                        diversity = diversutils.individual_measure(g_index, getattr(diversutils, i), alpha, beta)
                    else:
                        diversity = diversutils.individual_measure(g_index, getattr(diversutils, i), alpha)
                else:
                    diversity = diversutils.individual_measure(g_index, getattr(diversutils, i))

                counter = 0
    
                for j in diversity:
                    if __debug__:
                        try:
                            logger.info(f"{i[3:]}_{counter} (alpha={alpha:.4f}, beta={beta:.4f}): {j}")
                        except Exception as e:
                            logger.warning(f"Exception: {repr(e)}")
    
                    try:
                        tuple_sql_args = (
                            i[3:],
                            counter,
                            j,
                            alpha,
                            beta,
                            lang,
                            treebank_name,
                            subtreebank,
                            cfg_id_stark,
                        )
                        cursor.execute(sql_insert_table_diversities_query, tuple_sql_args)
                    except sql.IntegrityError as e:
                        logger.warning(f"sql.IntegrityError: {repr(e)}; {' / '.join([repr(y) for y in tuple_sql_args])}")
    
                    counter += 1

                if not iter_beta:
                    break
            if not iter_alpha:
                break

        logger.info(f"Computed {i} in {time.time() - t:.3f}s")

    if open_transaction_start:
        cursor.execute("END TRANSACTION;")
        open_transaction_start -= 1

def main_bulk_diversity_computation(ud_path: Union[list[str], Generator[str, None, None]] = [], config_path_stark: str = "none", config_file_only: int = 0, config_path_diversutils: str = "none", extract_lang: int = 1, tree_size_min: Union[int, None] = None, tree_size_max: Union[int, None] = None, head: list[str] = [], node_type: Union[str, None] = None, threads: int = -1, enable_disparities: bool = False, tree_edit_distance_timeout: float = 0.05, target_subtreebank: str = "all", complete_tree_type: int = 1, db_name_extra: list[str] = [], organization: str = "UniversalDependencies", is_macro_dir: Union[int, bool] = 1, save_mode: str = "none", ignore_existing_files: Union[int, bool] = 0, pattern_treebank: str = "^.*$", pattern_lang: str = "^.*$") -> int:
    logger.info("start: main_bulk_diversity_computation")

    t_main_bulk_diversity_computation_start = time.time()

    for d in ["data", os.sep.join(["data", "target"])]:
        os.makedirs(d, exist_ok=True)

    opener = bz2.open
    suffix = ".bz2"

    if config_path_stark == "none":
        key = "SLURM_ARRAY_TASK_ID"
        if key not in os.environ:
            raise Exception(f"config_path_stark and {key} are not defined, one must be")
        config_index_int = int(os.environ[key])
        if config_index_int < 0:
            raise Exception(f"{key} must be at least 0")
        all_options = sorted(glob.glob(os.sep.join(["config", "stark", "*.ini"])))
        if config_index_int >= len(all_options):
            raise Exception(f"{key} ({config_index_int}) is too high, there are only {len(all_options)} options")
        config_path_stark = all_options[config_index_int]

    logger.info(f"config_path_stark: {config_path_stark}")
    logger.info(f"config_path_diversutils: {config_path_diversutils}")

    if threads == -1:
        key = "SLURM_CPUS_PER_TASK"
        if key not in os.environ:
            threads = 4
            logger.info(f"{key} is not defined, defaulting to {threads}")
        else:
            threads = int(os.environ[key])

    ALL_ENV_VARIABLES_TO_DISPLAY = [
        "SLURM_ARRAY_JOB_ID",
        "SLURM_ARRAY_TASK_ID",
        "SLURM_ARRAY_TASK_COUNT",
        "SLURM_ARRAY_TASK_MIN",
        "SLURM_ARRAY_TASK_MAX",
        "SLURM_CPUS_PER_TASK",
    ]
    for env_var_to_display in ALL_ENV_VARIABLES_TO_DISPLAY:
        value = ""
        if env_var_to_display in os.environ:
            value = os.environ[env_var_to_display]
        logger.info(f"{env_var_to_display:<32}= {repr(value)}")

    cfgparser_diversutils = configparser.ConfigParser()
    if type(config_path_diversutils) == str:
        cfgparser_diversutils.read(config_path_diversutils)

    config_diversutils: dict[str, Any] = {}

    for k in cfgparser_diversutils.keys():
        config_diversutils[k] = {}
        for m in cfgparser_diversutils[k].keys():
            val = cfgparser_diversutils[k][m]
            if m.startswith("enable_") or m.startswith("num_") or m.startswith("force_") or m.endswith("use_log10") or m.endswith("batch_size"):
                config_diversutils[k][m] = int(val)
            elif "alpha" in m or "beta" in m:
                config_diversutils[k][m] = float(val)
            else:
                config_diversutils[k][m] = val

    logger.info(f"target_subtreebank: {target_subtreebank}")

    if head == ["none"] or head == ["None"]:
        head = []

    # Connect to SQL database

    organization_normalized = normalize(organization)
    config_index = config_path_stark.split(os.sep)[-1].split("_")[0]
    config_index_normalized = normalize(config_index)

    db_path_list = ["data", "target", "diversities", organization_normalized, config_index_normalized]
    os.system(f"mkdir -p {os.sep.join(db_path_list)}")
    if config_file_only:
        # ==== STARK ====

        if type(config_path_stark) != str:
            raise Exception("If config_file_only is set to True, config_path_stark must be a str")

        db_path_list.append(f"diversities_{'-'.join(db_name_extra) if db_name_extra != [] else 'default'}.db")

        if config_path_stark != None:
            config = stark.read_settings(config_path_stark, stark.parse_args([]))
            if tree_size_min == None or tree_size_max == None:
                ts = config["tree_size"]
                if "-" in ts:
                    tree_size_min, tree_size_max = ts.split("-")
                else:
                    tree_size_min = tree_size_max = int(ts)
            if complete_tree_type == None and ("complete" in config or "complete_tree_type" in config):
                complete_tree_type = 0 if (("complete" in config and config["complete"] in ["no", False, 0]) or ("complete_tree_type" in config and config["complete_tree_type"] in ["no", False, 0])) else 1
            if node_type == None and "node_type" in config:
                node_type = config["node_type"]

    else:
        db_path_list.append("diversities_{'-'.join(db_name_extra) if db_name_extra != [] else 'default'}.db")

        # ==== DIVERSUTILS ====

        config_diversutils["diversutils.diversity.function"]["enable_disparities"] = enable_disparities
       
    db_path = os.sep.join(db_path_list)

    logger.info(f"db_path: {db_path}")

    sql_conn = sql.connect(db_path)

    # Handle table creation

    sql_curs_create_table = sql_conn.cursor()

    sql_create_table_diversities_query = read_query_from_file(os.sep.join(["src", "sql", "diversities", "create_table.sql"]))
    sql_insert_table_diversities_query = read_query_from_file(os.sep.join(["src", "sql", "diversities", "insert_table.sql"]))
    sql_create_table_ud_tags_query = read_query_from_file(os.sep.join(["src", "sql", "ud_tags", "create_table.sql"]))
    sql_insert_table_ud_tags_query = read_query_from_file(os.sep.join(["src", "sql", "ud_tags", "insert_table.sql"]))

    sql_curs_create_table.execute(sql_create_table_diversities_query)
    sql_curs_create_table.execute(sql_create_table_ud_tags_query)

    # Prepare cursor

    sql_curs_treebank = sql_conn.cursor()
    sql_curs_file = sql_conn.cursor()
    sql_curs_tags = sql_conn.cursor()

    # Prepare regex

    treebank_regex = re.compile(r"""(?x)
        UD_(?P<LANG>[^\-]+)\-(?P<TREEBANK_NAME>.+)
    """)

    regex_filter_treebank = re.compile(pattern_treebank)
    regex_filter_lang = re.compile(pattern_lang)

    if ud_path == []:
        ud_path = list_ud_path(organization=organization, is_macro_dir=is_macro_dir)
    for subdir in ud_path:
        # Handling the treebank path

        treebank_path = subdir
        if treebank_path[-1] != os.sep:
            treebank_path += os.sep
        if not os.path.isdir(treebank_path):
            logger.info(f"ignoring {subdir} (not a directory)")
            continue

        logger.info(f"treebank: {subdir}")

        regex_result = treebank_regex.search(subdir)
        if type(regex_result) == re.Match:
            lang = regex_result.group("LANG")
            treebank_name = regex_result.group("TREEBANK_NAME")
        else:
            lang = "unknown"
            treebank_name = subdir

        if (not regex_filter_treebank.match(treebank_name)) or (not regex_filter_lang.match(lang)):
            logger.info(f"Ignoring because of filters: {repr(lang)}, {repr(treebank_name)}")
            continue

        logger.info(f"details: lang={lang}, treebank={treebank_name}")


        # ================ METADATA ================

        # Retrieve metadata from README.md

        readme_path_list = glob.glob(f"{treebank_path}README*")
        if len(readme_path_list) > 0:
            readme_path = readme_path_list[0]
            sql_curs_tags.execute("BEGIN TRANSACTION;")
            readme_f = open(readme_path, "rt", encoding="utf-8")
            found_genres = False
            for readme_line in readme_f.readlines():
                if readme_line.startswith("Genre: "):
                    for genre in readme_line[7:].split(" "):
                        sql_curs_tags.execute(sql_insert_table_ud_tags_query, (
                            lang,
                            treebank_name,
                            genre.rstrip("\n"),
                        ))
                    found_genres = True
                    break
            readme_f.close()
            sql_curs_tags.execute("END TRANSACTION;")
            if not found_genres:
                logger.error(f"Could not find genres in README at {readme_path}")
        else:
            sql_curs_tags.execute
            if "uppsalanlp" in db_name_extra:
                sql_curs_tags.execute("BEGIN TRANSACTION;")
                sql_curs_tags.execute("INSERT OR REPLACE INTO ud_tags (lang, treebank, tag) VALUES (?, ?, ?);", (lang, treebank_name, db_name_extra[1]))
                sql_curs_tags.execute("END TRANSACTION;")
            logger.warning(f"Could not find a README")

        # Retrieve metadata from stats.xml

        open_transaction = 0
        try:
            xml_path = f"{treebank_path}stats.xml"
            if not (os.path.exists(xml_path) and os.path.isfile(xml_path)):
                logger.warning(f"Could not find stats.xml")
            else:
                sql_curs_treebank.execute("BEGIN TRANSACTION;")
                open_transaction += 1
                xml_f = open(xml_path, "r")
                xml_tree = etree.parse(xml_f)
                xml_f.close()
    
                sentence_train_count = int(xml_tree.xpath("/treebank/size/train/sentences")[0].text)
                sentence_dev_count = int(xml_tree.xpath("/treebank/size/dev/sentences")[0].text)
                sentence_test_count = int(xml_tree.xpath("/treebank/size/test/sentences")[0].text)
                sentence_all_count = sentence_train_count + sentence_dev_count + sentence_test_count
    
                token_train_count = int(xml_tree.xpath("/treebank/size/train/tokens")[0].text)
                token_dev_count = int(xml_tree.xpath("/treebank/size/dev/tokens")[0].text)
                token_test_count = int(xml_tree.xpath("/treebank/size/test/tokens")[0].text)
                token_all_count = token_train_count + token_dev_count + token_test_count
    
                for type_, count_ in zip(["SENTENCE_TRAIN", "SENTENCE_DEV", "SENTENCE_TEST", "SENTENCE_ALL", "TOKEN_TRAIN", "TOKEN_DEV", "TOKEN_TEST", "TOKEN_ALL"], [sentence_train_count, sentence_dev_count, sentence_test_count, sentence_all_count, token_train_count, token_dev_count, token_test_count, token_all_count]):
                    count_type = type_.split("_")[0]
                    subtreebank = type_.split("_")[1].lower()
                    sql_curs_treebank.execute("INSERT OR REPLACE INTO diversities (div_name, div_value, lang, treebank, subtreebank) VALUES (?, ?, ?, ?, ?);", (f"META_{count_type}", count_, lang, treebank_name, subtreebank))
    
                sql_curs_treebank.execute("END TRANSACTION;")
                open_transaction -= 1
        except Exception as e:
                logger.warning(f"Encountered exception: {repr(e)}")
                if open_transaction:
                    sql_curs_treebank.execute("ROLLBACK;")
                    open_transaction -= 1


        # ================ COMPUTATION ================

        treebank_graph_index = diversutils.create_empty_graph(0, 0)
        treebank_trees = []

        # Handling the individual files in that treebank

        for filename in sorted(["."] if target_subtreebank == "all" else os.listdir(treebank_path)):
            # Handling file name

            if not (filename.endswith(".conllu") or filename == "."):
                continue

            filename_lower = filename.lower()

            subtreebank = "none"
            if target_subtreebank == "all":
                subtreebank = "all"
            else:
                for i in ["train", "dev", "test"]:
                    if i in filename_lower:
                        subtreebank = i
                        break

            filepath = f"{treebank_path}{filename}"

            filepath = os.path.dirname(filepath)
            
            logger.info(f"processing: {filepath} (exists: {os.path.exists(filepath)}; isdir: {os.path.isdir(filepath)})")

            # Generating config
            
            configs_subtreebank = stark.read_settings(config_path_stark, stark.parse_args([]))

            if not config_file_only:
                if tree_size_min == None:
                    if tree_size_max != None:
                        configs_subtreebank["tree_size"] = f"{tree_size_max}"
                    else:
                        configs_subtreebank["tree_size"] = None
                else:
                    if tree_size_max == None:
                        configs_subtreebank["tree_size"] = f"{tree_size_min}"
                    else:
                        configs_subtreebank["tree_size"] = f"{tree_size_min}-{tree_size_max}"
    
                if head != None:
                    configs_subtreebank["head"] = head
                    configs_subtreebank["root_whitelist"] = head
                if node_type != None:
                    configs_subtreebank["node_type"] = node_type
                if threads != None:
                    configs_subtreebank["cpu_cores"] = threads
                
    
    
                if configs_subtreebank["compare"] is not None:
                    configs_subtreebank["other_input_path"] = configs_subtreebank["compare"]

                output = os.sep.join(["data", "target", f"{'-'.join(db_name_extra) if db_name_extra != [] else 'None'}_{filename[:-7] if target_subtreebank != 'all' else treebank_path.rstrip(os.sep).split(os.sep)[-1] + '_all'}_{configs_subtreebank['node_type'] if 'node_type' in configs_subtreebank else 'None'}_ctt{int(complete_tree_type)}_root{root_whitelist_to_str(configs_subtreebank['root_whitelist']) if 'root_whitelist' in configs_subtreebank else 'None'}_ts{tree_size_min}-{tree_size_max}.tsv{suffix}"])
                configs_subtreebank.update({"cpu_cores": threads, "input": filepath, "input_path": filepath, "tree_size_min": tree_size_min, "tree_size_max": tree_size_max, "complete": "yes" if complete_tree_type else "no"})
            else:
                output = os.sep.join(["data", "target", f"output_{'-'.join(db_name_extra) if db_name_extra != [] else 'None'}_{config_index}_{filename[:-7] if target_subtreebank != 'all' else treebank_path.rstrip(os.sep).split(os.sep)[-1] + '_all'}.tsv{suffix}"])
                configs_subtreebank.update({"cpu_cores": threads, "input": filepath, "input_path": filepath, "tree_size_min": tree_size_min, "tree_size_max": tree_size_max})
            configs_subtreebank["output"] = None

            # Run STARK

            path_exists = os.path.exists(output)
            if path_exists:
                logger.info(f"Path exists: {output}")
            else:
                logger.info(f"Path does not exist: {output}")

            t = time.time()

            if ignore_existing_files or (not path_exists):
                #logger.info(f"configs_subtreebank: {repr(configs_subtreebank)}")
                rows = stark.run(configs_subtreebank)
                '''
                try:
                    rows = stark.run(configs_subtreebank)
                    """
                    try:
                        stark.run(configs_subtreebank)
                    except Exception as e:
                        logger.error(f"failed to run STARK on treebank {repr(treebank_name)}: {str(e)}")
                        continue
                    """
                    logger.info(f"Tree extraction took {time.time()-t:.4f}s")
                except Exception as e:
                    logger.warning(f"Exception: {repr(e)}")
                    continue
                '''

                stark_output_df = pd.DataFrame(rows[1:], columns=rows[0])

                if save_mode == "tsv":
                    f_out = opener(output, "wt", encoding="utf-8")
                    stark_output_df.to_csv(f_out, sep="\t", encoding="utf-8")
                    f_out.close()
            else:
                # Retrieve its output
    
                f_in = opener(output, "wt", encoding="utf-8")
                stark_output_df = pd.read_csv(f_in, sep="\t", encoding="utf-8", encoding_errors="ignore", engine="python", on_bad_lines="error", header=0) # header=0 denotes lines 0 is the header
                f_in.close()

            # Generate the graph object to compute diversity

            graph_index = diversutils.create_empty_graph(0, 0)

            for id_, row in stark_output_df.iterrows():
                diversutils.add_node(graph_index, int(row["Absolute frequency"]))
                diversutils.add_node(treebank_graph_index, int(row["Absolute frequency"]))
                #diversutils.add_node(graph_index, row["Absolute frequency"], row["Tree"])
                #diversutils.add_node(treebank_graph_index, row["Absolute frequency"], row["Tree"])

            diversutils.compute_relative_proportion(graph_index)

            if enable_disparities:
                mat =  tree_descriptions_to_distance_matrix(stark_output_df["Tree"], config_index=config_index, lang=lang, treebank=treebank_name, tree_edit_distance_timeout=tree_edit_distance_timeout)
                logger.info(f"Matrix: {repr(mat)}")
                diversutils.attach_distance_matrix(graph_index, mat, diversutils.FP32)
                treebank_trees += list(stark_output_df["Tree"])

            diversities_to_sql(graph_index, sql_curs_file, sql_insert_table_diversities_query, treebank_name, subtreebank, lang, configs_subtreebank, config_diversutils, config_index)

            diversutils.free_graph(graph_index)
            
            logger.info(f"Diversity computation took {time.time()-t:.4f}s")

        diversutils.compute_relative_proportion(treebank_graph_index)

        """ # old-style "all" agregation, do not remove
        if enable_disparities:
            mat = tree_descriptions_to_distance_matrix(treebank_trees, tree_edit_distance_timeout=tree_edit_distance_timeout)
            logger.info(f"Matrix: {repr(mat)}")
            diversutils.attach_distance_matrix(treebank_graph_index, mat, diversutils.FP32)

        if "configs_subtreebank" not in locals():
            #configs_subtreebank = stark.read_settings(config_path_stark, stark.parse_args([]))
            diversutils.free_graph(treebank_graph_index)
            continue

        diversities_to_sql(treebank_graph_index, sql_curs_treebank, sql_insert_table_diversities_query, treebank_name, "all", lang, configs_subtreebank, config_diversutils)

        diversutils.free_graph(treebank_graph_index)
        """

    sql_conn.close()

    t_main_bulk_diversity_computation_end = time.time()

    t_main_bulk_diversity_computation_delta = t_main_bulk_diversity_computation_end - t_main_bulk_diversity_computation_start

    logger.info(f"Total running time: {t_main_bulk_diversity_computation_delta:.3f}s")

    logger.info("end: main_bulk_diversity_computation")
    return 0

def infer_config_path_stark():
    key = "SLURM_ARRAY_TASK_ID"
    if key not in os.environ:
        raise Exception(f"config_path_stark and {key} are not defined, one must be")
    config_index_int = int(os.environ[key])
    if config_index_int < 0:
        raise Exception(f"{key} must be at least 0")
    all_options = sorted(glob.glob(os.sep.join(["config", "stark", "*.ini"])))
    if config_index_int >= len(all_options):
        raise Exception(f"{key} ({config_index_int}) is too high, there are only {len(all_options)} options")
    config_path_stark = all_options[config_index_int]
    return config_path_stark

def infer_threads():
    key = "SLURM_CPUS_PER_TASK"
    if key not in os.environ:
        threads = 4
        logger.info(f"{key} is not defined, defaulting to {threads}")
    else:
        threads = int(os.environ[key])
    return threads

def display_env_variables():
    ALL_ENV_VARIABLES_TO_DISPLAY = [
        "SLURM_ARRAY_JOB_ID",
        "SLURM_ARRAY_TASK_ID",
        "SLURM_ARRAY_TASK_COUNT",
        "SLURM_ARRAY_TASK_MIN",
        "SLURM_ARRAY_TASK_MAX",
        "SLURM_CPUS_PER_TASK",
    ]
    for env_var_to_display in ALL_ENV_VARIABLES_TO_DISPLAY:
        value = ""
        if env_var_to_display in os.environ:
            value = os.environ[env_var_to_display]
        logger.info(f"{env_var_to_display:<32}= {repr(value)}")

def infer_db_path(organization: str, config_path_stark: str, db_name_extra: List[str]):
    organization_normalized = normalize(organization)
    config_index = config_path_stark.split(os.sep)[-1].split("_")[0]
    config_index_normalized = normalize(config_index)

    db_path_list = ["data", "target", "precomputing", organization_normalized, config_index_normalized]
    os.system(f"mkdir -p {os.sep.join(db_path_list)}")
    db_path_list.append(f"data_{'-'.join(db_name_extra) if db_name_extra != [] else 'default'}.db")

    return os.sep.join(db_path_list)

def _jsonl_wrapper():
    reg_token_split = re.compile(r"[\b\s]+")

    def jsonl_to_conllu_sentences(f_in, text_key="text", id_key="id"):
        nonlocal reg_token_split

        for doc in f_in.readlines():
            json_obj = json.loads(doc.rstrip("\n"))
    
            #token_list = conllu.TokenList([{"id": id_, "form": t, "lemma": "_", "upos": "_", "xpos": "_", "feats": "_", "head": "_", "deprel": "_", "deps": "_", "misc": "_"} for t in reg_token_split.split(json_obj[text_key])])
            token_list = conllu.TokenList([conllu.Token(**{"id": id_, "form": t, "lemma": "_", "upos": "_", "xpos": "_", "feats": "_", "head": "_", "deprel": "_", "deps": "_", "misc": "_"}) for id_, t in enumerate(reg_token_split.split(json_obj[text_key]), start=1)])
            token_list.metadata["sent_id"] = json[id_key]
    
            yield token_list

    def jsonl_get_raw_tokens_per_doc(f_in, text_key="text", id_key="id"):
        for doc in f_in.readlines():
            json_obj = json.loads(doc.rstrip("\n"))
            token_list = reg_token_split.split(json_obj[text_key])
            yield {"id": json_obj[id_key], "tokens": token_list}

    return jsonl_to_conllu_sentences, jsonl_get_raw_tokens_per_doc

jsonl_to_conllu_sentences, jsonl_get_raw_tokens_per_doc = _jsonl_wrapper()

def conllu_token_only(f_in):
    for sent in conllu.parse_incr(f_in):
        token_list = [token["form"] for token in sent]
        yield {"id": sent.metadata["sent_id"], "tokens": token_list}


def main_precomputing(ud_path: Union[Generator[str, None, None], list[str]] = [], ud_path_base: Union[Generator[str, None, None], list[str]] = [], ud_path_extension: Union[Generator[str, None, None], list[str]] = [], batch_size: int = 32, config_path_stark: str = "none", threads: int = -1, config_file_only: Union[int, bool] = 1, organization: str = "UniversalDependencies", db_name_extra: list[str] = [], tree_size_min: Union[int, None] = None, tree_size_max: Union[int, None] = None, head: list[str] = [], node_type: Union[str, None] = None, complete_tree_type: int = 1, save_mode: str = "none", ignore_existing_files: Union[int, bool] = 0, token_only: Union[int, bool] = 0):
    logger.info("start: main_precomputing")

    t_main_precomputing_start = time.time()

    for d in ["data", os.sep.join(["data", "target"])]:
        os.makedirs(d, exist_ok=True)

    opener = bz2.open
    suffix = ".bz2"

    if config_path_stark == "none":
        config_path_stark = infer_config_path_stark()

    logger.info(f"config_path_stark: {config_path_stark}")

    if threads == -1:
        threads = infer_threads()

    config_index = config_path_stark.split(os.sep)[-1].split("_")[0]
    config_index_normalized = normalize(config_index)

    display_env_variables()

    # Connect to SQL database

    db_path = infer_db_path(organization=organization, config_path_stark=config_path_stark, db_name_extra=db_name_extra)

    batch = [None] * batch_size
    batch_start_index = 0 
    batch_counter = 0

    batch_rowid = None
    filepath_rowid = None

    if config_file_only:
        # ==== STARK ====

        if type(config_path_stark) != str:
            raise Exception("If config_file_only is set to True, config_path_stark must be a str")


        if config_path_stark != None:
            config_stark = stark.read_settings(config_path_stark, stark.parse_args([]))
            if tree_size_min == None or tree_size_max == None:
                ts = config_stark["tree_size"]
                if "-" in ts:
                    tree_size_min, tree_size_max = ts.split("-")
                else:
                    tree_size_min = tree_size_max = int(ts)
            if complete_tree_type == None and ("complete" in config_stark or "complete_tree_type" in config_stark):
                complete_tree_type = 0 if (("complete" in config_stark and config_stark["complete"] in ["no", False, 0]) or ("complete_tree_type" in config_stark and config_stark["complete_tree_type"] in ["no", False, 0])) else 1
            if node_type == None and "node_type" in config_stark:
                node_type = config_stark["node_type"]

        output = os.sep.join(["data", "target", f"output_{'-'.join(db_name_extra) if db_name_extra != [] else 'None'}_{config_index}.tsv"])
        config_stark.update({"cpu_cores": threads, "tree_size_min": tree_size_min, "tree_size_max": tree_size_max})
    else:
        raise Exception(f"config_file_only must be set to 1")
        """
        if tree_size_min == None:
            if tree_size_max != None:
                config_stark["tree_size"] = f"{tree_size_max}"
            else:
                config_stark["tree_size"] = None
        else:
            if tree_size_max == None:
                config_stark["tree_size"] = f"{tree_size_min}"
            else:
                config_stark["tree_size"] = f"{tree_size_min}-{tree_size_max}"

        if head != None:
            config_stark["head"] = head
            config_stark["root_whitelist"] = head
        if node_type != None:
            config_stark["node_type"] = node_type
        if threads != None:
            config_stark["cpu_cores"] = threads

        if config_stark["compare"] is not None:
            config_stark["other_input_path"] = config_stark["compare"]

        output = os.sep.join(["data", "target", f"{'-'.join(db_name_extra) if db_name_extra != [] else 'None'}_{filename[:-7] if target_subtreebank != 'all' else treebank_path.rstrip(os.sep).split(os.sep)[-1] + '_all'}_{config_stark['node_type'] if 'node_type' in config_stark else 'None'}_ctt{int(complete_tree_type)}_root{root_whitelist_to_str(config_stark['root_whitelist']) if 'root_whitelist' in config_stark else 'None'}_ts{tree_size_min}-{tree_size_max}.tsv{suffix}"])
        config_stark.update({"cpu_cores": threads, "input": filepath, "input_path": filepath, "tree_size_min": tree_size_min, "tree_size_max": tree_size_max, "complete": "yes" if complete_tree_type else "no"})
        """
    config_stark["output"] = None


    logger.info(f"db_path: {db_path}")

    sql_conn = sql.connect(db_path)

    # Handle table creation

    sql_curs_create_table = sql_conn.cursor()

    sql_curs_create_index = sql_conn.cursor()

    sql_create_table_files_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "files", "create_table.sql"]))
    sql_create_table_sentence_batches_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "sentence_batches", "create_table.sql"]))
    sql_create_table_sentences_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "sentences", "create_table.sql"]))
    sql_create_table_trees_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "trees", "create_table.sql"]))
    sql_create_table_batch_belonging_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "batch_belonging", "create_table.sql"]))
    sql_create_table_types_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "types", "create_table.sql"]))

    sql_create_index_files_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "files", "create_index.sql"]))
    sql_create_index_sentence_batches_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "sentence_batches", "create_index.sql"]))
    sql_create_index_sentences_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "sentences", "create_index.sql"]))
    sql_create_index_trees_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "trees", "create_index.sql"]))
    sql_create_index_batch_belonging_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "batch_belonging", "create_index.sql"]))
    sql_create_index_types_query = read_query_from_file(os.sep.join(["src", "sql", "precomputing", "types", "create_index.sql"]))

    sql_curs_create_table.execute(sql_create_table_files_query)
    sql_curs_create_table.execute(sql_create_table_sentence_batches_query)
    sql_curs_create_table.execute(sql_create_table_sentences_query)
    sql_curs_create_table.execute(sql_create_table_trees_query)
    sql_curs_create_table.execute(sql_create_table_batch_belonging_query)
    sql_curs_create_table.execute(sql_create_table_types_query)

    sql_curs_create_index.execute(sql_create_index_files_query)
    sql_curs_create_index.execute(sql_create_index_sentence_batches_query)
    sql_curs_create_index.execute(sql_create_index_sentences_query)
    sql_curs_create_index.execute(sql_create_index_trees_query)
    sql_curs_create_index.execute(sql_create_index_batch_belonging_query)
    sql_curs_create_index.execute(sql_create_index_types_query)

    # Prepare cursor

    sql_curs_treebank = sql_conn.cursor()
    sql_curs_file = sql_conn.cursor()
    sql_curs_tags = sql_conn.cursor()

    def token_only_generate_df():
        yield ["Tree", "Absolute frequency", "Number of nodes"]
        current_counter = Counter()
        for batch_sent in filter(lambda x: x != None, batch):
            current_counter += Counter(map(str, filter(lambda x: x != None, batch_sent["tokens"])))
        for k, v in sorted(current_counter.items(), key=lambda x: x[1], reverse=True):
            yield (k, v, 1)

    def process_batch(output: str, batch_id: Union[int, None], sql_curs_local: sql.Cursor) -> pd.DataFrame:
        nonlocal config_stark
        nonlocal opener
        nonlocal suffix
        nonlocal token_only

        #logging.debug(repr(config_stark))

        path_exists = os.path.exists(output)

        if ignore_existing_files or (not path_exists):
            if token_only:
                rows = token_only_generate_df()
            else:
                #t_stark_start = time.time()
                #logger.info(f"config_stark: {repr(config_stark)}")
                rows = stark.run(config_stark)
                """
                t_stark_end = time.time()
                t_stark_delta = t_stark_end - t_stark_start
                if t_stark_delta == 0.0:
                    stark_sent_per_second = 0.0
                else:
                    stark_sent_per_second = len(batch) / t_stark_delta
                logger.info(f"STARK time: {t_stark_delta:.3f}s, {stark_sent_per_second:.3f}sent/s")
                """
            rows = list(rows)

            stark_output_df = pd.DataFrame(rows[1:], columns=rows[0])

            if save_mode == "tsv":
                f_out = opener(output, "wt", encoding="utf-8")
                stark_output_df.to_csv(f_out, sep="\t", encoding="utf-8")
                f_out.close()
        else:
            # Retrieve its output

            f_in = opener(output, "wt", encoding="utf-8")
            stark_output_df = pd.read_csv(f_in, sep="\t", encoding="utf-8", encoding_errors="ignore", engine="python", on_bad_lines="error", header=0) # header=0 denotes lines 0 is the header
            f_in.close()

        for id_, row in stark_output_df.iterrows():
            desc = row["Tree"]
            abs_freq = row["Absolute frequency"]
            size = row["Number of nodes"]

            if len(desc) == 0:
                continue

            sql_curs_local.execute("""SELECT rowid FROM trees WHERE desc=? LIMIT 1;""", (desc,))
            sql_res = sql_curs_local.fetchall()
            if len(sql_res) == 0:
                sql_curs_local.execute("""
                    INSERT INTO trees (
                        desc,
                        size
                    ) VALUES (
                        ?,
                        ?
                    );
                """, (desc, size))
                tree_rowid = sql_curs_local.lastrowid
            else:
                tree_rowid = sql_res[0][0]

            sql_curs_local.execute("""INSERT OR REPLACE INTO types (batch_id, tree_id, abs_freq) VALUES (?, ?, ?);""", (batch_rowid, tree_rowid, abs_freq))
            type_rowid = sql_curs_local.lastrowid

        return stark_output_df

    if ud_path == [] and ud_path_base == [] and ud_path_extension == []:
        root_dir = os.sep.join(["data", "input", organization])
        #ud_path = [os.sep.join([root_dir, x]) for x in glob.glob("**/*.conllu", root_dir=root_dir, recursive=True)]
        ud_path = [root_dir]

    def extract_all_paths(paths: Union[list[str], Generator[str, None, None]], extension_names: list[str]) -> Generator[str, None, None]:
        for extension_name in extension_names:
            for p in sorted(paths):
                if not os.path.exists(p):
                    raise Exception(f"Does not exist: {repr(p)}")
                if os.path.isfile(p):
                    if not p.endswith(".{extension_name}"): # ??
                        raise Exception(f"Not a {extension_name} file: {repr(p)}")
                    yield p
                p = p.rstrip(os.sep)
                for q in sorted(glob.glob(f"**/*.{extension_name}", root_dir=p, recursive=True)):
                    yield os.sep.join([p, q])

    if ud_path_base != [] or ud_path_extension != []:
        ud_path_base = list(extract_all_paths(ud_path_base, extension_names=["conllu", "cupt", "jsonl"]))
        ud_path_extension = list(extract_all_paths(ud_path_extension, extension_names=["conllu", "cupt", "jsonl"]))

        if __debug__:
            ud_path_base = ud_path_base[:1]
            ud_path_extension = ud_path_extension[:1]
    else:
        ud_path = list(extract_all_paths(ud_path, extension_names=["conllu", "cupt", "jsonl"]))

        if __debug__:
            ud_path = ud_path[:1]

    sent_counter = 0

    sql_curs_local = sql_conn.cursor()

    t_global_start = time.time()

    def process_ud_path_list(ud_path_list, is_base):
        nonlocal sent_counter
        nonlocal sql_curs_local
        nonlocal batch
        nonlocal batch_start_index
        nonlocal batch_counter
        nonlocal batch_rowid
        nonlocal filepath_rowid

        batch = [None] * batch_size
        batch_start_index = 0 
        batch_counter = 0

        for filepath in ud_path_list:
            if len(sql_curs_local.execute("SELECT * FROM files WHERE path=? LIMIT 1;", (filepath,)).fetchall()) > 0:
                continue

            logger.info(f"Reading {repr(filepath)}")

            t_local_start = time.time()
    
            sql_curs_local.execute("BEGIN TRANSACTION;")
    
            sql_curs_local.execute(r"""
                INSERT INTO files (
                    path
                ) VALUES (
                    ?
                );
            """, (filepath,))
            filepath_rowid = sql_curs_local.lastrowid
    
            batch_name = f"batch_isbase{is_base}_{batch_counter}"
            sql_curs_local.execute(r"""
                INSERT OR REPLACE INTO sentence_batches (name) VALUES (?);
            """, (batch_name,))
            batch_rowid = sql_curs_local.lastrowid
            
            def generate_tmp_files_and_process_batch():
                nonlocal batch
                nonlocal config_stark
                nonlocal batch_start_index
                nonlocal sent_counter
                nonlocal batch_counter
                nonlocal batch_name
                nonlocal batch_rowid
                nonlocal token_only

                if not token_only:
                    tmp_f = tempfile.NamedTemporaryFile("wt")
                    for batch_sent in filter(lambda x:x != None, batch):
                        tmp_f.write(conllu.serializer.serialize(batch_sent))
                        #serialization_result = conllu.serializer.serialize(batch_sent)
                        #tmp_f.write(serialization_result)
                    tmp_f.seek(0)
    
                    config_stark["input_path"] = tmp_f.name

                output = os.sep.join(["data", "target", f"output_{organization}_{config_index}_sentences{batch_start_index}-{sent_counter}_{batch_name}.tsv{suffix}"])

                stark_output_df = process_batch(output=output, batch_id=batch_rowid, sql_curs_local=sql_curs_local)

                if not token_only:
                    tmp_f.close()

                batch_start_index = sent_counter

                batch = [None] * batch_size

                batch_counter += 1

                batch_name = f"batch_isbase{is_base}_{batch_counter}"
                sql_curs_local.execute(r"""
                    INSERT OR REPLACE INTO sentence_batches (name) VALUES (?);
                """, (batch_name,))
                batch_rowid = sql_curs_local.lastrowid

            f_in = open(filepath, "rt", encoding="utf-8")
    
            for sent in (conllu_token_only(f_in) if filepath.endswith(".conllu") or filepath.endswith(".cupt") else jsonl_get_raw_tokens_per_doc(f_in)) if token_only else (conllu.parse_incr(f_in) if filepath.endswith(".conllu") or filepath.endswith(".cupt") else jsonl_to_conllu_sentences(f_in)):
                if token_only:
                    sent_id = sent["id"]
                    num_tokens = len(sent["tokens"])
                else:
                    if "sent_id" not in sent.metadata:
                        logger.warning(f"A sentence does not have an id ({sent_counter})")
                        continue
        
                    sent_id = sent.metadata["sent_id"]
                    num_tokens = len(sent)
    
                sql_curs_local.execute(r"""
                    INSERT OR REPLACE INTO sentences (
                        sent_id,
                        file_id,
                        num_tokens
                    ) VALUES (
                        ?,
                        ?,
                        ?
                    );
                """, (sent_id, filepath_rowid, num_tokens))
                sentence_rowid = sql_curs_local.lastrowid
    
                sql_curs_local.execute(r"""
                    INSERT OR IGNORE INTO batch_belonging (batch_id, sent_id) VALUES (?, ?);
                """, (batch_rowid, sentence_rowid))
    
                batch[sent_counter % batch_size] = sent
    
                sent_counter += 1

    
                #if sent_counter - batch_start_index == batch_size:
                if sent_counter % batch_size == 0:
                    #config_stark["input"] = batch
                    if "input_filepath" in config_stark:
                        config_stark.pop("input_filepath")
                    if "input_path" in config_stark:
                        config_stark.pop("input_path")
                    
                    generate_tmp_files_and_process_batch()
    
    
            generate_tmp_files_and_process_batch()

            sql_curs_local.execute("END TRANSACTION;")

            f_in.close()
    
            t_local_end = time.time()
    
            t_local_delta = t_local_end - t_local_start
    
            t_global_delta = t_local_end - t_global_start
    
            logger.info(f"Processed: {filepath} ({t_local_delta:.3f}s, {((sent_counter/t_global_delta) if t_global_delta > 0 else 0.0):.3f}sent/s)")
    
    if ud_path_base != [] or ud_path_extension != []:
        process_ud_path_list(ud_path_base, is_base=1)
        process_ud_path_list(ud_path_extension, is_base=0)
    else:
        process_ud_path_list(ud_path, is_base=0)

    sql_conn.close()

    t_main_precomputing_end = time.time()

    t_main_precomputing_delta = t_main_precomputing_end - t_main_precomputing_start

    logger.info(f"Total running time: {t_main_precomputing_delta:.3f}s")

    logger.info("end: main_precomputing")
    return 0


class DiversityManager(torch.nn.Module):
    def __init__(self, target_score, mat_trees_per_batch_extension, num_batches, num_trees, vec_base_trees, ratio_clean_separation=0.05, **kwargs):
        super().__init__()

        logger.info(" Gradient descent preparation ".center(80, "="))

        self.target_score = torch.tensor(target_score, dtype=TORCH_DTYPE, device=TORCH_DEVICE)

        logger.info(f"Target scores: {self.target_score}")

        self.mat_trees_per_batch_extension = mat_trees_per_batch_extension

        logger.info(f"Initialized matrix of trees per batch (extension) with shape: {self.mat_trees_per_batch_extension.shape}")

        self.num_batches = num_batches
        self.num_trees = num_trees

        logger.info(f"#batches x #trees: {self.num_batches} x {self.num_trees}")

        self.ratio_clean_separation = ratio_clean_separation

        logger.info(f"Ratio clean separation {self.ratio_clean_separation}")

        self.vec_base_trees = vec_base_trees

        logger.info(f"Shape of vector for base trees: {self.vec_base_trees.shape}")
        logger.info(f"Mean vector for base trees: {torch.mean(self.vec_base_trees)}")

    def initialize_weight(self):
        raise NotImplementedError("DiversityManager.initialize_weight needs to be defined by a class that received inheritance")

    def compute_batch_probability(self):
        raise NotImplementedError("DiversityManager.compute_p_array needs to be defined by a class that received inheritance")

    def compute_p_array(self):
        raise NotImplementedError("DiversityManager.compute_p_array needs to be defined by a class that received inheritance")

    def feedforward(self):
        x = self.compute_p_array()

        """
        #print(x.shape)
        if __debug__:
            print(3, x, x.shape, any(x.isnan()))
        #x = x[x > 0.0] # this actually flattens the tensor, which is ok for DiversitySampler, but not for DiversitySplitter
        x = torch.mul(x, torch.log(x))
        x = torch.nan_to_num(x, nan=0.0)
        if __debug__:
            print(4, x, x.shape, any(x.isnan()))
        #x = torch.sum(x)
        #x = torch.sum(x, dim=-1)
        x = torch.nansum(x, dim=-1)
        if __debug__:
            print(5, x, x.shape)
        x = -x
        if __debug__:
            print(6, x, x.shape)
        #print(x.shape)
        return x
        """

        n = x.shape[0]
        y = torch.zeros(n)
        for i in range(n):
            p = x[i, :]
            p = p[p > 0.0]
            k = torch.mul(p, torch.log(p))
            y[i] = -torch.sum(k)
        return y

    def fit(self, epochs, optimizer, learning_rate=0.001):
        current_epoch = 1

        self.train(True)

        optimizer = optimizer.lower()

        if optimizer == "nadam":
            optim = torch.optim.NAdam(params=self.parameters(), lr=learning_rate)
        else:
            logger.info("Defaulting to Adam optimizer")
            optim = torch.optim.Adam(params=self.parameters(), lr=learning_rate)
        loss_fn = torch.nn.MSELoss()

        while current_epoch <= epochs:
            optim.zero_grad()

            t_start = time.time()

            ff_res = self.feedforward()
            loss = loss_fn(self.target_score, ff_res)

            """
            sigmoid_weight = torch.sigmoid(self.weight)
            #loss_clean_separation = self.ratio_clean_separation * (current_epoch / epochs) * torch.mean(sigmoid_weight * (1.0 - sigmoid_weight)) # ok for DiversitySampler
            loss_clean_separation = self.ratio_clean_separation * (current_epoch / epochs) * torch.mean(sigmoid_weight * (1.0 - sigmoid_weight))

            participation = torch.mean(sigmoid_weight)
            """

            batch_probability = self.compute_batch_probability()
            loss_clean_separation = self.ratio_clean_separation * (current_epoch / epochs) * torch.mean(batch_probability * (1.0 - batch_probability))

            participation = torch.mean(batch_probability, dim=-1)

            t_end = time.time()

            t_delta = t_end - t_start

            frmt_ff_res = "none"
            if len(ff_res.shape) == 0:
                frmt_ff_res = f"{float(ff_res):.4f}"
            else:
                frmt_ff_res = "[" + ", ".join([f"{float(x):+.4f}" for x in ff_res]) + "]"

            frmt_participation = "none"
            if len(participation.shape) == 0:
                frmt_participation = f"{float(participation):.4f}"
            else:
                frmt_participation = "[" + ", ".join([f"{float(x):.4f}" for x in participation]) + "]"

            #logger.info(f"[epoch {current_epoch:>4} / {epochs:>4}] ff_res: {float(ff_res):.3f} ; loss: {float(loss):.3f}, {float(loss_clean_separation):.3f} ; time: {t_delta:.3f}s ; participation: {float(participation):.3f}")
            logger.info(f"[epoch {current_epoch:>4} / {epochs:>4}] ff_res: {frmt_ff_res} ; loss: {float(loss):.3f}, {float(loss_clean_separation):.3f} ; time: {t_delta:.3f}s ; participation: {frmt_participation}")

            loss.backward()
            loss_clean_separation.backward()

            optim.step()

            current_epoch += 1

        self.train(False)

    def sample_output(self, sql_curs, output_dir, output_batch_size=1000):
        output_dir = output_dir.rstrip(os.sep)

        os.makedirs(output_dir, exist_ok=True)

        batch_indices_to_include = torch.where(self.weight >= 0.0)[0]

        files_to_open = {}

        sql_qry = read_query_from_file(os.sep.join(["src", "sql", "sampling_or_splitting", "extract_files_based_on_batch_id.sql"]))

        for batch_id in batch_indices_to_include:
            for row in sql_curs.execute(sql_qry, (int(batch_id),)):
                conllu_file = row[0]
                conllu_sent_id = row[1]
                if conllu_file not in files_to_open:
                    files_to_open[conllu_file] = set()
                files_to_open[conllu_file] |= {conllu_sent_id}

        num_sentences = 0

        path_f_out = os.sep.join([output_dir, "0.conllu"])
        f_out = open(path_f_out, "wt", encoding="utf-8")

        for filename in sorted(list(files_to_open.keys())):
            f_in = open(filename, "rt", encoding="utf-8")
            for sent in conllu.parse_incr(f_in):
                sent_id = sent.metadata["sent_id"]

                if sent_id in files_to_open[filename]:
                    print(f"{filename}\t{sent_id}")

                    f_out.write(conllu.serializer.serialize(sent))
                    num_sentences += 1

                    if num_sentences % output_batch_size == 0:
                        f_out.close()
                        path_f_out = os.sep.join([output_dir, f"{num_sentences // output_batch_size}.conllu"])
                        f_out = open(path_f_out, "wt", encoding="utf-8")

        f_out.close()

class DiversitySampler(DiversityManager):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.initialize_weight()

    def initialize_weight(self):
        #self.register_parameter("weight", torch.nn.Parameter(torch.zeros((self.num_batches,), dtype=TORCH_DTYPE, device=TORCH_DEVICE, requires_grad=True)))
        self.register_parameter("weight", torch.nn.Parameter(torch.zeros((1, self.num_batches), dtype=TORCH_DTYPE, device=TORCH_DEVICE, requires_grad=True)))

        logger.info(f"Initialized weight with shape: {self.weight.shape}")

    def compute_batch_probability(self):
        x = torch.nn.functional.sigmoid(self.weight)

        return x

    def compute_p_array(self):
        x = self.compute_batch_probability()

        # at this stage, x represents the probability to be sampled

        x = torch.matmul(x, self.mat_trees_per_batch_extension)

        x += self.vec_base_trees

        #x = torch.div(x, torch.nansum(x)) # works for vectors
        x = torch.div(x, torch.nansum(x, dim=1)) # works for DiversitySampler
        return x

class DiversitySplitter(DiversityManager):
    def __init__(self, n_splits=3, split_names=["train", "dev", "test"], split_ratios=[1/3, 1/3, 1/3], **kwargs):
        super().__init__(**kwargs)

        self.split_names = split_names[:]
        self.split_ratios = split_ratios[:]

        self.initialize_weight(n_splits=n_splits)

    def initialize_weight(self, n_splits):
        self.register_parameter("weight", torch.nn.Parameter(torch.zeros((n_splits, self.num_batches), dtype=TORCH_DTYPE, device=TORCH_DEVICE, requires_grad=True)))

        logger.info(f"Initialized weight with shape: {self.weight.shape}")

    def compute_batch_probability(self):
        x = torch.nn.functional.sigmoid(self.weight)
        x = torch.div(x, torch.nansum(x, dim=0)) # difference here

        return x

    def compute_p_array(self):
        x = self.compute_batch_probability()

        # at this stage, x represents the relative probability that each batch should be put in each given split

        x = torch.matmul(x, self.mat_trees_per_batch_extension)

        # at this stage, x represents the splits (a) against the vocabulary (b)

        x += self.vec_base_trees

        # at this stage, x represents the splits (a) against the vocabulary (b), with base accounted for

        # do not remove
        #print(x.shape, torch.nansum(x, dim=-1).shape)
        #print(x.shape, torch.nansum(x, dim=0).shape)

        #x = torch.div(x, torch.nansum(x)) # works for vectors
        #x = torch.div(x, torch.nansum(x, dim=1)) # works for DiversitySampler
        #x = torch.div(x, torch.nansum(x, dim=-1))
        #x = torch.div(x, torch.nansum(x, dim=0)) # runs but not correct so perhaps issue elsewhere


        #print(x.shape, torch.nansum(x, dim=0).shape, torch.nansum(x, dim=1).shape)

        #x = torch.div(x, torch.nansum(x, dim=1), dim=0)

        x = torch.div(x.T, torch.nansum(x, dim=1)).T

        # at this stage, x represents the probability array

        return x

def main_sampling_or_splitting(management_option: str = "sampling", config_path_stark: str = "none", config_path_sampling: str = "none", config_path_splitting: str = "none", threads: int = -1, config_file_only: Union[int, bool] = 1, organization: str = "UniversalDependencies", db_name_extra: list[str] = [], split_names: list[str] = ["train", "dev", "test"], split_ratios: list[float] = [1/3, 1/3, 1/3]):
    logger.info("start: main_sampling_or_splitting")

    t_main_sampling_or_splitting_start = time.time()

    if config_path_stark == "none":
        config_path_stark = infer_config_path_stark()

    logger.info(f"config_path_stark: {config_path_stark}")

    if threads == -1:
        threads = infer_threads()

    cfg_sampling_or_splitting = configparser.ConfigParser()
    if management_option == "sampling":
        cfg_sampling_or_splitting.read(config_path_sampling)
    elif management_option == "splitting":
        cfg_sampling_or_splitting.read(config_path_splitting)

    display_env_variables()

    # Connect to SQL database

    db_path = infer_db_path(organization=organization, config_path_stark=config_path_stark, db_name_extra=db_name_extra)

    logger.info(f"db_path: {db_path}")

    qry_get_max_batch = read_query_from_file(path = os.sep.join(["src", "sql", "sampling_or_splitting", "get_max_batch.sql"]), encoding = "utf-8")
    qry_get_max_tree = read_query_from_file(path = os.sep.join(["src", "sql", "sampling_or_splitting", "get_max_tree.sql"]), encoding = "utf-8")
    qry_select_batches = read_query_from_file(path = os.sep.join(["src", "sql", "sampling_or_splitting", "select_batches.sql"]), encoding = "utf-8")

    sql_conn = sql.connect(db_path)

    sql_curs = sql_conn.cursor()

    max_batch = sql_curs.execute(qry_get_max_batch, tuple()).fetchall()[0][0]
    max_tree = sql_curs.execute(qry_get_max_tree, tuple()).fetchall()[0][0]
    num_batches = max_batch + 1
    num_trees = max_tree + 1

    logger.info(f"max_batch: {max_batch}")
    logger.info(f"max_tree: {max_tree}")

    current_batch_id = None
    vec_tree_abs_freq = torch.zeros((max_tree + 1,), dtype=TORCH_DTYPE, device=TORCH_DEVICE)

    IS_TORCH_1 = torch.__version__.startswith("1.")

    # -------- EXTENSION --------

    mat_trees_per_batch_extension = []

    for row in sql_curs.execute(qry_select_batches, ('%_isbase0_%',)):
        batch_id, tree_id, abs_freq = row

        if batch_id != current_batch_id and current_batch_id != None:
            # check for gaps
            delta_batch_id = batch_id - current_batch_id
            if delta_batch_id > 1:
                if IS_TORCH_1:
                    mat_trees_per_batch_extension += [torch.zeros((max_tree + 1,), dtype=TORCH_DTYPE, device=TORCH_DEVICE)] * (delta_batch_id - 1)
                else:
                    mat_trees_per_batch_extension += [torch.zeros((max_tree + 1,), dtype=TORCH_DTYPE, device=TORCH_DEVICE).to_sparse()] * (delta_batch_id - 1)

            if IS_TORCH_1:
                mat_trees_per_batch_extension.append(vec_tree_abs_freq)
            else:
                mat_trees_per_batch_extension.append(vec_tree_abs_freq.to_sparse())

            vec_tree_abs_freq = torch.zeros((max_tree + 1,), dtype=TORCH_DTYPE, device=TORCH_DEVICE)

        vec_tree_abs_freq[tree_id] = float(abs_freq)

        current_batch_id = batch_id

    if len(mat_trees_per_batch_extension) != num_batches:
        if IS_TORCH_1:
            mat_trees_per_batch_extension += [torch.zeros((max_tree + 1,), dtype=TORCH_DTYPE, device=TORCH_DEVICE)] * (num_batches - len(mat_trees_per_batch_extension))
        else:
            mat_trees_per_batch_extension += [torch.zeros((max_tree + 1,), dtype=TORCH_DTYPE, device=TORCH_DEVICE).to_sparse()] * (num_batches - len(mat_trees_per_batch_extension))
    
    sparse_mat_trees_per_batch_extension = torch.stack(mat_trees_per_batch_extension)

    # logger.info(repr(mat_trees_per_batch_extension))

    # -------- BASE --------

    vec_base_trees = torch.zeros((max_tree + 1,), dtype=TORCH_DTYPE, device=TORCH_DEVICE, requires_grad=False)
    for row in sql_curs.execute(qry_select_batches, ('%_isbase1_%',)):
        batch_id, tree_id, abs_freq = row
        vec_base_trees[tree_id] += abs_freq

    # -------- DIVERSITYMANAGER --------

    #print(cfg_sampling_or_splitting.keys())
    print(config_path_sampling, config_path_splitting)

    ClassToUse = DiversityManager
    standard_args = {
        "mat_trees_per_batch_extension": sparse_mat_trees_per_batch_extension,
        "num_batches": num_batches,
        "num_trees": num_trees,
        "ratio_clean_separation": float(cfg_sampling_or_splitting["optimization"]["ratio_clean_separation"]),
        "vec_base_trees": vec_base_trees
    }
    non_standard_args = {}

    if management_option == "sampling":
        ClassToUse = DiversitySampler
        non_standard_args = {
            "target_score": float(cfg_sampling_or_splitting["target"]["score"])
        }
        #sampler_or_splitter = ClassToUse(target_score=float(cfg_sampling["target"]["score"]), mat_trees_per_batch_extension=sparse_mat_trees_per_batch_extension, num_batches=num_batches, num_trees=num_trees, ratio_clean_separation=float(cfg_sampling["optimization"]["ratio_clean_separation"]), vec_base_trees=vec_base_trees)
    elif management_option == "splitting":
        ClassToUse = DiversitySplitter
        """
        if "split_names" not in kwargs.keys():
            raise Exception("need split_names for splitting")
        if "split_ratios" not in kwargs.keys():
            n = len(kwargs["split_names"])
            kwargs["split_ratios"] = [1.0/n] * n
        non_standard_args = {
            "split_names": kwargs["split_names"],
            "split_ratios": kwargs["split_ratios"]
        }
        """

        if len(split_names) != len(split_ratios):
            raise Exception("len(split_names) != len(split_ratios)")
        target_scores = []
        if type(cfg_sampling_or_splitting["target"]["score"]) == str:
            target_scores = [float(x) for x in cfg_sampling_or_splitting["target"]["score"].split(" ")]
        if type(target_scores) == list:
            target_scores = torch.tensor(target_scores)
        non_standard_args = {
            "target_score": target_scores,
            "split_names": split_names,
            "split_ratios": split_ratios
        }
        #sampler_or_splitter = ClassToUse(target_score=float(cfg_sampling["target"]["score"]), mat_trees_per_batch_extension=sparse_mat_trees_per_batch_extension, num_batches=num_batches, num_trees=num_trees, ratio_clean_separation=float(cfg_sampling["optimization"]["ratio_clean_separation"]), vec_base_trees=vec_base_trees)

    #sampler_or_splitter = ClassToUse(target_score=float(cfg_sampling["target"]["score"]), mat_trees_per_batch_extension=sparse_mat_trees_per_batch_extension, num_batches=num_batches, num_trees=num_trees, ratio_clean_separation=float(cfg_sampling["optimization"]["ratio_clean_separation"]), vec_base_trees=vec_base_trees) # original

    sampler_or_splitter = ClassToUse(**(standard_args | non_standard_args))

    sampler_or_splitter.fit(epochs=int(cfg_sampling_or_splitting["hyperparameters"]["epochs"]), optimizer=cfg_sampling_or_splitting["optimization"]["optimizer"], learning_rate=float(cfg_sampling_or_splitting["optimization"]["learning_rate"]))

    sampler_or_splitter.sample_output(sql_curs=sql_conn.cursor(), output_dir=cfg_sampling_or_splitting["io"]["output_dir"])

    sql_conn.close()

    return 0

if __name__ == "__main__":
    logger.info("start: parsing arguments")

    parser = argparse.ArgumentParser(prog="delta")

    parser.add_argument("-a", "--tree_size_min", type=int, default=None)
    parser.add_argument("-b", "--tree_size_max", type=int, default=None)
    parser.add_argument("-c", "--head", type=str, default=[], nargs="+")
    parser.add_argument("-d", "--enable_disparities", type=int, default=0)
    parser.add_argument("-e", "--tree_edit_distance_timeout", type=float, default=0.1)
    parser.add_argument("-f", "--config_path_stark", type=str, default="none")
    parser.add_argument("-g", "--config_file_only", type=int, default=0)
    parser.add_argument("-i", "--ignore_existing_files", type=int, default=0)
    parser.add_argument("-j", "--config_path_sampling", type=str, default=os.sep.join(["config", "sampling", "gradient_based", "base.ini"]))
    parser.add_argument("-k", "--config_path_splitting", type=str, default=os.sep.join(["config", "splitting", "gradient_based", "base.ini"]))
    parser.add_argument("-l", "--extract_lang", type=int, default=1)
    parser.add_argument("-m", "--is_macro_dir", type=int, default=0)
    parser.add_argument("-n", "--node_type", type=str, default=None)
    parser.add_argument("-o", "--organization", type=str, default="UniversalDependencies")
    parser.add_argument("-p", "--save_mode", type=str, default="none")
    parser.add_argument("-q", "--pattern_treebank", type=str, default="^.*$")
    parser.add_argument("-r", "--pattern_lang", type=str, default="^.*$")
    parser.add_argument("-s", "--target_subtreebank", type=str, default="all")
    parser.add_argument("-t", "--threads", type=int, default=-1)
    parser.add_argument("-u", "--ud_path", type=str, nargs="+", default=[])
    parser.add_argument("-w", "--config_path_diversutils", type=str, default=os.sep.join(["config", "diversutils", "A1_linear_mt_exp-fr.ini"]))
    parser.add_argument("-x", "--batch_size", type=int, default=16)
    parser.add_argument("-y", "--db_name_extra", type=str, nargs="+", default=[])
    parser.add_argument("-z", "--complete_tree_type", type=int, default=1)
    parser.add_argument("--ud_path_base", type=str, nargs="+", default=[])
    parser.add_argument("--ud_path_extension", type=str, nargs="+", default=[])
    parser.add_argument("--action", type=str, default="bulk_diversity_computation")
    parser.add_argument("--token_only", type=int, default=0)

    logger.info("end: parsing arguments")

    args = parser.parse_args()

    if args.action == "bulk_diversity_computation":
        exit(main_bulk_diversity_computation(ud_path=args.ud_path, config_path_stark=args.config_path_stark, config_path_diversutils=args.config_path_diversutils, config_file_only=args.config_file_only, extract_lang=args.extract_lang, tree_size_min=args.tree_size_min, tree_size_max=args.tree_size_max, head=args.head, node_type=args.node_type, threads=args.threads, enable_disparities=args.enable_disparities, tree_edit_distance_timeout=args.tree_edit_distance_timeout, target_subtreebank=args.target_subtreebank, complete_tree_type=args.complete_tree_type, db_name_extra=args.db_name_extra, organization=args.organization, is_macro_dir=args.is_macro_dir, save_mode=args.save_mode, ignore_existing_files=args.ignore_existing_files, pattern_treebank=args.pattern_treebank, pattern_lang=args.pattern_lang))
    elif args.action == "precomputing":
        exit(main_precomputing(ud_path=args.ud_path, ud_path_base=args.ud_path_base, ud_path_extension=args.ud_path_extension, organization=args.organization, config_path_stark=args.config_path_stark, batch_size=args.batch_size, threads=args.threads, config_file_only=args.config_file_only, tree_size_min=args.tree_size_min, tree_size_max=args.tree_size_max, head=args.head, ignore_existing_files=args.ignore_existing_files, node_type=args.node_type, save_mode=args.save_mode, complete_tree_type=args.complete_tree_type, db_name_extra=args.db_name_extra, token_only=args.token_only))
    elif args.action in ["sampling", "splitting"]:
        exit(main_sampling_or_splitting(management_option=args.action, config_path_stark=args.config_path_stark, config_path_sampling=args.config_path_sampling, config_path_splitting=args.config_path_splitting, threads=args.threads, config_file_only=args.config_file_only, db_name_extra=args.db_name_extra, organization=args.organization))
    else:
        logger.error(f"Unknown action: {repr(args.action)}")
        exit(1)

