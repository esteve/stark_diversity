from setuptools import setup, Extension
import os

setup(
    name="delta",
    author="Louis Estève, Kaja Dobrovoljc",
    author_email="louis.esteve@lisn.fr, kaja.dobrovoljc@ff.uni-lj.si",
    version="0.1.1",
    packages=["delta"],
    package_dir={"delta": "delta"},
)

